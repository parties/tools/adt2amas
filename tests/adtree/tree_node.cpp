#include "tree_node.hpp"
#include <catch.hpp>
#include <string>
#include <vector>
#include "attributes/cost_attribute.hpp"

using namespace std;

/* TODO:
 * 1) to_string_parents()
 * 2) to_string_children()
 * 3) get_info()
 * 4) get_node_behavior()
 * 5) add agents
 */

TEST_CASE("A node has a role", "[tree node]") {
  TreeNode node;
  REQUIRE(node.get_node_role() == NodeRole::Attack);

  SECTION("its role can be modified and retrieved") {
    node.set_node_role(NodeRole::Defence);
    REQUIRE(node.get_node_role() == NodeRole::Defence);
  }
}

TEST_CASE("A node has a several attributes (e.g, time, cost)") {
  TreeNode node;
  REQUIRE(node.get_attributes().size() == 0);

  SECTION("the attributes can be retrieved or modified") {
    CostAttribute *cost = new CostAttribute(10);
    node.set_attributes({cost});

    REQUIRE(node.get_attributes().size() == 1);
    REQUIRE(node.get_attributes()[0] == cost);
  }
}

TEST_CASE("A node has a goal (name)", "[tree node]") {
  TreeNode node("n", NodeRole::Attack, "a");

  REQUIRE(node.get_node_role() == NodeRole::Attack);
  REQUIRE(node.get_goal() == "a");

  SECTION("a goal can be retrieved and modified") {
    node.set_goal("b");
    REQUIRE(node.get_goal() == "b");
  }
}

TEST_CASE("A node has children", "[tree node]") {
  TreeNode node;
  REQUIRE(node.get_child_associated_ids().empty());
  REQUIRE(node.get_children().empty());

  SECTION("Children can be added") {
    vector<TreeNode *> children =
        vector<TreeNode *>{new TreeNode(), new TreeNode()};
    node.set_children(children);

    REQUIRE(node.get_children().size() == 2);
  }

  SECTION("Identifier of children can be added") {
    set<int> children_id = set<int>{1, 2};
    node.set_child_associated_ids(children_id);

    REQUIRE(node.get_child_associated_ids().size() == 2);
  }
}

TEST_CASE("A node has parents", "[tree node]") {
  TreeNode node;
  REQUIRE(node.get_parents().empty());
  REQUIRE(node.get_parent_associated_ids().empty());

  SECTION("Parents can be added") {
    vector<TreeNode *> parents =
        vector<TreeNode *>{new TreeNode(), new TreeNode()};
    node.set_parents(parents);

    REQUIRE(node.get_parents().size() == 2);
  }

  SECTION("Identifier of parents can be added") {
    set<int> parents_id = set<int>{1, 2};
    node.set_parent_associated_ids(parents_id);

    REQUIRE(node.get_parent_associated_ids().size() == 2);
  }
}

TEST_CASE("New Tree Node with no parameters", "[tree node]") {
  TreeNode node;

  // SECTION("Its id is 0") { REQUIRE(node.get_id() == 0); }

  SECTION("It has no attributes") {
    REQUIRE(node.get_attributes().size() == 0);
  }

  SECTION("Its role by default is Attacker") {
    REQUIRE(node.get_node_role() == NodeRole::Attack);
  }

  SECTION("It has no children") {
    REQUIRE(node.get_children().empty());
    REQUIRE(node.get_child_associated_ids().empty());
  }
  SECTION("It has no parents") {
    REQUIRE(node.get_parents().empty());
    REQUIRE(node.get_parent_associated_ids().empty());
  }
}
