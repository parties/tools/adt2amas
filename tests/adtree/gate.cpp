#include "gate.hpp"
#include <catch.hpp>

/* TODO:
 * 1) Add conditions to countermeasure gates
 */

TEST_CASE("New Gate with no parameters", "[gate]") {
  Gate gate;

  SECTION("Its role by default is Attacker") {
    REQUIRE(gate.get_node_role() == NodeRole::Attack);
  }
}
