#include "leaf.hpp"
#include <catch.hpp>

/* TODO:
 * 3) get_info()
 * 4) get_node_behavior()
 */

TEST_CASE("New Leaf with no parameters", "[leaf]") {
  Leaf leaf;

  SECTION("Its role by default is Attacker") {
    REQUIRE(leaf.get_node_role() == NodeRole::Attack);
  }
}
