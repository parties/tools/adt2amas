#include "ad_tree.hpp"
#include <catch.hpp>
#include <vector>

using namespace std;

/* TODO:
 * 1) root must be a Gate
 * 2) function to change root
 * 3) get_info(**)
 * 4) change node_behavior by gate_type
 * 5) Add function to check if a gate is a countermeasure gate
 * 6) A node cannot be child of different trees
 * 7) A leaf cannot have children
 * 8) add test for add_parent_pos
 * 9) add test for add_child_pos
 */

TEST_CASE("New Tree Node with no parameters", "[tree node]") {
  TreeNode* root = new TreeNode();
  ADtree my_tree(root);

  SECTION("It must have a root node") {
    REQUIRE(my_tree.get_root() == root);
    REQUIRE(my_tree.get_node_associated_ids().size() == 1);
    REQUIRE(*my_tree.get_node_associated_ids().begin() ==
            my_tree.get_root()->get_id());
  }
}

TEST_CASE("Children can be added to the tree", "[tree node]") {
  TreeNode* root = new TreeNode();
  TreeNode* child = new TreeNode();

  ADtree my_tree(root);

  REQUIRE(my_tree.get_node_associated_ids().size() == 1);
  REQUIRE(root->get_children().size() == 0);
  REQUIRE(child->get_children().size() == 0);
  REQUIRE(child->get_parents().size() == 0);

  SECTION("It is possible to add a child to the root node") {
    my_tree.add_child(child, root);

    REQUIRE(my_tree.get_node_associated_ids().size() == 2);

    REQUIRE(root->get_children().size() == 1);
    REQUIRE(root->get_parents().size() == 0);
    REQUIRE(root->get_children()[0] == child);
    REQUIRE(*root->get_child_associated_ids().begin() == child->get_id());

    REQUIRE(child->get_children().size() == 0);
    REQUIRE(child->get_parents().size() == 1);
    REQUIRE(child->get_parents()[0] == root);
    REQUIRE(*child->get_parent_associated_ids().begin() == root->get_id());
  }
}

TEST_CASE("Parent can be added to the tree", "[tree node]") {
  TreeNode* root = new TreeNode();
  TreeNode* child_left = new TreeNode();
  TreeNode* child_right = new TreeNode();

  ADtree my_tree(root);

  REQUIRE(my_tree.get_node_associated_ids().size() == 1);
  REQUIRE(root->get_children().size() == 0);
  REQUIRE(child_left->get_children().size() == 0);
  REQUIRE(child_left->get_parents().size() == 0);
  REQUIRE(child_right->get_children().size() == 0);
  REQUIRE(child_right->get_parents().size() == 0);

  SECTION("It is possible to add a parent to the node") {
    my_tree.add_child(child_left, root);
    my_tree.add_child(child_right, root);
    my_tree.add_parent(child_left, child_right);

    REQUIRE(my_tree.get_node_associated_ids().size() == 3);

    REQUIRE(root->get_children().size() == 2);
    REQUIRE(root->get_parents().size() == 0);
    REQUIRE(root->get_children()[0] == child_left);
    REQUIRE(root->get_children()[1] == child_right);
    REQUIRE(root->get_child_associated_ids().size() == 2);

    REQUIRE(child_left->get_children().size() == 1);
    REQUIRE(child_left->get_parents().size() == 1);
    REQUIRE(child_left->get_parents()[0] == root);
    REQUIRE(child_left->get_parent_associated_ids().size() == 1);

    REQUIRE(child_right->get_children().size() == 0);
    REQUIRE(child_right->get_parents().size() == 2);
    REQUIRE(child_right->get_parents() == vector<TreeNode*>{root, child_left});
    REQUIRE(child_right->get_parent_associated_ids().size() == 2);
  }
}

TEST_CASE("Children can be removed from the tree", "[tree node]") {
  TreeNode* root = new TreeNode();
  TreeNode* child_left = new TreeNode();
  TreeNode* child_right = new TreeNode();

  ADtree my_tree(root);
  my_tree.add_child(child_left, root);
  my_tree.add_child(child_right, root);

  REQUIRE(my_tree.get_node_associated_ids().size() == 3);
  REQUIRE(root->get_children().size() == 2);
  REQUIRE(root->get_child_associated_ids().size() == 2);

  REQUIRE(child_left->get_children().size() == 0);
  REQUIRE(child_left->get_parents().size() == 1);
  REQUIRE(child_left->get_child_associated_ids().size() == 0);
  REQUIRE(child_left->get_parent_associated_ids().size() == 1);

  REQUIRE(child_right->get_children().size() == 0);
  REQUIRE(child_right->get_parents().size() == 1);
  REQUIRE(child_right->get_child_associated_ids().size() == 0);
  REQUIRE(child_right->get_parent_associated_ids().size() == 1);

  SECTION("Child can be removed") {
    my_tree.remove_child(child_right, root);

    REQUIRE(my_tree.get_node_associated_ids().size() == 2);
    REQUIRE(root->get_children().size() == 1);
    REQUIRE(root->get_child_associated_ids().size() == 1);

    REQUIRE(child_right->get_children().size() == 0);
    REQUIRE(child_right->get_parents().size() == 0);
    REQUIRE(child_right->get_child_associated_ids().size() == 0);
    REQUIRE(child_right->get_parent_associated_ids().size() == 0);
  }
}

TEST_CASE("Parent can be removed to the tree", "[tree node]") {
  TreeNode* root = new TreeNode();
  TreeNode* child_left = new TreeNode();
  TreeNode* child_right = new TreeNode();

  ADtree my_tree(root);
  my_tree.add_child(child_left, root);
  my_tree.add_child(child_right, root);

  REQUIRE(my_tree.get_node_associated_ids().size() == 3);
  REQUIRE(root->get_children().size() == 2);
  REQUIRE(root->get_child_associated_ids().size() == 2);

  REQUIRE(child_left->get_children().size() == 0);
  REQUIRE(child_left->get_parents().size() == 1);
  REQUIRE(child_left->get_child_associated_ids().size() == 0);
  REQUIRE(child_left->get_parent_associated_ids().size() == 1);

  REQUIRE(child_right->get_children().size() == 0);
  REQUIRE(child_right->get_parents().size() == 1);
  REQUIRE(child_right->get_child_associated_ids().size() == 0);
  REQUIRE(child_right->get_parent_associated_ids().size() == 1);

  SECTION("Parent can be removed") {
    my_tree.remove_parent(root, child_right);

    REQUIRE(my_tree.get_node_associated_ids().size() == 2);
    REQUIRE(root->get_children().size() == 1);
    REQUIRE(root->get_child_associated_ids().size() == 1);

    REQUIRE(child_right->get_children().size() == 0);
    REQUIRE(child_right->get_parents().size() == 0);
    REQUIRE(child_right->get_child_associated_ids().size() == 0);
    REQUIRE(child_right->get_parent_associated_ids().size() == 0);
  }
}
