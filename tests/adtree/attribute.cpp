#include "attribute.hpp"
#include <catch.hpp>

TEST_CASE("A new attribute with no arguments", "[attribute]") {
  Attribute attr;

  SECTION("its value by default is 0") { REQUIRE(attr.get_value() == 0); }
}

TEST_CASE("An attribute has a value", "[attribute]") {
  Attribute attr;
  REQUIRE(attr.get_value() == 0);

  SECTION("the value can be retrieved and modified") {
    attr.set_value(10);
    REQUIRE(attr.get_value() == 10);
  }
}
