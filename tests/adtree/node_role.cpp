#include "node_role.hpp"
#include <catch.hpp>

TEST_CASE("Node types have a string representation", "[node type]") {
  SECTION("NodeRole::Defence") {
    NodeRole defence_type = NodeRole::Defence;
    REQUIRE(to_string(defence_type) == "Defence");
  }
  SECTION("NodeRole::Attack") {
    NodeRole defence_type = NodeRole::Attack;
    REQUIRE(to_string(defence_type) == "Attack");
  }
}
