#include "../../case_studies/forestall.hpp"

#include <catch.hpp>
#include <fstream>

#include "amas/parser/amas_tikz_visitor.hpp"
#include "gates/nand.hpp"
#include "gates/or.hpp"
#include "gates/sand.hpp"
#include "gates/snand.hpp"
#include "leaf.hpp"
#include "translator.hpp"

using namespace std;

TEST_CASE("Forestalling a software release example", "[forestall]") {
  ADtree* forestall = get_forestall();

  Automata* amata = Translator::adt2amas_translate(forestall);

  SECTION("the amas has a tikz representation") {
    ofstream myfile("forestall.tex");
    AMASTikzVisitor visitor;
    amata->accept(visitor);

    if (myfile.is_open()) {
      myfile << "\\documentclass[11pt]{article}" << endl;
      myfile << "\\usepackage{tikz}" << endl;
      myfile << "\\usetikzlibrary{arrows, automata, positioning}" << endl;
      myfile << "\\tikzset{initial text =\\(\\)}" << endl;
      myfile << "\\begin{document}" << endl;

      myfile << visitor.get_tikz() << endl;

      myfile << "\\end{document}" << endl;
      myfile.close();
    }
  }

  SECTION("the amas is composed of 19 automata") {
    REQUIRE(amata->get_vector_automaton().size() == 19);
  }

  SECTION("the amas is composed of 38 channels") {
    REQUIRE(amata->get_channels().size() == 38);
  }
}
