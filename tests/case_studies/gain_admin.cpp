#include "../../case_studies/gain_admin.hpp"

#include <catch.hpp>
#include <fstream>

#include "amas/parser/amas_tikz_visitor.hpp"
#include "gates/nand.hpp"
#include "gates/or.hpp"
#include "gates/sand.hpp"
#include "gates/snand.hpp"
#include "leaf.hpp"
#include "translator.hpp"

using namespace std;

TEST_CASE("Obtain admin privileges", "[gain admin]") {
  ADtree* gain_admin = get_gain_admin();

  Automata* amata = Translator::adt2amas_translate(gain_admin);

  SECTION("the amas has a tikz representation") {
    ofstream myfile("gain-admin.tex");
    AMASTikzVisitor visitor;
    amata->accept(visitor);

    if (myfile.is_open()) {
      myfile << "\\documentclass[11pt]{article}" << endl;
      myfile << "\\usepackage{tikz}" << endl;
      myfile << "\\usetikzlibrary{arrows, automata, positioning}" << endl;
      myfile << "\\tikzset{initial text =\\(\\)}" << endl;
      myfile << "\\begin{document}" << endl;

      myfile << visitor.get_tikz() << endl;

      myfile << "\\end{document}" << endl;
      myfile.close();
    }
  }

  SECTION("the amas is composed of 26 automata") {
    REQUIRE(amata->get_vector_automaton().size() == 26);
  }

  SECTION("the amas is composed of 52 channels") {
    REQUIRE(amata->get_channels().size() == 52);
  }
}
