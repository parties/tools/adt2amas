#include "../../case_studies/treasure_hunters.hpp"

#include <catch.hpp>
#include <fstream>
#include <iostream>
#include <string>

#include "amas/parser/amas_imitator_visitor.hpp"
#include "amas/parser/amas_tikz_visitor.hpp"
#include "factory.hpp"
#include "gates/and.hpp"
#include "gates/nand.hpp"
#include "gates/or.hpp"
#include "gates/sand.hpp"
#include "inequality.hpp"
#include "leaf.hpp"
#include "translator.hpp"

using namespace std;

TEST_CASE("Treasure Hunters", "[treasure hunters]") {
  ADtree* treasure_hunter = get_treasure_hunters();

  Automata* amata = Translator::adt2amas_translate(treasure_hunter);

  SECTION("the amas has a tikz representation") {
    ofstream myfile("treasure_hunters_tikz.tex");
    AMASTikzVisitor visitor;
    amata->accept(visitor);

    if (myfile.is_open()) {
      myfile << "\\documentclass[11pt]{article}" << endl;
      myfile << "\\usepackage{xcolor, tikz}" << endl;
      myfile << "\\usetikzlibrary{arrows, automata, positioning}" << endl;
      myfile << "\\tikzset{initial text =\\(\\)}" << endl;
      myfile << "\\begin{document}" << endl;

      myfile << visitor.get_tikz() << endl;

      myfile << "\\end{document}" << endl;
      myfile.close();
    }
  }

  SECTION("the amas is composed of 9 automata") {
    REQUIRE(amata->get_vector_automaton().size() == 9);
  }

  SECTION("the amas is composed of 18 channels") {
    REQUIRE(amata->get_channels().size() == 18);
  }

  SECTION("the amas has an imitator model") {
    ofstream myfile("treasure_hunters.imi");
    AMASIMITATORVisitor visitor;
    amata->accept(visitor);

    if (myfile.is_open()) {
      myfile << visitor.get_imitator() << endl;
      myfile.close();
    }
  }
}
