#include "../../case_studies/iot_dev.hpp"

#include <catch.hpp>
#include <fstream>

#include "amas/parser/amas_tikz_visitor.hpp"
#include "gates/and.hpp"
#include "gates/nand.hpp"
#include "gates/or.hpp"
#include "gates/sand.hpp"
#include "gates/snand.hpp"
#include "leaf.hpp"
#include "translator.hpp"

using namespace std;

TEST_CASE("Compromise IoT device", "[iot-dev]") {
  ADtree* iot_dev = get_iot_dev();

  Automata* amata = Translator::adt2amas_translate(iot_dev);

  SECTION("the amas has a tikz representation") {
    ofstream myfile("iot-dev.tex");
    AMASTikzVisitor visitor;
    amata->accept(visitor);

    if (myfile.is_open()) {
      myfile << "\\documentclass[11pt]{article}" << endl;
      myfile << "\\usepackage{tikz}" << endl;
      myfile << "\\usetikzlibrary{arrows, automata, positioning}" << endl;
      myfile << "\\tikzset{initial text =\\(\\)}" << endl;
      myfile << "\\begin{document}" << endl;

      myfile << visitor.get_tikz() << endl;

      myfile << "\\end{document}" << endl;
      myfile.close();
    }
  }

  SECTION("the amas is composed of 16 automata") {
    REQUIRE(amata->get_vector_automaton().size() == 16);
  }

  SECTION("the amas is composed of 32 channels") {
    REQUIRE(amata->get_channels().size() == 32);
  }
}
