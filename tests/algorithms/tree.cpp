#include "algorithms/tree.hpp"

#include <catch.hpp>
#include <iostream>
#include <string>
#include <vector>

TEST_CASE("Ancestors of a Tree", "[ancestors_tree]") {
  Tree<std::string, std::string> tree;
  tree.add_vertex("a", "a");
  tree.add_vertex("b", "b");
  tree.add_vertex("c", "c");
  tree.add_vertex("d", "d");
  tree.add_vertex("e", "e");
  tree.add_vertex("f", "f");
  tree.add_vertex("g", "g");
  tree.set_root("a");
  tree.add_edge("a", "b");
  tree.add_edge("a", "c");
  tree.add_edge("c", "d");
  tree.add_edge("c", "e");
  tree.add_edge("c", "f");
  tree.add_edge("d", "g");
  tree.add_edge("e", "g");
  tree.add_edge("f", "g");

  std::vector<std::string> ancestors = tree.get_ancestors("g");

  REQUIRE(ancestors.size() == 5);
}
