#include "algorithms/layered_reductions.hpp"

#include <catch.hpp>
#include <fstream>
#include <iostream>
#include <set>

#include "../../case_studies/treasure_hunters.hpp"
#include "amas/parser/amas_imitator_visitor.hpp"
#include "amas/parser/amas_tikz_visitor.hpp"
#include "translator.hpp"

TEST_CASE("Layered Reductions", "[reductions]") {
  ADtree* adtree = get_treasure_hunters();
  Automata* eamas = Translator::adt2amas_translate(adtree);
  LayeredReductions reductions = LayeredReductions(adtree, eamas);
  SG sg_tree = reductions.get_tree();

  SECTION("Calculate height") { REQUIRE(sg_tree.height == 3); }

  SECTION("Calculate depths") {
    SG::depths_map depths = sg_tree.depths;

    REQUIRE(depths[0].size() == 1);
    REQUIRE(depths[1].size() == 2);
    REQUIRE(depths[2].size() == 2);
    REQUIRE(depths[3].size() == 4);
  }

  SECTION("Calculate number of children of a node") {
    int n_children = sg_tree.get_number_children_node("TF");
    REQUIRE(n_children == 2);
  }

  SECTION("Calculate number of children of a depth") {
    int n_children = sg_tree.get_number_children_depth(2);
    int n_children_height = sg_tree.get_number_children_depth(3);
    REQUIRE(n_children == 4);
    REQUIRE(n_children_height == 0);
  }

  SECTION("Return the synchronization action of an automaton") {
    Automaton* a = reductions.get_tree().get_vertex_data("TF").automaton;
    std::vector<Action*> sync_actions = a->get_actions();
    REQUIRE(sync_actions.size() == 7);
  }

  SECTION("Return the actions from one automaton that are not in another one") {
    Automaton* a = reductions.get_tree().get_vertex_data("TF").automaton;
    Automaton* b = reductions.get_tree().get_vertex_data("GA").automaton;
    Automaton* c = reductions.get_tree().get_vertex_data("ST").automaton;

    std::vector<Action*> sync_actions_a = a->get_actions();
    std::vector<Action*> sync_actions_b = b->get_actions();
    std::vector<Action*> sync_actions_c = c->get_actions();

    std::vector<std::vector<Action*>> tmp = {sync_actions_b, sync_actions_c};
    std::vector<Action*> sync_actions_children = flatten(tmp);

    std::vector<Action*> except_actions =
        except(sync_actions_a, sync_actions_children,
               [reductions](Action* a, Action* b) -> bool {
                 return reductions.get_action_label(a) ==
                        reductions.get_action_label(b);
               });

    REQUIRE(sync_actions_a.size() == 7);
    REQUIRE(sync_actions_b.size() == 7);
    REQUIRE(sync_actions_c.size() == 7);
    REQUIRE(sync_actions_children.size() == 14);
    REQUIRE(except_actions.size() == 3);
  }

  SECTION("Find transitions t from a node") {
    LayeredReductions::TransitionsOutput transitions = reductions.lstC("TF");

    REQUIRE(transitions.t.size() == 2);
    REQUIRE(transitions.lstc.size() == 3);
  }

  SECTION("Update transitions lstC with v=v+child(MN) in node at depth") {
    ADtree* adtree = get_treasure_hunters();
    Automata* eamas = Translator::adt2amas_translate(adtree);
    LayeredReductions reductions = LayeredReductions(adtree, eamas);

    SG tree = reductions.get_tree();
    Automaton* ST = tree.get_vertex_data("ST").automaton;
    Automaton* GA = tree.get_vertex_data("GA").automaton;

    reductions.reduction_at_depth(2);
    // std::cout << GA->get_info() << std::endl;
  }

  SECTION("Apply reduction to EAMAS") {
    ADtree* adtree = get_treasure_hunters();
    Automata* eamas = Translator::adt2amas_translate(adtree);
    LayeredReductions reductions = LayeredReductions(adtree, eamas);

    reductions.apply_reduction();
    SG tree = reductions.get_tree();
    Automaton* TF = tree.get_vertex_data("TF").automaton;
    Automaton* p = tree.get_vertex_data("p").automaton;

    std::cout << TF->get_info() << std::endl;
    std::cout << p->get_info() << std::endl;
  }

  SECTION("the reduction has imitator representation") {
    std::ofstream myfile("treasure_hunters_reduction.imi");
    AMASIMITATORVisitor visitor;

    ADtree* adtree = get_treasure_hunters();
    Automata* eamas = Translator::adt2amas_translate(adtree);
    LayeredReductions reductions = LayeredReductions(adtree, eamas);
    reductions.apply_reduction();

    eamas->accept(visitor);
    if (myfile.is_open()) {
      myfile << visitor.get_imitator() << std::endl;
      myfile.close();
    }
  }

  SECTION("the reduction has tikz representation") {
    std::ofstream myfile("treasure_hunters_reduction_tikz.tex");
    AMASTikzVisitor visitor;

    ADtree* adtree = get_treasure_hunters();
    Automata* eamas = Translator::adt2amas_translate(adtree);
    LayeredReductions reductions = LayeredReductions(adtree, eamas);
    reductions.apply_reduction();

    eamas->accept(visitor);

    if (myfile.is_open()) {
      myfile << "\\documentclass[preview]{standalone}" << std::endl;
      myfile << "\\usepackage[table,dvipsnames]{xcolor}" << std::endl;
      myfile << "\\colorlet{colorCondition}{RedOrange}" << std::endl;
      myfile << "\\colorlet{colorAttribute}{PineGreen!75!green!85!black}"
             << std::endl;
      myfile << "\\colorlet{colorSync}{Blue}" << std::endl;
      myfile << "\\colorlet{colorTransition}{blue}" << std::endl;
      myfile << "\\usepackage{tikz}" << std::endl;
      myfile << "\\usetikzlibrary{arrows, automata, positioning}" << std::endl;
      myfile << "\\tikzset{initial text =\\(\\)}" << std::endl;
      myfile << "\\begin{document}" << std::endl;

      myfile << visitor.get_tikz() << std::endl;

      myfile << "\\end{document}" << std::endl;
      myfile.close();
    }
  }
}
