#include "algorithms/min_agents.hpp"

#include <catch.hpp>
#include <fstream>
#include <iostream>

#include "../../case_studies/icfem2020.hpp"
#include "../../case_studies/iot_dev.hpp"
#include "../../case_studies/lukasz.hpp"
#include "../../case_studies/lukasz_or.hpp"
#include "../../case_studies/treasure_hunters.hpp"


void generate_files(ADtree *adtree, std::string model_name){
  // preprocess the adtree
  Preprocessing preprocessing(adtree);

  // get the preprocessing output
  std::vector<Preprocessing::Output> preprocessing_graphs =
      preprocessing.get_graphs();

  // print preprocessing info
  std::cout << "gcd: " << preprocessing.get_gcf() << std::endl;
  std::cout << "# preprocessing graphs: " << preprocessing_graphs.size()
            << std::endl;

  // printers
  TikzPrinter printer;
  TablePrinter table_printer;

  // compute minimal assignment for each defense strategy
  int index_graph=0;
  for (const Preprocessing::Output & output : preprocessing_graphs){
    std::vector<ProcessingGraph> flat_graphs = output.flat_graphs;

    // print preprocessing graph of the selected defense strategy
    std::ofstream preprocessing_file(model_name + "_preprocessing_graph_" + std::to_string(index_graph) + ".tex");
    if (preprocessing_file.is_open()) {
      printer.parser(preprocessing_file, output.preprocessed_graph, false);
      preprocessing_file.close();
    }

    // each preprocessed graph can contain several or combinations
    int index_assignment = 0;
    std::vector<MinimalAgentAssignment> candidates;
    for (const ProcessingGraph & graph : flat_graphs){
      MinimalAgentAssignment minimal_assignment(graph);
      candidates.push_back(minimal_assignment);

      // print minimal assignment graph
      std::ofstream minimal_file(model_name + "_graph_" + std::to_string(index_graph) +  "_minimal_assignment_" +  std::to_string(index_assignment) + ".tex");
      if (minimal_file.is_open()) {
        printer.parser(minimal_file, minimal_assignment.get_graph());
        minimal_file.close();
      }

      index_assignment++;
    }

    // choose only one assignment
    std::sort(candidates.begin(), candidates.end());
    MinimalAgentAssignment best_minimal_assignment = candidates.front();

    // print "best" minimal assignment information
    MinimalAgentAssignment::scheduling scheduling = best_minimal_assignment.get_scheduling();
    std::cout << "n: " << best_minimal_assignment.get_graph().get_n() << std::endl;
    std::cout << table_printer.ascii(best_minimal_assignment);

    index_graph++;
  }
}

TEST_CASE("Lukasz OR Preprocessing") {
  ADtree* my_tree = get_lukasz_or_example();
  generate_files(my_tree,"lukasz_or");
}

TEST_CASE("Lukasz Preprocessing") {
   ADtree* my_tree = get_lukasz_example();
  generate_files(my_tree,"lukasz");
}

TEST_CASE("Treasure Hunters Preprocessing") {
  ADtree* my_tree = get_treasure_hunters();
  generate_files(my_tree, "treasure_hunters");
}

TEST_CASE("Toy example Preprocessing") {
  ADtree* my_tree = get_icfem_2020_scaling_example();
  generate_files(my_tree, "toy_preprocessing");
}

TEST_CASE("iot-dev Preprocessing") {
  ADtree* my_tree = get_iot_dev();
  generate_files(my_tree, "iot_dev_preprocessing");
}
