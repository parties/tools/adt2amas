#include "automata.hpp"
#include <catch.hpp>
#include <iostream>
#include "parser/amas_imitator_visitor.hpp"

using namespace std;

/* TODO:
 * 1) Add test for get_info()
 * 2) Add implementation for for get_channels_id()
 * 3) When adding an automaton, channels must exists for each transition
 * belonging to it.
 */

TEST_CASE("An automata has channels", "[automata]") {
  Automata automata;
  Channel *chan = new Channel("a");

  REQUIRE(automata.get_channels().size() == 0);

  SECTION("a channel can be retrieved and added") {
    automata.add_channel(chan);

    REQUIRE(automata.get_channels().size() == 1);
    REQUIRE(*automata.get_channels().begin() == chan);
  }

  SECTION("channels with the same name are merged") {
    automata.add_channel(chan);

    Channel *chan_b = new Channel("b");
    automata.add_channel(chan_b);
    REQUIRE(automata.get_channels().size() == 2);

    Channel *chan_a = new Channel("a");
    automata.add_channel(chan_a);
    REQUIRE(automata.get_channels().size() == 2);
  }

  SECTION("a channel can be removed") {
    automata.add_channel(chan);
    REQUIRE(automata.get_channels().size() == 1);

    automata.remove_channel(chan);
    REQUIRE(automata.get_channels().size() == 0);
  }
}

TEST_CASE("An automata has one or more automaton", "[automata]") {
  Automata automata;
  Automaton *automaton = new Automaton(new State(StateType::Initial));

  REQUIRE(automata.get_automaton_ids().size() == 0);
  REQUIRE(automata.get_vector_automaton().size() == 0);

  SECTION("an automaton can be retrieved or added") {
    automata.add_automaton(automaton);

    REQUIRE(automata.get_automaton_ids().size() == 1);
    REQUIRE(automata.get_vector_automaton().size() == 1);

    REQUIRE(*automata.get_automaton_ids().begin() == automaton->get_id());
    REQUIRE(automata.get_vector_automaton()[0] == automaton);
  }

  SECTION("an automaton can be removed") {
    automata.add_automaton(automaton);

    REQUIRE(automata.get_automaton_ids().size() == 1);
    REQUIRE(automata.get_vector_automaton().size() == 1);

    automata.remove_automaton(automaton);

    REQUIRE(automata.get_automaton_ids().size() == 0);
    REQUIRE(automata.get_vector_automaton().size() == 0);
  }
}

TEST_CASE("A new automata", "[automata]") {
  Automata automata;

  SECTION("it has no any automaton") {
    REQUIRE(automata.get_automaton_ids().size() == 0);
    REQUIRE(automata.get_vector_automaton().size() == 0);
  }

  SECTION("it has no channels") {
    REQUIRE(automata.get_channels().size() == 0);
  }
}

TEST_CASE("An automata has an imitator representation", "[automata]") {
  AMASIMITATORVisitor visitor;
  Automata automata;

  /** Generate several automaton */
  for (int i = 0; i < 3; i++) {
    State *initial_s = new State(StateType::Initial);
    State *new_s = new State(StateType::Normal);
    State *new_s2 = new State(StateType::Normal);

    Automaton *automaton = new Automaton(initial_s);
    automaton->add_state(new_s);
    automaton->add_state(new_s2);
    automaton->add_transition(initial_s, new_s, new Label("t1"));
    automaton->add_transition(new_s, new_s2, new Label("t2"));

    automata.add_automaton(automaton);
  }

  automata.accept(visitor);
}
