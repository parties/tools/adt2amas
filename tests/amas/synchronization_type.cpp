#include "synchronization_type.hpp"
#include <catch.hpp>

TEST_CASE("Synchronization types have a string representation",
          "[synchronization type]") {
  SECTION("SynchronizationType::Send") {
    SynchronizationType send_type = SynchronizationType::Send;
    REQUIRE(to_string(send_type) == "!");
  }

  SECTION("SynchronizationType::Receive") {
    SynchronizationType receive_type = SynchronizationType::Receive;
    REQUIRE(to_string(receive_type) == "?");
  }
}
