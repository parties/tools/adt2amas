#include "channel.hpp"
#include <catch.hpp>

/* TODO
 * 1) Add test for get_info
 * 2) Add test for get_info_vector
 * 3) Change name by value
 */

TEST_CASE("A Channel has a value", "[channel]") {
  Channel chan;

  SECTION("it can be retrieved and modified") {
    chan.set_name("B");
    REQUIRE(chan.get_name() == "B");
  }
}

TEST_CASE("A Channel with no parameters", "[channel]") {
  Channel chan;

  SECTION("its label is empty") { REQUIRE(chan.get_name() == ""); }
}
