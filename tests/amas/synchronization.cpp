#include "synchronization.hpp"
#include <catch.hpp>

/* TODO
 * 1) Add tests for get_info()
 * 2) Add tests for get_info_vector()
 */

TEST_CASE("A synchronization has a channel") {
  Synchronization sync;

  SECTION("It can be retrieved and modified") {
    Channel *chan = new Channel("A");
    sync.set_channel(chan);

    REQUIRE(sync.get_channel() == chan);
  }
}

TEST_CASE("A synchronization has a synchronization type") {
  Synchronization sync;

  SECTION("It can be retrieved and modified") {
    SynchronizationType sync_type = SynchronizationType::Receive;
    sync.set_synchronization_type(sync_type);

    REQUIRE(sync.get_synchronization_type() == sync_type);
  }
}

TEST_CASE("An empty synchronization", "[synchronization]") {
  Synchronization sync;

  SECTION("Its default synchronization type is Send") {
    REQUIRE(sync.get_synchronization_type() == SynchronizationType::Send);
  }

  SECTION("Its default channel is empty") {
    REQUIRE(sync.get_channel() == nullptr);
  }
}
