#include "automaton.hpp"
#include <catch.hpp>
#include <iostream>
#include "parser/amas_imitator_visitor.hpp"

using namespace std;
using Catch::Matchers::Contains;

/* TODO
 * 1) Add test for get_info
 */

TEST_CASE("An automaton has states", "[automaton]") {
  State *initial_s = new State(StateType::Initial);
  State *new_state = new State();
  Automaton automaton(initial_s);

  REQUIRE(automaton.get_states().size() == 1);
  REQUIRE(automaton.get_state_ids().size() == 1);

  SECTION("a new state can be added") {
    automaton.add_state(new_state);

    REQUIRE(automaton.get_states().size() == 2);
    REQUIRE(automaton.get_state_ids().size() == 2);
  }

  SECTION("an existing state can be removed") {
    automaton.add_state(new_state);

    REQUIRE(automaton.get_states().size() == 2);
    REQUIRE(automaton.get_state_ids().size() == 2);

    REQUIRE_THROWS(automaton.remove_state(initial_s));

    automaton.remove_state(new_state);
    REQUIRE(automaton.get_states().size() == 1);
    REQUIRE(automaton.get_state_ids().size() == 1);
  }
}

TEST_CASE("An automaton has transitions", "[automaton]") {
  State *initial_s = new State(StateType::Initial);
  State *new_s = new State(StateType::Normal);

  Automaton automaton(initial_s);
  automaton.add_state(new_s);

  REQUIRE(automaton.get_transition_ids().size() == 0);
  REQUIRE(automaton.get_transitions().size() == 0);

  SECTION("a new transition can be added") {
    automaton.add_transition(initial_s, new_s, new Label("A"));

    REQUIRE(automaton.get_transition_ids().size() == 1);
    REQUIRE(automaton.get_transitions().size() == 1);

    Transition *t = *automaton.get_transitions().begin();
    REQUIRE(initial_s->get_transitions().size() == 1);
    REQUIRE(*initial_s->get_transitions().begin() == t);

    REQUIRE(t->get_source_state() == initial_s);
    REQUIRE(t->get_destination_state() == new_s);
  }

  SECTION("an existing transition can be removed") {
    automaton.add_transition(initial_s, new_s, new Label("A"));
    REQUIRE(automaton.get_transition_ids().size() == 1);
    REQUIRE(automaton.get_transitions().size() == 1);

    Transition *t = *automaton.get_transitions().begin();
    automaton.remove_transition(t);

    REQUIRE(automaton.get_transition_ids().size() == 0);
    REQUIRE(automaton.get_transitions().size() == 0);
  }

  SECTION("an existing transition can be removed by its ID") {
    automaton.add_transition(initial_s, new_s, new Label("A"));
    REQUIRE(automaton.get_transition_ids().size() == 1);
    REQUIRE(automaton.get_transitions().size() == 1);

    int t_id = *automaton.get_transition_ids().begin();
    automaton.remove_transition(t_id);

    REQUIRE(automaton.get_transition_ids().size() == 0);
    REQUIRE(automaton.get_transitions().size() == 0);
  }
}

TEST_CASE("An automaton has an initial state", "[automaton]") {
  State *initial_s = new State(StateType::Initial);
  Automaton automaton(initial_s);

  REQUIRE(automaton.get_initial_state() == initial_s);
  REQUIRE(automaton.get_state_ids().size() == 1);
  REQUIRE(automaton.get_states().size() == 1);

  SECTION("it must be of type StateType::Initial") {
    REQUIRE_THROWS_WITH(new Automaton(new State(StateType::Normal)),
                        Contains("must be Initial"));
  }

  SECTION("it can be retrieved and modified") {
    State *state = new State(StateType::Normal);
    automaton.set_initial_state(state);

    REQUIRE(automaton.get_initial_state() == state);
    REQUIRE(state->get_state_type() == StateType::Initial);
    REQUIRE(initial_s->get_state_type() == StateType::Normal);

    REQUIRE(automaton.get_state_ids().size() == 2);
    REQUIRE(automaton.get_states().size() == 2);
  }

  SECTION("it can be removed") {
    automaton.remove_initial_state();
    REQUIRE(automaton.get_initial_state() == nullptr);
    REQUIRE(automaton.get_state_ids().size() == 0);
    REQUIRE(automaton.get_states().size() == 0);
  }
};

TEST_CASE("A new automaton", "[automaton]") {
  State *initial_s = new State(StateType::Initial);
  Automaton automaton(initial_s);

  SECTION("it must have an initial state") {
    REQUIRE(automaton.get_initial_state() == initial_s);

    REQUIRE(automaton.get_states().size() == 1);
    REQUIRE(automaton.get_states()[0] == initial_s);

    REQUIRE(automaton.get_state_ids().size() == 1);
    REQUIRE(*automaton.get_state_ids().begin() == initial_s->get_id());
  }

  SECTION("it has no transitions") {
    REQUIRE(automaton.get_transitions().size() == 0);
    REQUIRE(automaton.get_transition_ids().size() == 0);
  }
}

TEST_CASE("An automaton has an imitator representation", "[automaton]") {
  AMASIMITATORVisitor visitor;

  State *initial_s = new State(StateType::Initial);
  State *new_s = new State(StateType::Normal);
  State *new_s2 = new State(StateType::Normal);

  Automaton automaton(initial_s);
  automaton.add_state(new_s);
  automaton.add_state(new_s2);
  automaton.add_transition(initial_s, new_s, new Label("t1"));
  automaton.add_transition(new_s, new_s2, new Label("t2"));

  automaton.accept(visitor);
}
