#include "state_type.hpp"
#include <catch.hpp>

TEST_CASE("State types have a string representation", "[State type]") {
  SECTION("StateType::Normal") {
    StateType normal_type = StateType::Normal;
    REQUIRE(to_string(normal_type) == "Normal");
  }

  SECTION("StateType::Initial") {
    StateType initial_type = StateType::Initial;
    REQUIRE(to_string(initial_type) == "Initial");
  }

  SECTION("StateType::Accepted") {
    StateType accepted_type = StateType::Accepted;
    REQUIRE(to_string(accepted_type) == "Accepted");
  }
}
