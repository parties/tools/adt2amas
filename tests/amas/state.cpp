#include "state.hpp"
#include <catch.hpp>
#include <iostream>
#include <vector>
#include "parser/amas_imitator_visitor.hpp"

using namespace std;

/* TODO:
 * 1) Change state type by a property of the state
 * 2) Add test for get_info()
 */

TEST_CASE("A state has a type", "[state]") {
  State state;

  SECTION("it can be retrieved and modified") {
    StateType s_type = StateType::Initial;
    state.set_state_type(s_type);

    REQUIRE(state.get_state_type() == s_type);
  }
}

TEST_CASE("A state has transitions", "[state]") {
  State state;

  SECTION("it can be retrieved and modified") {
    vector<Transition *> t{new Transition(), new Transition()};
    state.set_transitions(t);

    REQUIRE(state.get_transitions().size() == 2);
  }
}

TEST_CASE("A new state with no parameters", "[state]") {
  State state;

  SECTION("its a normal state by default") {
    REQUIRE(state.get_state_type() == StateType::Normal);
  }

  SECTION("it has no outgoing transitions") {
    REQUIRE(state.get_transitions().empty());
  }
};

TEST_CASE("A state has an imitator representation", "[state]") {
  AMASIMITATORVisitor visitor;

  SECTION("an empty state") {
    State state;
    state.accept(visitor);
    string state_id = to_string(state.get_id());
    REQUIRE(visitor.get_imitator() ==
            "loc l" + state_id + ": while True wait {}\n");
  }

  SECTION("a state with transitions") {
    State *source_state = new State();

    State *target_state_one = new State();
    State *target_state_two = new State();

    Transition *transition_one =
        new Transition(source_state, target_state_one, new Label("t1"));
    Transition *transition_two =
        new Transition(source_state, target_state_two, new Label("t2"));

    source_state->set_transitions({transition_one, transition_two});

    source_state->accept(visitor);
  }
}
