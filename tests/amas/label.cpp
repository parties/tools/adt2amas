#include "label.hpp"
#include <catch.hpp>

/* TODO
 * 1) Add test for get_info
 * 2) Add test for get_info_vector
 * 3) Change name by value
 */

TEST_CASE("A Label has a value", "[label]") {
  Label l;

  SECTION("it can be retrieved and modified") {
    l.set_name("B");
    REQUIRE(l.get_name() == "B");
  }
}

TEST_CASE("A Label with no parameters", "[label]") {
  Label l;

  SECTION("its label is empty") { REQUIRE(l.get_name() == ""); }
}
