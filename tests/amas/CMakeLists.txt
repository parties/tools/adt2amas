# Specify the minimum version for CMake
cmake_minimum_required(VERSION 3.8.0)

# project information
project(amas_tests CXX)

# Add the test files for the AMAS implementation
set(TEST_FILES label.cpp channel.cpp synchronization_type.cpp state_type.cpp synchronization.cpp state.cpp transition.cpp automaton.cpp automata.cpp)

add_test_suite("AMAS" amas_lib "${TEST_FILES}")
