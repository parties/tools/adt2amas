#include "transition.hpp"
#include <catch.hpp>
#include "constant.hpp"
#include "function.hpp"
#include "operators/addition.hpp"
#include "operators/multiplication.hpp"
#include "variable.hpp"

/* TODO:
 * 1) Add test for get_info()
 */

TEST_CASE("A transition has a source state", "[state]") {
  Transition t;
  State *source_s = new State();

  REQUIRE(t.get_source_state() == nullptr);
  REQUIRE(t.get_destination_state() == nullptr);

  SECTION("it can be retrieved and modified") {
    t.set_source_state(source_s);
    REQUIRE(t.get_source_state() == source_s);
    REQUIRE(t.get_destination_state() == nullptr);
  }
}

TEST_CASE("A transition has a target state", "[state]") {
  Transition t;
  State *target_s = new State();

  REQUIRE(t.get_destination_state() == nullptr);
  REQUIRE(t.get_source_state() == nullptr);

  SECTION("it can be retrieved and modified") {
    t.set_destination_state(target_s);
    REQUIRE(t.get_source_state() == nullptr);
    REQUIRE(t.get_destination_state() == target_s);
  }
}

TEST_CASE("A transition has an action", "[transition]") {
  Transition t;

  SECTION("it can be a label") {
    Action *label = new Label();
    t.set_action(label);
    REQUIRE(t.get_action() == label);
  }

  SECTION("it can be a synchronization action") {
    Action *sync_action = new Synchronization();
    t.set_action(sync_action);
    REQUIRE(t.get_action() == sync_action);
  }
}

TEST_CASE("A transition has updates", "[transition]") {
  Assignment *update = new Assignment("t", new Constant(10));
  Transition t(new State, new State, new Label("A"), {update});

  REQUIRE(t.get_updates().size() == 1);
  REQUIRE(t.get_updates()[0] == update);

  SECTION("updates can be retrieved and added") {
    Assignment *update_two = new Assignment("t", new Constant(20));
    t.set_updates({update, update_two});

    REQUIRE(t.get_updates().size() == 2);
    REQUIRE(t.get_updates()[1] == update_two);
  }
}

TEST_CASE("A new transition with no parameters", "[transition]") {
  Transition t;

  SECTION("it has no source state") {
    REQUIRE(t.get_source_state() == nullptr);
  }

  SECTION("it has no target state") {
    REQUIRE(t.get_destination_state() == nullptr);
  }

  SECTION("it has no action") { REQUIRE(t.get_action() == nullptr); }
}
