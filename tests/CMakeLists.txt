# Specify the minimum version for CMake
cmake_minimum_required(VERSION 3.8.0)

# project information
project(adt2amas_tests C CXX)

# Requires Catch2
set(CATCH_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../third-party/catch2)
add_library(catch INTERFACE)
target_include_directories(catch INTERFACE ${CATCH_INCLUDE_DIR})

function(add_test_suite NAME LIBRARIES FILES)
  foreach(TEST_SRC ${FILES})
    # Extract the filename without an extension (NAME_WE)
    get_filename_component(TEST_NAME ${TEST_SRC} NAME_WE)

    # Make test executable
    add_executable(${TEST_NAME} "${adt2amas_tests_SOURCE_DIR}/main.cpp" ${TEST_SRC})

    # Link to depedencies
    target_link_libraries(${TEST_NAME} catch ${LIBRARIES})

    # Add test to suite
    add_test(NAME "${NAME}:${TEST_NAME}" COMMAND ${TEST_NAME})
  endforeach()
endfunction()

# Add tests for the ADTree module
add_subdirectory(adtree)

# Add tests for the AMAS module
add_subdirectory(amas)

# Add tests for the algorithms
add_subdirectory(algorithms)

## Add test for the ADT2AMAS translator
set(TEST_FILES translator.cpp)
add_test_suite("adt2amas" adt2amas_lib "${TEST_FILES}")

# Add case studies
add_subdirectory(case_studies)
