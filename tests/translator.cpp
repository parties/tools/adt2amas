#include "translator.hpp"
#include <algorithm>
#include <catch.hpp>
#include <iostream>
#include "constant.hpp"
#include "leaf.hpp"

using namespace std;
using Catch::Matchers::Contains;

vector<pair<Action *, State *>> get_transition_relation(State *s) {
  vector<pair<Action *, State *>> targets;
  vector<Transition *> transitions = s->get_transitions();

  transform(transitions.begin(), transitions.end(), back_inserter(targets),
            [](Transition *t) {
              return make_pair(t->get_action(), t->get_destination_state());
            });

  return targets;
}

// global instance of the translator
Translator adt2amas;

TEST_CASE("Leaf translation", "[translator]") {
  Automata *amata = new Automata;
  Attribute *leaf_attribute = new Attribute("attr", 10, new Constant(0));
  TreeNode *l = new TreeNode("Leaf", NodeRole::Attack, "a", {leaf_attribute});

  adt2amas.leaf_to_automaton(l, amata);
  REQUIRE(amata->get_vector_automaton().size() == 1);
  REQUIRE(amata->get_automaton_ids().size() == 1);
  REQUIRE(amata->get_channels().size() == 2);

  Automaton *automaton = amata->get_vector_automaton()[0];

  SECTION("it has 3 states") { REQUIRE(automaton->get_states().size() == 3); }

  SECTION("it has 4 transitions") {
    REQUIRE(automaton->get_transitions().size() == 4);
  }
  SECTION("initial states has transitions to 2 states") {
    State *initial_s = automaton->get_initial_state();
    vector<Transition *> transitions = initial_s->get_transitions();

    REQUIRE(transitions.size() == 2);
  }

  SECTION("normal states has only 1 transition (a loop) ") {
    auto targets = get_transition_relation(automaton->get_initial_state());

    for (auto t : targets) {
      State *s = t.second;
      REQUIRE(s->get_transitions().size() == 1);
      REQUIRE((s->get_transitions()[0])->get_destination_state() == s);
    }
  }
}

TEST_CASE("Or Gate (no children) translation", "[translator]") {
  Automata *amata = new Automata;
  TreeNode *or_gate = new TreeNode;

  REQUIRE_THROWS_WITH(adt2amas.or_to_automaton(or_gate, amata),
                      Contains("two or more children"));
}

TEST_CASE("Or Gate translation", "[translator]") {
  Automata *amata = new Automata;
  TreeNode *or_gate = new TreeNode("OR", NodeRole::Attack, "OR");
  Leaf *right = new Leaf(NodeRole::Attack, "r");
  Leaf *left = new Leaf(NodeRole::Attack, "l");

  ADtree tree(or_gate);
  tree.add_child(left, or_gate);
  tree.add_child(right, or_gate);

  REQUIRE(or_gate->get_children().size() == 2);

  adt2amas.or_to_automaton(or_gate, amata);

  REQUIRE(amata->get_vector_automaton().size() == 1);
  REQUIRE(amata->get_automaton_ids().size() == 1);
  REQUIRE(amata->get_channels().size() == 6);

  Automaton *automaton = amata->get_vector_automaton()[0];

  SECTION("it has 5 states") {
    auto states = automaton->get_states();
    REQUIRE(states.size() == 5);
  }

  SECTION("it has 7 transitions") {
    auto transitions = automaton->get_transitions();
    REQUIRE(transitions.size() == 7);
  }

  SECTION("initial state has 3 transitions") {
    State *initial_s = automaton->get_initial_state();
    REQUIRE(initial_s->get_transitions().size() == 3);
  }
}

TEST_CASE("And Gate (no children) translation", "[translator]") {
  Automata *amata = new Automata;
  TreeNode *and_gate = new TreeNode;

  REQUIRE_THROWS_WITH(adt2amas.and_to_automaton(and_gate, amata),
                      Contains("two or more children"));
}

TEST_CASE("And Gate translation", "[translator]") {
  Automata *amata = new Automata;
  TreeNode *and_gate = new TreeNode("NAND", NodeRole::Attack, "NAND");
  Leaf *right = new Leaf(NodeRole::Attack, "r");
  Leaf *left = new Leaf(NodeRole::Attack, "l");

  ADtree tree(and_gate);
  tree.add_child(left, and_gate);
  tree.add_child(right, and_gate);

  REQUIRE(and_gate->get_children().size() == 2);

  adt2amas.and_to_automaton(and_gate, amata);

  REQUIRE(amata->get_vector_automaton().size() == 1);
  REQUIRE(amata->get_automaton_ids().size() == 1);

  REQUIRE(amata->get_channels().size() == 6);

  Automaton *automaton = amata->get_vector_automaton()[0];

  SECTION("it has 5 states") {
    auto states = automaton->get_states();
    REQUIRE(states.size() == 5);
  }

  SECTION("it has 7 transitions") {
    auto transitions = automaton->get_transitions();
    REQUIRE(transitions.size() == 7);
  }

  SECTION("initial state has 3 transitions") {
    State *initial_s = automaton->get_initial_state();
    REQUIRE(initial_s->get_transitions().size() == 3);
  }
}

TEST_CASE("NOr Gate (no children) translation", "[translator]") {
  Automata *amata = new Automata;
  TreeNode *nor_gate = new TreeNode;

  REQUIRE_THROWS_WITH(adt2amas.nor_to_automaton(nor_gate, amata),
                      Contains("have two children"));
}

TEST_CASE("NOr Gate translation", "[translator]") {
  Automata *amata = new Automata;
  TreeNode *nor_gate = new TreeNode("NOR", NodeRole::Attack, "NOR");
  Leaf *right = new Leaf(NodeRole::Attack, "r");
  Leaf *left = new Leaf(NodeRole::Defence, "l");

  ADtree tree(nor_gate);
  tree.add_child(left, nor_gate);
  tree.add_child(right, nor_gate);

  REQUIRE(nor_gate->get_children().size() == 2);

  adt2amas.nor_to_automaton(nor_gate, amata);

  REQUIRE(amata->get_vector_automaton().size() == 1);
  REQUIRE(amata->get_automaton_ids().size() == 1);
  REQUIRE(amata->get_channels().size() == 6);

  Automaton *automaton = amata->get_vector_automaton()[0];

  SECTION("it has 5 states") {
    auto states = automaton->get_states();
    REQUIRE(states.size() == 5);
  }

  SECTION("it has 7 transitions") {
    auto transitions = automaton->get_transitions();
    REQUIRE(transitions.size() == 7);
  }

  SECTION("initial state has 3 transitions") {
    State *initial_s = automaton->get_initial_state();
    REQUIRE(initial_s->get_transitions().size() == 3);
  }
}

TEST_CASE("NAnd Gate (no children) translation", "[translator]") {
  Automata *amata = new Automata;
  TreeNode *nand_gate = new TreeNode;

  REQUIRE_THROWS_WITH(adt2amas.nand_to_automaton(nand_gate, amata),
                      Contains("have two children"));
}

TEST_CASE("NAnd Gate translation", "[translator]") {
  Automata *amata = new Automata;
  TreeNode *nand_gate = new TreeNode("NAND", NodeRole::Attack, "NAND");
  Leaf *right = new Leaf(NodeRole::Attack, "r");
  Leaf *left = new Leaf(NodeRole::Defence, "l");

  ADtree tree(nand_gate);
  tree.add_child(left, nand_gate);
  tree.add_child(right, nand_gate);

  REQUIRE(nand_gate->get_children().size() == 2);

  adt2amas.nand_to_automaton(nand_gate, amata);

  REQUIRE(amata->get_vector_automaton().size() == 1);
  REQUIRE(amata->get_automaton_ids().size() == 1);
  REQUIRE(amata->get_channels().size() == 6);

  Automaton *automaton = amata->get_vector_automaton()[0];

  SECTION("it has 5 states") {
    auto states = automaton->get_states();
    REQUIRE(states.size() == 5);
  }

  SECTION("it has 7 transitions") {
    auto transitions = automaton->get_transitions();
    REQUIRE(transitions.size() == 7);
  }

  SECTION("initial state has 3 transitions") {
    State *initial_s = automaton->get_initial_state();
    REQUIRE(initial_s->get_transitions().size() == 3);
  }
}

TEST_CASE("SAnd Gate (no children) translation", "[translator]") {
  Automata *amata = new Automata;
  TreeNode *sand_gate = new TreeNode;

  REQUIRE_THROWS_WITH(adt2amas.sand_to_automaton(sand_gate, amata),
                      Contains("have two or more children"));
}

TEST_CASE("Sand Gate translation", "[translator]") {
  Automata *amata = new Automata;
  TreeNode *sand_gate = new TreeNode("SAND", NodeRole::Attack, "SAND");
  Leaf *right = new Leaf(NodeRole::Attack, "r");
  Leaf *left = new Leaf(NodeRole::Attack, "l");

  ADtree tree(sand_gate);
  tree.add_child(left, sand_gate);
  tree.add_child(right, sand_gate);

  REQUIRE(sand_gate->get_children().size() == 2);

  adt2amas.sand_to_automaton(sand_gate, amata);

  REQUIRE(amata->get_vector_automaton().size() == 1);
  REQUIRE(amata->get_automaton_ids().size() == 1);
  REQUIRE(amata->get_channels().size() == 6);

  Automaton *automaton = amata->get_vector_automaton()[0];

  SECTION("it has 5 states") {
    auto states = automaton->get_states();
    REQUIRE(states.size() == 5);
  }

  SECTION("it has 7 transitions") {
    auto transitions = automaton->get_transitions();
    REQUIRE(transitions.size() == 7);
  }

  SECTION("initial state has 2 transitions") {
    State *initial_s = automaton->get_initial_state();
    REQUIRE(initial_s->get_transitions().size() == 2);
  }
}

TEST_CASE("SNand Gate (no children) translation", "[translator]") {
  Automata *amata = new Automata;
  TreeNode *snand_gate = new TreeNode;

  REQUIRE_THROWS_WITH(adt2amas.snand_to_automaton(snand_gate, amata),
                      Contains("have two children"));
}

TEST_CASE("SNand Gate translation", "[translator]") {
  Automata *amata = new Automata;
  TreeNode *snand_gate = new TreeNode("SNAND", NodeRole::Attack, "SNAND");
  Leaf *right = new Leaf(NodeRole::Attack, "r");
  Leaf *left = new Leaf(NodeRole::Attack, "l");

  ADtree tree(snand_gate);
  tree.add_child(left, snand_gate);
  tree.add_child(right, snand_gate);

  REQUIRE(snand_gate->get_children().size() == 2);

  adt2amas.snand_to_automaton(snand_gate, amata);

  REQUIRE(amata->get_vector_automaton().size() == 1);
  REQUIRE(amata->get_automaton_ids().size() == 1);

  REQUIRE(amata->get_channels().size() == 6);

  Automaton *automaton = amata->get_vector_automaton()[0];

  SECTION("it has 5 states") {
    auto states = automaton->get_states();
    REQUIRE(states.size() == 5);
  }

  SECTION("it has 7 transitions") {
    auto transitions = automaton->get_transitions();
    REQUIRE(transitions.size() == 7);
  }

  SECTION("initial state has 2 transitions") {
    State *initial_s = automaton->get_initial_state();
    REQUIRE(initial_s->get_transitions().size() == 2);
  }
}
