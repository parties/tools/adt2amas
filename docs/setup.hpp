/** Group definitions. */

/**
 * @defgroup adtree Attack-Defence Tree
 */

/**
 * @defgroup adtree_node ADTree Nodes
 * @ingroup adtree
 */

/**
 * @defgroup adtree_attribute Node's Attributes
 * @ingroup adtree
 */

/**
 * @defgroup adtree_visitor Algorithms
 * @ingroup adtree
 */

/**
 * @defgroup eamas Extended Asynchronous Multi-Agents Systems
 */

/**
 * @defgroup eamas_action Transition's Actions
 * @ingroup eamas
 */

/**
 * @defgroup eamas_visitor Parsers
 * @ingroup eamas
 */

/**
 * @defgroup linear_expr Linear Expression
 */

/**
 * @defgroup linear_expr_operator Operators
 * @ingroup linear_expr
 */

/**
 * @defgroup linear_expr_visitor Parsers
 * @ingroup linear_expr
 */

/**
 * @defgroup adt2amas Translator ADTree into EAMAS
 */
