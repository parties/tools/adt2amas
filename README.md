# ADT2AMAS

Tool for transforming Attack-Defense Trees (ADTrees) into Asynchronous
Multi-Agent Systems (AMAS).

# Dependencies

- `cmake`
- `doxygen` (for documentation)

# Build

1. `mkdir build`
2. `cd build`
3. `cmake ..`
4. `make`
5. `make install`

# Run

Once installed, the binary `adt2amas` will be in the `assets` folder. That is:

1. `cd ..` # if you are in the `build` folder
2. `cd assets`
3. `./adt2amas`

# Generate Documentation

To generate the documentation you need to install
[Graphviz](http://www.graphviz.org/) and replace the step 3 above by `cmake
.. -DBUILD_DOCS=ON`.

Once installed, the documentation will be in the `assets` folder.

# Usage

```
λ> ./adt2amas --help
Attack-Defence Tree tool
Usage: ./adt2amas [OPTIONS] SUBCOMMAND

Options:
  -h,--help                   Print this help message and exit

Subcommands:
  transform                   ADTree transformation into EAMAS
  minimal                     Schedule for minimal attack time and number of agents
```

