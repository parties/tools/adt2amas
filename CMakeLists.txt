# Specify the minimum version for CMake
cmake_minimum_required(VERSION 3.8.0)

# Project information
project(adt2amas CXX)

# Require C++17
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

# Installing folder
set(INSTALL_FOLDER ${adt2amas_SOURCE_DIR}/assets)

# Source folder
add_subdirectory(src)

# Test folder
option(BUILD_TESTS "Build Tests" ON)
if(BUILD_TESTS)
  enable_testing()
  add_subdirectory(tests)
endif(BUILD_TESTS)

# Documentation folder
option(BUILD_DOCS "Build Documentation")
if(BUILD_DOCS)
  add_subdirectory(docs)
endif(BUILD_DOCS)
