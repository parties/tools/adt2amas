#ifndef CASE_STUDIES_LUKASZ_OR_HPP_
#define CASE_STUDIES_LUKASZ_OR_HPP_

#include "../src/adtree/ad_tree.hpp"
#include "../src/adtree/factory.hpp"
#include "../src/adtree/gates/or.hpp"
#include "../src/adtree/leaf.hpp"

ADtree* get_lukasz_or_example() {
  Leaf* c = new Leaf(NodeRole::Attack, "c", leaf_factory_attributes(0, 6));

  Leaf* f = new Leaf(NodeRole::Attack, "f", leaf_factory_attributes(0, 5));
  Leaf* g = new Leaf(NodeRole::Attack, "g", leaf_factory_attributes(0, 5));
  Leaf* h = new Leaf(NodeRole::Attack, "h", leaf_factory_attributes(0, 5));

  Leaf* i = new Leaf(NodeRole::Attack, "i", leaf_factory_attributes(0, 2));
  Leaf* j = new Leaf(NodeRole::Attack, "j", leaf_factory_attributes(0, 2));
  Leaf* k = new Leaf(NodeRole::Attack, "k", leaf_factory_attributes(0, 2));
  Leaf* l = new Leaf(NodeRole::Attack, "l", leaf_factory_attributes(0, 2));
  Leaf* m = new Leaf(NodeRole::Attack, "m", leaf_factory_attributes(0, 2));

  Or* b = new Or(NodeRole::Attack, "b", or_factory_attributes(0, 0, "b"));
  And* a = new And(NodeRole::Attack, "a", and_factory_attributes(0, 0, {b, c}));
  And* d =
      new And(NodeRole::Attack, "d", and_factory_attributes(0, 0, {f, g, h}));
  And* e = new And(NodeRole::Attack, "e",
                   and_factory_attributes(0, 0, {i, j, k, l, m}));

  ADtree* toy_example = new ADtree(a);

  toy_example->add_child(b, a);
  toy_example->add_child(c, a);

  toy_example->add_child(d, b);
  toy_example->add_child(e, b);

  toy_example->add_child(f, d);
  toy_example->add_child(g, d);
  toy_example->add_child(h, d);

  toy_example->add_child(i, e);
  toy_example->add_child(j, e);
  toy_example->add_child(k, e);
  toy_example->add_child(l, e);
  toy_example->add_child(m, e);

  return toy_example;
}

#endif  // CASE_STUDIES_LUKASZ_OR_HPP_
