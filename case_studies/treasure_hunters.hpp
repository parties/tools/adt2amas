#ifndef CASE_STUDIES_TREASURE_HUNTERS_HPP_
#define CASE_STUDIES_TREASURE_HUNTERS_HPP_

#include "../src/adtree/ad_tree.hpp"
#include "../src/adtree/factory.hpp"
#include "../src/adtree/gates/and.hpp"
#include "../src/adtree/gates/nand.hpp"
#include "../src/adtree/gates/or.hpp"
#include "../src/adtree/gates/sand.hpp"
#include "../src/adtree/leaf.hpp"

ADtree* get_treasure_hunters() {
  Leaf* b = new Leaf(NodeRole::Attack, "b", leaf_factory_attributes(500, 60));
  Leaf* f = new Leaf(NodeRole::Attack, "f", leaf_factory_attributes(100, 120));
  Leaf* h = new Leaf(NodeRole::Attack, "h", leaf_factory_attributes(500, 3));
  Leaf* e = new Leaf(NodeRole::Attack, "e", leaf_factory_attributes(0, 10));
  Leaf* p = new Leaf(NodeRole::Defence, "p", leaf_factory_attributes(100, 10));

  Or* GA = new Or(NodeRole::Attack, "GA", or_factory_attributes(0, 0, "GA"));

  And* ST =
      new And(NodeRole::Attack, "ST", and_factory_attributes(0, 2, {b, f}));

  Sand* TF =
      new Sand(NodeRole::Attack, "TF", sand_factory_attributes(0, 0, {ST, GA}));

  Inequality* TS_condition =
      new Inequality(new Constant(10),
                     new Addition({new Constant(2), new Variable("time_GA")}),
                     InequalityOperator::Greater);
  Nand* TS = new Nand(NodeRole::Attack, "TS", nand_factory_attributes(0, 0, TF),
                      TS_condition);

  ADtree* treasure_hunter = new ADtree(TS);
  treasure_hunter->add_child(TF, TS);
  treasure_hunter->add_child(p, TS);
  treasure_hunter->add_child(ST, TF);
  treasure_hunter->add_child(GA, TF);
  treasure_hunter->add_child(b, ST);
  treasure_hunter->add_child(f, ST);
  treasure_hunter->add_child(h, GA);
  treasure_hunter->add_child(e, GA);

  return treasure_hunter;
}

#endif  // CASE_STUDIES_TREASURE_HUNTERS_HPP_
