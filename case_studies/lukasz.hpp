#ifndef CASE_STUDIES_LUKASZ_HPP_
#define CASE_STUDIES_LUKASZ_HPP_

#include "../src/adtree/ad_tree.hpp"
#include "../src/adtree/factory.hpp"
#include "../src/adtree/gates/and.hpp"
#include "../src/adtree/gates/sand.hpp"
#include "../src/adtree/leaf.hpp"

ADtree* get_lukasz_example() {
  Leaf* b = new Leaf(NodeRole::Attack, "b", leaf_factory_attributes(0, 100));
  Leaf* d = new Leaf(NodeRole::Attack, "d", leaf_factory_attributes(0, 1));
  Leaf* e = new Leaf(NodeRole::Attack, "e", leaf_factory_attributes(0, 1));
  Leaf* f = new Leaf(NodeRole::Attack, "f", leaf_factory_attributes(0, 1));
  Leaf* g = new Leaf(NodeRole::Attack, "g", leaf_factory_attributes(0, 1));
  Leaf* h = new Leaf(NodeRole::Attack, "h", leaf_factory_attributes(0, 1));

  And* c =
        new And(NodeRole::Attack, "c", and_factory_attributes(0, 0, {d,e,f,g,h}));
  Sand* a =
      new Sand(NodeRole::Attack, "a", sand_factory_attributes(0, 0, {b, c}));

  ADtree* toy_example = new ADtree(a);

  toy_example->add_child(b, a);
  toy_example->add_child(c, a);

  toy_example->add_child(d, c);
  toy_example->add_child(e, c);
  toy_example->add_child(f, c);
  toy_example->add_child(g, c);
  toy_example->add_child(h, c);

  return toy_example;
}

#endif  // CASE_STUDIES_LUKASZ_HPP_
