#ifndef CASE_STUDIES_GAIN_ADMIN_HPP_
#define CASE_STUDIES_GAIN_ADMIN_HPP_

#include "ad_tree.hpp"
#include "factory.hpp"
#include "gates/nand.hpp"
#include "gates/or.hpp"
#include "gates/sand.hpp"
#include "gates/snand.hpp"
#include "leaf.hpp"

/**
 * Build the ADtree of the gain_admin example
 */
ADtree* get_gain_admin() {
  Or* OAP = new Or(NodeRole::Attack, "OAP", or_factory_attributes(0, 0, "OAP"));
  Or* ACLI =
      new Or(NodeRole::Attack, "ACLI", or_factory_attributes(0, 2, "ACLI"));
  Leaf* co =
      new Leaf(NodeRole::Attack, "co", leaf_factory_attributes(4000, 5760));

  Or* ECC = new Or(NodeRole::Attack, "ECC", or_factory_attributes(0, 0, "ECC"));
  Nand* ECCS =
      new Nand(NodeRole::Attack, "ECCS", nand_factory_attributes(0, 60, ECC));
  Leaf* bcc =
      new Leaf(NodeRole::Attack, "bcc", leaf_factory_attributes(6000, 2880));
  Leaf* ccg =
      new Leaf(NodeRole::Attack, "ccg", leaf_factory_attributes(100, 7200));
  Leaf* scr =
      new Leaf(NodeRole::Defence, "scr", leaf_factory_attributes(5000, 0));

  Or* GSAP =
      new Or(NodeRole::Attack, "GSAP", or_factory_attributes(0, 0, "GSAP"));

  Leaf* opf =
      new Leaf(NodeRole::Attack, "opf", leaf_factory_attributes(100, 4320));
  Leaf* fgp =
      new Leaf(NodeRole::Attack, "fgp", leaf_factory_attributes(0, 1440));
  Sand* GAP = new Sand(NodeRole::Attack, "GAP",
                       sand_factory_attributes(0, 10, {opf, fgp}));
  Snand* GAPS =
      new Snand(NodeRole::Attack, "GAPS", snand_factory_attributes(0, 2, GAP));

  Leaf* tla = new Leaf(NodeRole::Defence, "tla", leaf_factory_attributes(5, 1));

  Leaf* bsa =
      new Leaf(NodeRole::Attack, "bsa", leaf_factory_attributes(500, 20160));
  Leaf* vsa =
      new Leaf(NodeRole::Attack, "vsa", leaf_factory_attributes(20, 2880));
  Leaf* sat = new Leaf(NodeRole::Attack, "sat", leaf_factory_attributes(0, 30));
  Sand* LSA = new Sand(NodeRole::Attack, "LSA",
                       sand_factory_attributes(0, 0, {bsa, vsa, sat}));
  Nand* LSAS =
      new Nand(NodeRole::Attack, "LSAS", nand_factory_attributes(0, 0, LSA));

  Leaf* nv = new Leaf(NodeRole::Defence, "nv", leaf_factory_attributes(0, 0));

  Leaf* th =
      new Leaf(NodeRole::Attack, "th", leaf_factory_attributes(100, 4320));
  Nand* TSA =
      new Nand(NodeRole::Attack, "TSA", nand_factory_attributes(0, 0, th));

  Or* DTH =
      new Or(NodeRole::Defence, "DTH", or_factory_attributes(0, 0, "DTH"));
  Leaf* wd =
      new Leaf(NodeRole::Defence, "wd", leaf_factory_attributes(2000, 5));
  Leaf* efw =
      new Leaf(NodeRole::Defence, "efw", leaf_factory_attributes(3000, 0));

  Leaf* csa =
      new Leaf(NodeRole::Attack, "csa", leaf_factory_attributes(5000, 7200));

  ADtree* gain_admin = new ADtree(OAP);

  gain_admin->add_child(ACLI, OAP);
  gain_admin->add_child(GSAP, OAP);

  gain_admin->add_child(co, ACLI);
  gain_admin->add_child(ECCS, ACLI);

  gain_admin->add_child(ECC, ECCS);
  gain_admin->add_child(scr, ECCS);

  gain_admin->add_child(bcc, ECC);
  gain_admin->add_child(ccg, ECC);

  gain_admin->add_child(GAPS, GSAP);
  gain_admin->add_child(LSAS, GSAP);
  gain_admin->add_child(TSA, GSAP);
  gain_admin->add_child(csa, GSAP);

  gain_admin->add_child(GAP, GAPS);
  gain_admin->add_child(tla, GAPS);

  gain_admin->add_child(opf, GAP);
  gain_admin->add_child(fgp, GAP);

  gain_admin->add_child(LSA, LSAS);
  gain_admin->add_child(nv, LSAS);

  gain_admin->add_child(bsa, LSA);
  gain_admin->add_child(vsa, LSA);
  gain_admin->add_child(sat, LSA);

  gain_admin->add_child(th, TSA);
  gain_admin->add_child(DTH, TSA);

  gain_admin->add_child(wd, DTH);
  gain_admin->add_child(efw, DTH);

  return gain_admin;
}

#endif  // CASE_STUDIES_GAIN_ADMIN_HPP_
