#ifndef CASE_STUDIES_FORESTALL_HPP_
#define CASE_STUDIES_FORESTALL_HPP_

#include "../src/adtree/ad_tree.hpp"
#include "../src/adtree/factory.hpp"
#include "../src/adtree/gates/nand.hpp"
#include "../src/adtree/gates/or.hpp"
#include "../src/adtree/gates/sand.hpp"
#include "../src/adtree/gates/snand.hpp"
#include "../src/adtree/leaf.hpp"

/**
 * Build the ADtree of the forestall example
 */
ADtree* get_forestall() {
  Leaf* icp =
      new Leaf(NodeRole::Attack, "icp", leaf_factory_attributes(2000, 21600));
  Leaf* dtm =
      new Leaf(NodeRole::Attack, "dtm", leaf_factory_attributes(1000, 7200));
  Or* SC = new Or(NodeRole::Attack, "SC", or_factory_attributes(0, 0, "SC"));
  Sand* FS = new Sand(NodeRole::Attack, "FS",
                      sand_factory_attributes(0, 14400, {SC, icp, dtm}));

  Leaf* bp =
      new Leaf(NodeRole::Attack, "bp", leaf_factory_attributes(2000, 21600));
  Leaf* psc =
      new Leaf(NodeRole::Attack, "psc", leaf_factory_attributes(0, 10080));
  Sand* BRB = new Sand(NodeRole::Attack, "BRB",
                       sand_factory_attributes(0, 4320, {bp, psc}));

  Leaf* hh =
      new Leaf(NodeRole::Attack, "hh", leaf_factory_attributes(1000, 28800));
  Leaf* sb = new Leaf(NodeRole::Attack, "sb", leaf_factory_attributes(0, 0));
  Leaf* heb =
      new Leaf(NodeRole::Attack, "heb", leaf_factory_attributes(0, 4320));
  Sand* NA = new Sand(NodeRole::Attack, "NA",
                      sand_factory_attributes(0, 1440, {hh, sb, heb}));
  Snand* NAS =
      new Snand(NodeRole::Attack, "NAS", snand_factory_attributes(0, 0, NA));

  Leaf* hr =
      new Leaf(NodeRole::Attack, "hr", leaf_factory_attributes(4000, 14400));
  Leaf* reb =
      new Leaf(NodeRole::Attack, "reb", leaf_factory_attributes(500, 4320));
  Leaf* rfc = new Leaf(NodeRole::Attack, "rfc", leaf_factory_attributes(0, 0));
  Sand* PR = new Sand(NodeRole::Attack, "PR",
                      sand_factory_attributes(0, 0, {hr, reb, rfc}));
  Nand* PRS =
      new Nand(NodeRole::Attack, "PRS", nand_factory_attributes(0, 0, PR));

  Leaf* id =
      new Leaf(NodeRole::Defence, "id", leaf_factory_attributes(200, 1440));

  Leaf* scr =
      new Leaf(NodeRole::Defence, "scr", leaf_factory_attributes(5000, 0));

  ADtree* forestall = new ADtree(FS);

  forestall->add_child(SC, FS);
  forestall->add_child(icp, FS);
  forestall->add_child(dtm, FS);

  forestall->add_child(BRB, SC);
  forestall->add_child(NAS, SC);
  forestall->add_child(PRS, SC);

  forestall->add_child(bp, BRB);
  forestall->add_child(psc, BRB);

  forestall->add_child(NA, NAS);
  forestall->add_child(id, NAS);

  forestall->add_child(hh, NA);
  forestall->add_child(sb, NA);
  forestall->add_child(heb, NA);

  forestall->add_child(PR, PRS);
  forestall->add_child(scr, PRS);

  forestall->add_child(hr, PR);
  forestall->add_child(reb, PR);
  forestall->add_child(rfc, PR);

  return forestall;
}

#endif  // CASE_STUDIES_FORESTALL_HPP_
