#ifndef CASE_STUDIES_IOT_DEV_HPP_
#define CASE_STUDIES_IOT_DEV_HPP_

#include "../src/adtree/ad_tree.hpp"
#include "../src/adtree/factory.hpp"
#include "../src/adtree/gates/and.hpp"
#include "../src/adtree/gates/nand.hpp"
#include "../src/adtree/gates/or.hpp"
#include "../src/adtree/gates/sand.hpp"
#include "../src/adtree/gates/snand.hpp"
#include "../src/adtree/leaf.hpp"

/**
 * Build the ADtree of the iot_dev example
 */
ADtree* get_iot_dev() {
  Leaf* esv =
      new Leaf(NodeRole::Attack, "esv", leaf_factory_attributes(10, 60));

  Leaf* rms =
      new Leaf(NodeRole::Attack, "rms", leaf_factory_attributes(100, 30));

  Leaf* inc = new Leaf(NodeRole::Defence, "inc", leaf_factory_attributes(5, 1));

  Or* CPN = new Or(NodeRole::Attack, "CPN", or_factory_attributes(0, 0, "CPN"));

  Leaf* gc =
      new Leaf(NodeRole::Attack, "gc", leaf_factory_attributes(100, 600));

  Leaf* tla = new Leaf(NodeRole::Defence, "tla", leaf_factory_attributes(5, 1));

  Nand* GVC =
      new Nand(NodeRole::Attack, "GVC", nand_factory_attributes(0, 0, gc));

  And* APN = new And(NodeRole::Attack, "APN",
                     and_factory_attributes(0, 3, {CPN, GVC}));

  Snand* APNS =
      new Snand(NodeRole::Attack, "APNS", snand_factory_attributes(0, 1, APN));

  Sand* CIoTD = new Sand(NodeRole::Attack, "CIoTD",
                         sand_factory_attributes(0, 0, {APNS, esv, rms}));

  Leaf* flp =
      new Leaf(NodeRole::Attack, "flp", leaf_factory_attributes(10, 60));

  Leaf* sma =
      new Leaf(NodeRole::Attack, "sma", leaf_factory_attributes(50, 30));

  Sand* AL = new Sand(NodeRole::Attack, "AL",
                      sand_factory_attributes(0, 0, {flp, sma}));

  Leaf* fw = new Leaf(NodeRole::Attack, "fw", leaf_factory_attributes(10, 300));

  Leaf* bwk =
      new Leaf(NodeRole::Attack, "bwk", leaf_factory_attributes(100, 120));

  Sand* AW = new Sand(NodeRole::Attack, "AW",
                      sand_factory_attributes(0, 0, {fw, bwk}));

  ADtree* iot_dev = new ADtree(CIoTD);

  iot_dev->add_child(APNS, CIoTD);
  iot_dev->add_child(esv, CIoTD);
  iot_dev->add_child(rms, CIoTD);

  iot_dev->add_child(APN, APNS);
  iot_dev->add_child(inc, APNS);

  iot_dev->add_child(CPN, APN);
  iot_dev->add_child(GVC, APN);

  iot_dev->add_child(AL, CPN);
  iot_dev->add_child(AW, CPN);

  iot_dev->add_child(flp, AL);
  iot_dev->add_child(sma, AL);

  iot_dev->add_child(fw, AW);
  iot_dev->add_child(bwk, AW);

  iot_dev->add_child(gc, GVC);
  iot_dev->add_child(tla, GVC);

  return iot_dev;
}

#endif  // CASE_STUDIES_IOT_DEV_HPP_
