#ifndef CASE_STUDIES_TOY_EXAMPLE_HPP_
#define CASE_STUDIES_TOY_EXAMPLE_HPP_

#include "../src/adtree/ad_tree.hpp"
#include "../src/adtree/factory.hpp"
#include "../src/adtree/gates/and.hpp"
#include "../src/adtree/leaf.hpp"

ADtree* get_icfem_2020_scaling_example() {
  Leaf* l1 = new Leaf(NodeRole::Attack, "l1", leaf_factory_attributes(0, 1));
  Leaf* l2 = new Leaf(NodeRole::Attack, "l2", leaf_factory_attributes(0, 1));
  Leaf* l3 = new Leaf(NodeRole::Attack, "l3", leaf_factory_attributes(0, 1));
  Leaf* l4 = new Leaf(NodeRole::Attack, "l4", leaf_factory_attributes(0, 1));
  Leaf* l5 = new Leaf(NodeRole::Attack, "l5", leaf_factory_attributes(0, 1));
  Leaf* l6 = new Leaf(NodeRole::Attack, "l6", leaf_factory_attributes(0, 1));
  Leaf* l7 = new Leaf(NodeRole::Attack, "l7", leaf_factory_attributes(0, 1));
  Leaf* l8 = new Leaf(NodeRole::Attack, "l8", leaf_factory_attributes(0, 1));

  And* A1 =
      new And(NodeRole::Attack, "A1", and_factory_attributes(0, 1, {l1, l2}));
  And* A2 =
      new And(NodeRole::Attack, "A2", and_factory_attributes(0, 1, {l3, l4}));
  And* A3 =
      new And(NodeRole::Attack, "A3", and_factory_attributes(0, 1, {l5, l6}));
  And* A4 =
      new And(NodeRole::Attack, "A4", and_factory_attributes(0, 1, {A1, A2}));
  And* A5 =
      new And(NodeRole::Attack, "A5", and_factory_attributes(0, 1, {A3, l7}));
  And* A6 =
      new And(NodeRole::Attack, "A6", and_factory_attributes(0, 1, {A4, A5}));
  And* A7 =
      new And(NodeRole::Attack, "A7", and_factory_attributes(0, 1, {A6, l8}));

  ADtree* toy_example = new ADtree(A7);

  toy_example->add_child(A6, A7);
  toy_example->add_child(l8, A7);

  toy_example->add_child(A4, A6);
  toy_example->add_child(A5, A6);

  toy_example->add_child(A1, A4);
  toy_example->add_child(A2, A4);

  toy_example->add_child(A3, A5);
  toy_example->add_child(l7, A5);

  toy_example->add_child(l1, A1);
  toy_example->add_child(l2, A1);

  toy_example->add_child(l3, A2);
  toy_example->add_child(l4, A2);

  toy_example->add_child(l5, A3);
  toy_example->add_child(l6, A3);

  return toy_example;
}

#endif  // CASE_STUDIES_TOY_EXAMPLE_HPP_
