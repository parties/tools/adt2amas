#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/* structures and types definitions */
typedef enum nodeType { leaf, OR, AND } nodetype;

typedef struct adtNode {
  nodetype type;             /* type of node */
  int number;                /* number of the node of this type */
  char name[15];             /* node name */
  int depth;                 /* node depth */
  int time;
  struct adtNode *parent;    /* parent */
  struct adtNode *children;  /* first child */
  struct adtNode *nextchild; /* next sibling */
  struct adtNode *nextnode;  /* next node in the ADT */
} adtnode, *adtnodeptr;

/* functions defined in this program */
void createADT();
adtnodeptr getRoot();
char* type2string();
void generateFile();

/* Global variables used for the characteristics of the generation */
int adtwidth = 0, adtdepth = 0,
    adtchildren = 0; /* parameters for the generation */
nodetype adttype = leaf;
char thefilename[20] = {'\0'}; /* the file to write */
adtnodeptr theADT = NULL;      /* the first node of the ADT */
char cmdline[100];

/* main program */
int main(int argc, char *argv[]) {
  /* check the options passed */
  int opt, count;

  for (count = 0; count < argc; count++) {
    strcat(cmdline, argv[count]);
    strcat(cmdline, " ");
  }
  while ((opt = getopt(argc, argv, "w:d:c:f:t:")) != -1) {
    switch (opt) {
      case 'w':
        adtwidth = atoi(optarg);
        break;
      case 'd':
        adtdepth = atoi(optarg);
        break;
      case 'c':
        adtchildren = atoi(optarg);
        break;
      case 't':
        if (!strcmp(optarg, "OR"))
          adttype = OR;
        else if (!strcmp(optarg, "AND"))
          adttype = AND;
        else {
          printf("intermediate nodes must be of type AND or OR\n");
          return 1;
        }
        break;
      case 'f':
        strcpy(thefilename, optarg);
        break;
      case ':':
        printf("option needs a value\n");
        return 1;
      case '?':
        printf("unknown option: %c\n", optopt);
        return 1;
    }
  }
  // optind is for the extra arguments
  // which are not parsed
  for (; optind < argc; optind++) {
    printf("extra arguments: %s\n", argv[optind]);
  }

  if (adtwidth == 0) {
    printf("using default width 2\n");
    adtwidth = 2;
  }
  if (adtdepth == 0) {
    printf("using default depth 2\n");
    adtdepth = 2;
  }
  if (adtchildren == 0) {
    printf("using default of 2 children per node\n");
    adtchildren = 2;
  }
  if (adttype == leaf) {
    printf("using default node type AND\n");
    adttype = AND;
  }
  if (!strcmp(thefilename, "")) {
    printf("A file name is needed\n");
    return 1;
  }
  printf("Generating ADT of width %d, depth %d with type ", adtwidth, adtdepth);
  switch (adttype) {
    case leaf:
      return 1;
    case OR:
      printf("OR");
      break;
    case AND:
      printf("AND");
      break;
  }
  printf(" and %d children in file %s\n", adtchildren, thefilename);
  createADT();
  generateFile();
  return 0;
} /* end main */

/* create the ADT structure with the entry point in theADT global variable */
void createADT() {
  int lastleafnum = 0, lastnodenum = 0;
  int thedepth = adtdepth, thewidth, thechildren;
  adtnodeptr lastnode = NULL, thenode, thechild;
  bool morenodes;

  /* create the leaf nodes at maximum depth */
  for (thewidth = adtwidth; thewidth > 0; thewidth--) {
    /* create a leaf node */
    thenode = (adtnodeptr)malloc(sizeof(adtnode));
    thenode->type = leaf;
    thenode->time = 1;
    lastleafnum++;
    thenode->number = lastleafnum;
    sprintf(thenode->name, "leaf%d", thenode->number);
    thenode->depth = thedepth;
    thenode->parent = NULL;
    thenode->children = NULL;
    thenode->nextchild = NULL;
    thenode->nextnode = NULL;
    if (theADT == NULL) {
      /* this is the first node */
      theADT = thenode;
      lastnode = thenode;
      thenode->time = adtwidth - 1;
    } else {
      lastnode->nextnode = thenode;
      lastnode = thenode;
    }
  }
  /* create the intermediate nodes */
  for (thedepth--; thedepth >= 0; thedepth--) {
    morenodes = true;
    while (morenodes) {
      thenode = (adtnodeptr)malloc(sizeof(adtnode));
      thenode->type = adttype;
      lastnodenum++;
      thenode->number = lastnodenum;
      if (thenode->type == OR)
        sprintf(thenode->name, "OR%d", thenode->number);
      else
        sprintf(thenode->name, "AND%d", thenode->number);
      thenode->depth = thedepth;
      thenode->parent = NULL;
      thenode->children = NULL;
      thenode->nextchild = NULL;
      thenode->nextnode = NULL;
      thenode->time = 1;
      lastnode->nextnode = thenode;
      lastnode = thenode;
      /* find the children */
      thechild = theADT;
      for (thechildren = adtchildren; thechildren > 0; thechildren--) {
        for (; thechild != NULL &&
               (thechild->parent != NULL || thechild->depth != thedepth + 1);
             thechild = thechild->nextnode)
          ;
        if (thechild != NULL) {
          /* a child node was found */
          thechild->parent = thenode;
          thechild->nextchild = thenode->children;
          thenode->children = thechild;
        } else {
          /* no available children were found, create a new leaf */
          thechild = (adtnodeptr)malloc(sizeof(adtnode));
          thechild->type = leaf;
          thechild->time = 1;
          lastleafnum++;
          thechild->number = lastleafnum;
          sprintf(thechild->name, "leaf%d", thechild->number);
          thechild->depth = thedepth + 1;
          thechild->parent = thenode;
          thechild->children = NULL;
          thechild->nextchild = thenode->children;
          thenode->children = thechild;
          thechild->nextnode = NULL;
          lastnode->nextnode = thechild;
          lastnode = thechild;
        }
      }
      /* Are there more possible children at that depth? */
      for (; thechild != NULL &&
             (thechild->parent != NULL || thechild->depth != thedepth + 1);
           thechild = thechild->nextnode)
        ;
      if (thechild == NULL) morenodes = false;
    }
  }
} /* end createADT */

/** Get the root node of the tree */
adtnodeptr getRoot(){
  adtnodeptr thenode;
  for (thenode = theADT; thenode != NULL; thenode = thenode->nextnode) {
    if (thenode->parent == NULL && thenode->depth == 0) {
      return thenode;
    }
  }
  return NULL;
}

/** Return the string representation of the node type */
char* type2string(nodetype type) {
  switch (type) {
    case leaf:
      return "leaf";
      break;
    case OR:
      return "or";
      break;
    case AND:
      return "and";
      break;
  }
}

/** BFS */
void BFS(adtnodeptr root, int max_depth, FILE *thefile, bool print_child) {
  int rear = 0;
  int front = 0;
  adtnodeptr queue[1000];
  adtnodeptr v, thechild;

  int width = 0;
  int nb_leaves = 0;
  int nb_nodes = 0;

  queue[rear] = root;
  rear++;

  while (front < rear) {
    v = queue[front];
    front++;
    nb_nodes++;

    // print nodes
    if (!print_child) {
      fprintf(thefile, "%s %s attack 0 %d\n", v->name, type2string(v->type),
              v->time);
    }

    thechild = v->children;
    if (thechild != NULL) {
      if (print_child) fprintf(thefile, "\n%s", v->name);
    } else {
      nb_leaves++;
      if (v->depth == max_depth) width++;
    }

    for (; thechild != NULL; thechild = thechild->nextchild) {
      if (print_child) fprintf(thefile, " %s", thechild->name);

      queue[rear] = thechild;
      rear++;
    }
  }

  if (!print_child) {
    printf("width: %d\n", width);
    printf("# leaves: %d\n", nb_leaves);
    printf("# nodes: %d\n", nb_nodes);
  }
}

/** Write the adt2amas adt file */
void generateFile() {
  FILE *thefile;
  char filename[50];

  adtnodeptr thenode, thechild;

  int max_depth = theADT->depth;
  printf("depth: %d\n", max_depth);

  strcpy(filename, thefilename);
  thefile = fopen(filename, "w");

  // print root node
  adtnodeptr root = getRoot();
  fprintf(thefile, "%s\n\n", root->name);

  // print nodes
  BFS(root, max_depth, thefile, false);

  // print arcs
  BFS(root, max_depth, thefile, true);

  // endline
  fprintf(thefile, "\n");

  fclose(thefile);
}
