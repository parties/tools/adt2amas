# Authors

- Giann Karlo AGUIRRE SAMBONÍ (giannkas1@gmail.com)
- Jaime ARIAS (arias@lipn.univ-paris13.fr)
- Laure PETRUCCI (laure.petrucci@lipn.univ-paris13.fr)

The development of this tool has been support by:

- PICS CNRS Project PARTIES (2019)
- PHC Van Gogh Project PAMPAS (2019)
- BQR Project AMOJAS (2018)
