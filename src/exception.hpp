#ifndef SRC_EXCEPTION_HPP_
#define SRC_EXCEPTION_HPP_

#include <exception>
#include <string>

/**
 * Generic error class
 */
class Exception : public std::exception {
 public:
  explicit Exception(const std::string &message) : _message(message) {}
  const char *what() const noexcept override { return _message.c_str(); }

 private:
  std::string _message;
};

#endif  // SRC_EXCEPTION_HPP_
