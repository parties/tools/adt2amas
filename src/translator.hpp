#ifndef TRANSLATOR_HPP
#define TRANSLATOR_HPP

#include "ad_tree.hpp"
#include "automata.hpp"
#include "exception.hpp"
#include "tree_node.hpp"

/**
 * Error class to handle translation errors
 */
class TranslatorException : public Exception {
 public:
  explicit TranslatorException(const std::string &message)
      : Exception(message) {}
};

/**
 * @ingroup adt2amas
 *
 * Represents the Attack-Defence Tree (ADTree) to Extended Asynchronous
 * Multi-Agent System (EAMAS) translation introduced in:
 *
 *   Hackers vs. Security: Attack-Defence Trees as Asynchronous Multi-Agent
 * Systems
 */
class Translator {
 protected:
  static Automata *translation(TreeNode *node, Automata *amata);
  static Automata *translation_full(TreeNode *node, Automata *amata);

 public:
  Translator();
  ~Translator();

  /**
   * Translates a leaf ADTree node into its EAMAS model
   * @param[in] leaf leaf node
   * @param[in,out] amata resulting EAMAS model
   */
  static void leaf_to_automaton(TreeNode *leaf, Automata *amata);

  /**
   * Translates a leaf ADTree node into its full EAMAS model
   * @param[in] leaf leaf node
   * @param[in,out] amata resulting EAMAS model
   */
  static void leaf_to_automaton_full(TreeNode *leaf, Automata *amata);

  /**
   * Translates an AND ADTree gate into its EAMAS model
   * @param[in] node AND gate
   * @param[in,out] amata resulting EAMAS model
   */
  static void and_to_automaton(TreeNode *node, Automata *amata);

  /**
   * Translates an AND ADTree gate into its full EAMAS model
   * @param[in] node AND gate
   * @param[in,out] amata resulting EAMAS model
   */
  static void and_to_automaton_full(TreeNode *node, Automata *amata);

  /**
   * Translates a NAND ADTree gate into its EAMAS model
   * @param[in] node NAND gate
   * @param[in,out] amata resulting EAMAS model
   */
  static void nand_to_automaton(TreeNode *node, Automata *amata);

  /**
   * Translates a NAND ADTree gate into its full EAMAS model
   * @param[in] node NAND gate
   * @param[in,out] amata resulting EAMAS model
   */
  static void nand_to_automaton_full(TreeNode *node, Automata *amata);

  /**
   * Translates a SAND ADTree gate into its EAMAS model
   * @param[in] node SAND gate
   * @param[in,out] amata resulting EAMAS model
   */
  static void sand_to_automaton(TreeNode *node, Automata *amata);

  /**
   * Translates a SAND ADTree gate into its full EAMAS model
   * @param[in] node SAND gate
   * @param[in,out] amata resulting EAMAS model
   */
  static void sand_to_automaton_full(TreeNode *node, Automata *amata);

  /**
   * Translates a SNAND ADTree gate into its EAMAS model
   * @param[in] node SNAND gate
   * @param[in,out] amata resulting EAMAS model
   */
  static void snand_to_automaton(TreeNode *node, Automata *amata);

  /**
   * Translates a SNAND ADTree gate into its full EAMAS model
   * @param[in] node SNAND gate
   * @param[in,out] amata resulting EAMAS model
   */
  static void snand_to_automaton_full(TreeNode *node, Automata *amata);

  /**
   * Translates a OR ADTree gate into its EAMAS model
   * @param[in] node OR gate
   * @param[in,out] amata resulting EAMAS model
   */
  static void or_to_automaton(TreeNode *node, Automata *amata);

  /**
   * Translates a OR ADTree gate into its full EAMAS model
   * @param[in] node OR gate
   * @param[in,out] amata resulting EAMAS model
   */
  static void or_to_automaton_full(TreeNode *node, Automata *amata);

  /**
   * Translates a NOR ADTree gate into its EAMAS model
   * @param[in] node NOR gate
   * @param[in,out] amata resulting EAMAS model
   */
  static void nor_to_automaton(TreeNode *node, Automata *amata);

  /**
   * Translate a ADtree model into its EAMAS representation
   *
   * @param[in] adtree ADtree model
   * @param[in] full_model generated full model without reductions
   *
   * @return an EAMAS model
   */
  static Automata *adt2amas_translate(ADtree *adtree, bool full_model = false);
};

#endif
