#include "translator.hpp"

#include <typeinfo>

#include "gates/and.hpp"
#include "gates/nand.hpp"
#include "gates/nor.hpp"
#include "gates/or.hpp"
#include "gates/sand.hpp"
#include "gates/snand.hpp"
#include "leaf.hpp"
#include "variable.hpp"

using namespace std;

Translator::Translator() {}
Translator::~Translator() {}

Automata *Translator::adt2amas_translate(ADtree *adtree, bool full_model) {
  Automata *new_amata = new Automata();
  if (full_model) {
    return translation_full(adtree->get_root(), new_amata);
  } else {
    return translation(adtree->get_root(), new_amata);
  }
}

Automata *Translator::translation_full(TreeNode *node, Automata *amata) {
  if (node->get_node_type() == "Leaf")
    leaf_to_automaton_full(node, amata);
  else if (node->get_node_type() == "And")
    and_to_automaton_full(node, amata);
  else if (node->get_node_type() == "Or")
    or_to_automaton_full(node, amata);
  else if (node->get_node_type() == "Nand")
    nand_to_automaton_full(node, amata);
  else if (node->get_node_type() == "Sand")
    sand_to_automaton_full(node, amata);
  else if (node->get_node_type() == "Snand")
    snand_to_automaton_full(node, amata);
  else
    throw TranslatorException("node " + node->get_goal() + "(" +
                              node->get_node_type() +
                              ") is not supported in the full translation");

  for (auto &i : node->get_children()) amata = translation_full(i, amata);

  return amata;
}

Automata *Translator::translation(TreeNode *node, Automata *amata) {
  if (node->get_node_type() == "Leaf")
    leaf_to_automaton(node, amata);
  else if (node->get_node_type() == "And")
    and_to_automaton(node, amata);
  else if (node->get_node_type() == "Or")
    or_to_automaton(node, amata);
  else if (node->get_node_type() == "Nand")
    nand_to_automaton(node, amata);
  else if (node->get_node_type() == "Nor")
    nor_to_automaton(node, amata);
  else if (node->get_node_type() == "Sand")
    sand_to_automaton(node, amata);
  else if (node->get_node_type() == "Snand")
    snand_to_automaton(node, amata);

  for (auto &i : node->get_children()) amata = translation(i, amata);

  return amata;
}

void Translator::leaf_to_automaton(TreeNode *node, Automata *amata) {
  State *initial_state = new State(StateType::Initial);
  Automaton *new_amaton =
      new Automaton(initial_state, node->get_goal(), node->get_node_type());

  State *state_one = new State(StateType::Normal);
  State *state_two = new State(StateType::Normal);

  Channel *channel_one = new Channel(node->get_goal() + "_ok");
  Channel *channel_two = new Channel(node->get_goal() + "_nok");

  Synchronization *synchronization_one =
      new Synchronization(SynchronizationType::Send, channel_one);
  Synchronization *synchronization_two =
      new Synchronization(SynchronizationType::Send, channel_two);

  Label *label_one = new Label(node->get_goal());

  vector<Assignment *> updates;
  for (auto &a : node->get_attributes()) {
    auto linear_expr = a->get_computation_function();
    auto variable = a->get_type() + '_' + node->get_goal();

    updates.push_back(new Assignment(variable, linear_expr));

    // Add initial value as an automaton's variable
    new_amaton->add_variable(
        new Assignment("init_" + variable, new Constant(a->get_value())));
  }

  new_amaton->add_state(state_one);
  new_amaton->add_state(state_two);

  new_amaton->add_transition(initial_state, state_one, label_one, updates);
  new_amaton->add_transition(state_one, state_one, synchronization_one);
  new_amaton->add_transition(initial_state, state_two, synchronization_two);
  new_amaton->add_transition(state_two, state_two, synchronization_two);

  amata->add_channel(channel_one);
  amata->add_channel(channel_two);
  amata->add_automaton(new_amaton);
}

void Translator::leaf_to_automaton_full(TreeNode *node, Automata *amata) {
  State *initial_state = new State(StateType::Initial);
  Automaton *new_amaton =
      new Automaton(initial_state, node->get_goal(), node->get_node_type());

  State *state_one = new State(StateType::Normal);
  State *state_two = new State(StateType::Normal);

  Channel *channel_one = new Channel(node->get_goal() + "_ok");
  Channel *channel_two = new Channel(node->get_goal() + "_nok");

  Synchronization *synchronization_one =
      new Synchronization(SynchronizationType::Send, channel_one);
  Synchronization *synchronization_two =
      new Synchronization(SynchronizationType::Send, channel_two);

  Label *label_one = new Label(node->get_goal());

  new_amaton->add_state(state_one);
  new_amaton->add_state(state_two);

  new_amaton->add_transition(initial_state, state_one, label_one);
  new_amaton->add_transition(state_one, state_one, synchronization_one);
  new_amaton->add_transition(initial_state, state_two, synchronization_two);
  new_amaton->add_transition(state_two, state_two, synchronization_two);

  amata->add_channel(channel_one);
  amata->add_channel(channel_two);
  amata->add_automaton(new_amaton);
}

void Translator::and_to_automaton(TreeNode *node, Automata *amata) {
  // check that the gate has two or more children
  if (node->get_children().size() < 2)
    throw TranslatorException("Gate must have two or more children");

  State *initial_state = new State(StateType::Initial);
  Automaton *new_amaton =
      new Automaton(initial_state, node->get_goal(), node->get_node_type());

  /* ok automaton path creation */
  for (auto &i : node->get_children()) {
    State *state_nth = new State(StateType::Normal);
    Channel *channel_ok_nth = new Channel(i->get_goal() + "_ok");
    Synchronization *synchronization_ok_nth =
        new Synchronization(SynchronizationType::Receive, channel_ok_nth);

    State *latest_s = new_amaton->get_states().back();
    new_amaton->add_state(state_nth);

    new_amaton->add_transition(latest_s, state_nth, synchronization_ok_nth);
    amata->add_channel(channel_ok_nth);
  }

  /* Last nodes creation */
  State *state_nok_node = new State(StateType::Normal);
  State *state_ok_node = new State(StateType::Normal);

  /* Respective channels with synchronizations */
  Channel *channel_ok_node = new Channel(node->get_goal() + "_ok");
  Channel *channel_nok_node = new Channel(node->get_goal() + "_nok");

  Synchronization *synchronization_ok_node =
      new Synchronization(SynchronizationType::Send, channel_ok_node);
  Synchronization *synchronization_nok_node =
      new Synchronization(SynchronizationType::Send, channel_nok_node);

  Label *unique_label = new Label(node->get_goal());

  vector<Assignment *> updates;
  for (auto &a : node->get_attributes()) {
    auto linear_expr = a->get_computation_function();
    auto variable = a->get_type() + '_' + node->get_goal();

    updates.push_back(new Assignment(variable, linear_expr));

    new_amaton->add_variable(
        new Assignment("init_" + variable, new Constant(a->get_value())));
  }

  /* Creation of label transition */
  new_amaton->add_state(state_ok_node);
  new_amaton->add_transition(*(++(new_amaton->get_states().rbegin())),
                             state_ok_node, unique_label, updates);

  /* Self-loops */
  new_amaton->add_state(state_nok_node);
  new_amaton->add_transition(state_ok_node, state_ok_node,
                             synchronization_ok_node);
  new_amaton->add_transition(state_nok_node, state_nok_node,
                             synchronization_nok_node);

  /* Creation of transitions */
  for (auto &i : node->get_children()) {
    Channel *channel_nok_nth = new Channel(i->get_goal() + "_nok");
    Synchronization *synchronization_nok_nth =
        new Synchronization(SynchronizationType::Receive, channel_nok_nth);

    new_amaton->add_transition(initial_state, state_nok_node,
                               synchronization_nok_nth);
    amata->add_channel(channel_nok_nth);
  }

  amata->add_channel(channel_ok_node);
  amata->add_channel(channel_nok_node);
  amata->add_automaton(new_amaton);
}

void Translator::and_to_automaton_full(TreeNode *node, Automata *amata) {
  int nb_children = node->get_children().size();

  // check that the gate has two or more children
  if (nb_children < 2)
    throw TranslatorException("Gate must have two or more children");

  State *initial_state = new State(StateType::Initial);
  Automaton *new_amaton =
      new Automaton(initial_state, node->get_goal(), node->get_node_type());

  /** nok state */
  State *state_nok_node = new State(StateType::Normal);

  State *state_one_ = new State(StateType::Normal);
  new_amaton->add_state(state_one_);

  /** add nok transitions */
  State *latest_s = state_one_;
  std::vector<State *> jump_states{latest_s};
  for (int i = 1; i < nb_children; i++) {
    State *state_nth = nullptr;

    if (i == nb_children - 1) {
      state_nth = state_nok_node;
    } else {
      state_nth = new State(StateType::Normal);
    }

    new_amaton->add_state(state_nth);
    jump_states.push_back(state_nth);

    for (auto &i : node->get_children()) {
      Channel *channel_ok_nth = new Channel(i->get_goal() + "_ok");
      Synchronization *synchronization_ok_nth =
          new Synchronization(SynchronizationType::Receive, channel_ok_nth);
      Channel *channel_nok_nth = new Channel(i->get_goal() + "_nok");
      Synchronization *synchronization_nok_nth =
          new Synchronization(SynchronizationType::Receive, channel_nok_nth);

      new_amaton->add_transition(latest_s, state_nth, synchronization_ok_nth);
      new_amaton->add_transition(latest_s, state_nth, synchronization_nok_nth);

      amata->add_channel(channel_ok_nth);
      amata->add_channel(channel_nok_nth);
    }

    latest_s = state_nth;
  }

  /* ok automaton path creation */
  latest_s = initial_state;
  for (int i = 0; i < nb_children; i++) {
    State *state_nth = new State(StateType::Normal);
    new_amaton->add_state(state_nth);

    State *state_nok_nth = jump_states[i];

    for (auto &n : node->get_children()) {
      Channel *channel_ok_nth = new Channel(n->get_goal() + "_ok");
      Synchronization *synchronization_ok_nth =
          new Synchronization(SynchronizationType::Receive, channel_ok_nth);
      new_amaton->add_transition(latest_s, state_nth, synchronization_ok_nth);
      amata->add_channel(channel_ok_nth);

      Channel *channel_nok_nth = new Channel(n->get_goal() + "_nok");
      Synchronization *synchronization_nok_nth =
          new Synchronization(SynchronizationType::Receive, channel_nok_nth);
      new_amaton->add_transition(latest_s, state_nok_nth,
                                 synchronization_nok_nth);
      amata->add_channel(channel_nok_nth);
    }
    latest_s = state_nth;
  }

  /* OK state */
  State *state_ok_node = new State(StateType::Normal);
  Channel *channel_ok_node = new Channel(node->get_goal() + "_ok");
  Synchronization *synchronization_ok_node =
      new Synchronization(SynchronizationType::Send, channel_ok_node);

  Label *unique_label = new Label(node->get_goal());

  /* Creation of label transition */
  new_amaton->add_state(state_ok_node);
  new_amaton->add_transition(*(++(new_amaton->get_states().rbegin())),
                             state_ok_node, unique_label);
  amata->add_channel(channel_ok_node);

  /* Self-loops */
  new_amaton->add_transition(state_ok_node, state_ok_node,
                             synchronization_ok_node);

  /* nok self-loop */
  Channel *channel_nok_node = new Channel(node->get_goal() + "_nok");
  Synchronization *synchronization_nok_node =
      new Synchronization(SynchronizationType::Send, channel_nok_node);

  new_amaton->add_transition(state_nok_node, state_nok_node,
                             synchronization_nok_node);
  amata->add_channel(channel_nok_node);

  amata->add_automaton(new_amaton);
}

void Translator::or_to_automaton(TreeNode *node, Automata *amata) {
  // check that the gate has two or more children
  if (node->get_children().size() < 2)
    throw TranslatorException("Gate must have two or more children");

  State *initial_state = new State(StateType::Initial);
  Automaton *new_amaton =
      new Automaton(initial_state, node->get_goal(), node->get_node_type());

  /* nok automaton path creation */
  for (auto &i : node->get_children()) {
    State *state_nth = new State(StateType::Normal);
    Channel *channel_nok_nth = new Channel(i->get_goal() + "_nok");
    Synchronization *synchronization_nok_nth =
        new Synchronization(SynchronizationType::Receive, channel_nok_nth);

    State *latest_s = new_amaton->get_states().back();
    new_amaton->add_state(state_nth);

    new_amaton->add_transition(latest_s, state_nth, synchronization_nok_nth);
    amata->add_channel(channel_nok_nth);
  }

  /* nok self-loop */
  Channel *channel_nok_node = new Channel(node->get_goal() + "_nok");
  Synchronization *synchronization_nok_node =
      new Synchronization(SynchronizationType::Send, channel_nok_node);
  State *last_state = new_amaton->get_states().back();
  new_amaton->add_transition(last_state, last_state, synchronization_nok_node);
  amata->add_channel(channel_nok_node);

  /* ok transitions */
  State *state_one = new State(StateType::Normal);
  new_amaton->add_state(state_one);
  for (auto &i : node->get_children()) {
    Channel *channel_ok_nth = new Channel(i->get_goal() + "_ok");
    Synchronization *synchronization_ok_nth =
        new Synchronization(SynchronizationType::Receive, channel_ok_nth);

    vector<Assignment *> updates;
    for (auto &a : i->get_attributes()) {
      auto variable = a->get_type() + '_' + node->get_goal();
      auto linear_expr = new Variable(a->get_type() + '_' + i->get_goal());

      updates.push_back(new Assignment(variable, linear_expr));

      new_amaton->add_variable(
          new Assignment("init_" + variable, new Constant(a->get_value())));
    }

    new_amaton->add_transition(initial_state, state_one, synchronization_ok_nth,
                               updates);
    amata->add_channel(channel_ok_nth);
  }

  State *state_ok_node = new State(StateType::Normal);
  Label *unique_label = new Label(node->get_goal());

  vector<Assignment *> updates;
  for (auto &a : node->get_attributes()) {
    auto linear_expr = a->get_computation_function();
    auto variable = a->get_type() + '_' + node->get_goal();

    updates.push_back(new Assignment(variable, linear_expr));

    new_amaton->add_variable(
        new Assignment("init_" + variable, new Constant(a->get_value())));
  }

  new_amaton->add_state(state_ok_node);
  new_amaton->add_transition(state_one, state_ok_node, unique_label, updates);

  Channel *channel_ok_node = new Channel(node->get_goal() + "_ok");
  Synchronization *synchronization_ok_node =
      new Synchronization(SynchronizationType::Send, channel_ok_node);
  new_amaton->add_transition(state_ok_node, state_ok_node,
                             synchronization_ok_node);

  amata->add_channel(channel_ok_node);
  amata->add_automaton(new_amaton);
}

void Translator::nand_to_automaton(TreeNode *node, Automata *amata) {
  // check that the gate has two or more children
  if (node->get_children().size() != 2)
    throw TranslatorException("Gate must have two children");

  State *initial_state = new State(StateType::Initial);
  Automaton *new_amaton =
      new Automaton(initial_state, node->get_goal(), node->get_node_type());

  State *state_one = new State(StateType::Normal);
  State *state_two = new State(StateType::Normal);
  State *state_ok_node = new State(StateType::Normal);
  State *state_nok_node = new State(StateType::Normal);

  vector<TreeNode *> node_children = node->get_children();
  auto it = node_children.begin();
  Channel *channel_ok_one = new Channel(((*it)->get_goal()) + "_ok");
  Channel *channel_ok_two = new Channel((*(++it))->get_goal() + "_ok");
  Channel *channel_nok_one = new Channel((*(--it))->get_goal() + "_nok");
  Channel *channel_nok_two = new Channel((*(++it))->get_goal() + "_nok");
  Channel *channel_ok_node = new Channel(node->get_goal() + "_ok");
  Channel *channel_nok_node = new Channel(node->get_goal() + "_nok");

  Synchronization *synchronization_ok_one =
      new Synchronization(SynchronizationType::Receive, channel_ok_one);
  Synchronization *synchronization_ok_two =
      new Synchronization(SynchronizationType::Receive, channel_ok_two);
  Synchronization *synchronization_nok_one =
      new Synchronization(SynchronizationType::Receive, channel_nok_one);
  Synchronization *synchronization_nok_two =
      new Synchronization(SynchronizationType::Receive, channel_nok_two);
  Synchronization *synchronization_ok_node =
      new Synchronization(SynchronizationType::Send, channel_ok_node);
  Synchronization *synchronization_nok_node =
      new Synchronization(SynchronizationType::Send, channel_nok_node);

  Label *label_node = new Label(node->get_goal());

  vector<Assignment *> updates;
  for (auto &a : node->get_attributes()) {
    auto linear_expr = a->get_computation_function();
    auto variable = a->get_type() + '_' + node->get_goal();

    updates.push_back(new Assignment(variable, linear_expr));

    new_amaton->add_variable(
        new Assignment("init_" + variable, new Constant(a->get_value())));
  }

  Inequality *adtree_guard = static_cast<Nand *>(node)->get_condition();
  vector<Inequality *> guard = adtree_guard == nullptr
                                   ? vector<Inequality *>{}
                                   : vector<Inequality *>{adtree_guard};

  new_amaton->add_state(state_one);
  new_amaton->add_state(state_two);
  new_amaton->add_state(state_ok_node);
  new_amaton->add_state(state_nok_node);

  new_amaton->add_transition(initial_state, state_one, synchronization_ok_one);
  new_amaton->add_transition(state_one, state_two, synchronization_nok_two);
  new_amaton->add_transition(state_two, state_ok_node, label_node, updates,
                             guard);
  new_amaton->add_transition(state_ok_node, state_ok_node,
                             synchronization_ok_node);
  new_amaton->add_transition(initial_state, state_nok_node,
                             synchronization_nok_one);
  new_amaton->add_transition(initial_state, state_nok_node,
                             synchronization_ok_two);
  new_amaton->add_transition(state_nok_node, state_nok_node,
                             synchronization_nok_node);

  amata->add_channel(channel_ok_one);
  amata->add_channel(channel_ok_two);
  amata->add_channel(channel_nok_one);
  amata->add_channel(channel_nok_two);
  amata->add_channel(channel_ok_node);
  amata->add_channel(channel_nok_node);
  amata->add_automaton(new_amaton);
}

void Translator::nand_to_automaton_full(TreeNode *node, Automata *amata) {
  std::vector<TreeNode *> children = node->get_children();

  // check that the gate has less or more children
  if (children.size() != 2)
    throw TranslatorException("Gate must have two children");

  State *initial_state = new State(StateType::Initial);
  Automaton *new_amaton =
      new Automaton(initial_state, node->get_goal(), node->get_node_type());

  State *state_one_ = new State(StateType::Normal);
  new_amaton->add_state(state_one_);

  /** nok state */
  State *state_nok_node = new State(StateType::Normal);
  new_amaton->add_state(state_nok_node);

  /** add nok transitions */
  for (auto &i : children) {
    Channel *channel_ok_nth = new Channel(i->get_goal() + "_ok");
    Synchronization *synchronization_ok_nth =
        new Synchronization(SynchronizationType::Receive, channel_ok_nth);
    Channel *channel_nok_nth = new Channel(i->get_goal() + "_nok");
    Synchronization *synchronization_nok_nth =
        new Synchronization(SynchronizationType::Receive, channel_nok_nth);

    new_amaton->add_transition(state_one_, state_nok_node,
                               synchronization_ok_nth);
    new_amaton->add_transition(state_one_, state_nok_node,
                               synchronization_nok_nth);

    amata->add_channel(channel_ok_nth);
    amata->add_channel(channel_nok_nth);
  }

  State *latest_s = initial_state;
  std::vector jump_states{state_one_, state_nok_node};

  /* ok automaton path creation */
  for (int i = 0; i < children.size(); i++) {
    State *state_nth = new State(StateType::Normal);
    new_amaton->add_state(state_nth);

    State *state_nok_nth = jump_states[i];

    for (int j = 0; j < children.size(); j++) {
      std::string goal = children[j]->get_goal();
      std::vector<std::string> suffix{"_ok", "_nok"};

      Channel *channel_ok_nth = new Channel(goal + suffix[j]);
      Synchronization *synchronization_ok_nth =
          new Synchronization(SynchronizationType::Receive, channel_ok_nth);
      new_amaton->add_transition(latest_s, state_nth, synchronization_ok_nth);
      amata->add_channel(channel_ok_nth);

      Channel *channel_nok_nth = new Channel(goal + suffix[1 - j]);
      Synchronization *synchronization_nok_nth =
          new Synchronization(SynchronizationType::Receive, channel_nok_nth);
      new_amaton->add_transition(latest_s, state_nok_nth,
                                 synchronization_nok_nth);
      amata->add_channel(channel_nok_nth);
    }

    latest_s = state_nth;
  }

  /* OK state */
  State *state_ok_node = new State(StateType::Normal);
  Channel *channel_ok_node = new Channel(node->get_goal() + "_ok");
  Synchronization *synchronization_ok_node =
      new Synchronization(SynchronizationType::Send, channel_ok_node);

  Label *unique_label = new Label(node->get_goal());

  /* Creation of label transition */
  new_amaton->add_state(state_ok_node);
  new_amaton->add_transition(*(++(new_amaton->get_states().rbegin())),
                             state_ok_node, unique_label);
  amata->add_channel(channel_ok_node);

  /* Self-loops */
  new_amaton->add_transition(state_ok_node, state_ok_node,
                             synchronization_ok_node);

  /* nok self-loop */
  Channel *channel_nok_node = new Channel(node->get_goal() + "_nok");
  Synchronization *synchronization_nok_node =
      new Synchronization(SynchronizationType::Send, channel_nok_node);

  new_amaton->add_transition(state_nok_node, state_nok_node,
                             synchronization_nok_node);
  amata->add_channel(channel_nok_node);

  amata->add_automaton(new_amaton);
}

void Translator::nor_to_automaton(TreeNode *node, Automata *amata) {
  // check that the gate has two or more children
  if (node->get_children().size() != 2)
    throw TranslatorException("Gate must have two children");

  State *initial_state = new State(StateType::Initial);
  Automaton *new_amaton =
      new Automaton(initial_state, node->get_goal(), node->get_node_type());

  State *state_one = new State(StateType::Normal);
  State *state_two = new State(StateType::Normal);
  State *state_ok_node = new State(StateType::Normal);
  State *state_nok_node = new State(StateType::Normal);

  vector<TreeNode *> node_children = node->get_children();
  auto it = node_children.begin();
  Channel *channel_ok_one = new Channel(((*it)->get_goal()) + "_ok");
  Channel *channel_ok_two = new Channel((*(++it))->get_goal() + "_ok");
  Channel *channel_nok_one = new Channel((*(--it))->get_goal() + "_nok");
  Channel *channel_nok_two = new Channel((*(++it))->get_goal() + "_nok");
  Channel *channel_ok_node = new Channel(node->get_goal() + "_ok");
  Channel *channel_nok_node = new Channel(node->get_goal() + "_nok");

  Synchronization *synchronization_ok_one =
      new Synchronization(SynchronizationType::Receive, channel_ok_one);
  Synchronization *synchronization_ok_two =
      new Synchronization(SynchronizationType::Receive, channel_ok_two);
  Synchronization *synchronization_nok_one =
      new Synchronization(SynchronizationType::Receive, channel_nok_one);
  Synchronization *synchronization_nok_two =
      new Synchronization(SynchronizationType::Receive, channel_nok_two);
  Synchronization *synchronization_ok_node =
      new Synchronization(SynchronizationType::Send, channel_ok_node);
  Synchronization *synchronization_nok_node =
      new Synchronization(SynchronizationType::Send, channel_nok_node);

  Inequality *adtree_guard = static_cast<Nor *>(node)->get_condition();
  vector<Inequality *> guard = adtree_guard == nullptr
                                   ? vector<Inequality *>{}
                                   : vector<Inequality *>{adtree_guard};

  vector<Assignment *> updates;
  for (auto &a : node->get_attributes()) {
    auto linear_expr = a->get_computation_function();
    auto variable = a->get_type() + '_' + node->get_goal();

    updates.push_back(new Assignment(variable, linear_expr));

    new_amaton->add_variable(
        new Assignment("init_" + variable, new Constant(a->get_value())));
  }

  Label *label_node = new Label(node->get_goal());

  new_amaton->add_state(state_one);
  new_amaton->add_state(state_two);
  new_amaton->add_state(state_ok_node);
  new_amaton->add_state(state_nok_node);

  new_amaton->add_transition(initial_state, state_two, synchronization_nok_one);
  new_amaton->add_transition(state_two, state_nok_node, synchronization_ok_two);
  new_amaton->add_transition(state_nok_node, state_nok_node,
                             synchronization_nok_node);
  new_amaton->add_transition(initial_state, state_one, synchronization_ok_one);
  new_amaton->add_transition(initial_state, state_one, synchronization_nok_two);
  new_amaton->add_transition(state_one, state_ok_node, label_node, updates,
                             guard);
  new_amaton->add_transition(state_ok_node, state_ok_node,
                             synchronization_ok_node);

  amata->add_channel(channel_ok_one);
  amata->add_channel(channel_ok_two);
  amata->add_channel(channel_nok_one);
  amata->add_channel(channel_nok_two);
  amata->add_channel(channel_ok_node);
  amata->add_channel(channel_nok_node);
  amata->add_automaton(new_amaton);
}

void Translator::sand_to_automaton(TreeNode *node, Automata *amata) {
  // check that the gate has two or more children
  if (node->get_children().size() < 2)
    throw TranslatorException("Gate must have two or more children");

  State *initial_state = new State(StateType::Initial);
  Automaton *new_amaton =
      new Automaton(initial_state, node->get_goal(), node->get_node_type());

  State *state_nok_node = new State(StateType::Normal);
  new_amaton->add_state(state_nok_node);

  /* The last state doesn't have a transition to state_nok_node */
  /* ok automaton path creation */
  vector<TreeNode *> node_children = node->get_children();
  int i = 0;
  for (; i < node_children.size(); i++) {
    State *state_nth = new State(StateType::Normal);
    Channel *channel_ok_nth = new Channel(node_children[i]->get_goal() + "_ok");
    Synchronization *synchronization_ok_nth =
        new Synchronization(SynchronizationType::Receive, channel_ok_nth);

    Channel *channel_nok_nth =
        new Channel(node_children[i]->get_goal() + "_nok");
    Synchronization *synchronization_nok_nth =
        new Synchronization(SynchronizationType::Receive, channel_nok_nth);

    new_amaton->add_state(state_nth);
    auto states = new_amaton->get_states();
    auto rit = states[states.size() - 2];

    if (rit == state_nok_node) {
      new_amaton->add_transition(new_amaton->get_states().front(),
                                 new_amaton->get_states().back(),
                                 synchronization_ok_nth);
      new_amaton->add_transition(new_amaton->get_states().front(),
                                 state_nok_node, synchronization_nok_nth);
    } else {
      new_amaton->add_transition(rit, new_amaton->get_states().back(),
                                 synchronization_ok_nth);
      new_amaton->add_transition(rit, state_nok_node, synchronization_nok_nth);
    }
    amata->add_channel(channel_ok_nth);
    amata->add_channel(channel_nok_nth);
  }

  /* Last node creation */
  State *state_ok_node = new State(StateType::Normal);

  /* Respective channels with synchronizations */
  Channel *channel_ok_node = new Channel(node->get_goal() + "_ok");
  Channel *channel_nok_node = new Channel(node->get_goal() + "_nok");

  Synchronization *synchronization_ok_node =
      new Synchronization(SynchronizationType::Send, channel_ok_node);
  Synchronization *synchronization_nok_node =
      new Synchronization(SynchronizationType::Send, channel_nok_node);

  Label *unique_label = new Label(node->get_goal());

  vector<Assignment *> updates;
  for (auto &a : node->get_attributes()) {
    auto linear_expr = a->get_computation_function();
    auto variable = a->get_type() + '_' + node->get_goal();

    updates.push_back(new Assignment(variable, linear_expr));

    new_amaton->add_variable(
        new Assignment("init_" + variable, new Constant(a->get_value())));
  }

  /* Creation of label transition */
  new_amaton->add_state(state_ok_node);
  new_amaton->add_transition(*(++(new_amaton->get_states().rbegin())),
                             state_ok_node, unique_label, updates);

  /* Self-loops */
  new_amaton->add_transition(state_ok_node, state_ok_node,
                             synchronization_ok_node);
  new_amaton->add_transition(state_nok_node, state_nok_node,
                             synchronization_nok_node);

  amata->add_channel(channel_ok_node);
  amata->add_channel(channel_nok_node);
  amata->add_automaton(new_amaton);
}

void Translator::snand_to_automaton(TreeNode *node, Automata *amata) {
  // check that the gate has only two children
  if (node->get_children().size() != 2)
    throw TranslatorException("Gate must have two children");

  State *initial_state = new State(StateType::Initial);
  Automaton *new_amaton =
      new Automaton(initial_state, node->get_goal(), node->get_node_type());

  State *state_one = new State(StateType::Normal);
  State *state_two = new State(StateType::Normal);
  State *state_ok_node = new State(StateType::Normal);
  State *state_nok_node = new State(StateType::Normal);

  vector<TreeNode *> node_children = node->get_children();
  auto it = node_children.begin();
  Channel *channel_ok_one = new Channel(((*it)->get_goal()) + "_ok");
  Channel *channel_ok_two = new Channel((*(++it))->get_goal() + "_ok");
  Channel *channel_nok_one = new Channel((*(--it))->get_goal() + "_nok");
  Channel *channel_nok_two = new Channel((*(++it))->get_goal() + "_nok");
  Channel *channel_ok_node = new Channel(node->get_goal() + "_ok");
  Channel *channel_nok_node = new Channel(node->get_goal() + "_nok");

  Synchronization *synchronization_ok_one =
      new Synchronization(SynchronizationType::Receive, channel_ok_one);
  Synchronization *synchronization_ok_two =
      new Synchronization(SynchronizationType::Receive, channel_ok_two);
  Synchronization *synchronization_nok_one =
      new Synchronization(SynchronizationType::Receive, channel_nok_one);
  Synchronization *synchronization_nok_two =
      new Synchronization(SynchronizationType::Receive, channel_nok_two);
  Synchronization *synchronization_ok_node =
      new Synchronization(SynchronizationType::Send, channel_ok_node);
  Synchronization *synchronization_nok_node =
      new Synchronization(SynchronizationType::Send, channel_nok_node);

  Label *label_node = new Label(node->get_goal());

  vector<Assignment *> updates;
  for (auto &a : node->get_attributes()) {
    auto linear_expr = a->get_computation_function();
    auto variable = a->get_type() + '_' + node->get_goal();

    updates.push_back(new Assignment(variable, linear_expr));

    new_amaton->add_variable(
        new Assignment("init_" + variable, new Constant(a->get_value())));
  }

  new_amaton->add_state(state_one);
  new_amaton->add_state(state_two);
  new_amaton->add_state(state_ok_node);
  new_amaton->add_state(state_nok_node);

  new_amaton->add_transition(initial_state, state_one, synchronization_ok_one);
  new_amaton->add_transition(state_one, state_two, synchronization_nok_two);
  new_amaton->add_transition(state_two, state_ok_node, label_node, updates);
  new_amaton->add_transition(state_ok_node, state_ok_node,
                             synchronization_ok_node);
  new_amaton->add_transition(initial_state, state_nok_node,
                             synchronization_nok_one);
  new_amaton->add_transition(state_one, state_nok_node, synchronization_ok_two);
  new_amaton->add_transition(state_nok_node, state_nok_node,
                             synchronization_nok_node);

  amata->add_channel(channel_ok_one);
  amata->add_channel(channel_ok_two);
  amata->add_channel(channel_nok_one);
  amata->add_channel(channel_nok_two);
  amata->add_channel(channel_ok_node);
  amata->add_channel(channel_nok_node);
  amata->add_automaton(new_amaton);
}

void Translator::sand_to_automaton_full(TreeNode *node, Automata *amata) {
  std::vector<TreeNode *> children = node->get_children();
  int nb_children = children.size();

  // check that the gate has two or more children
  if (nb_children < 2)
    throw TranslatorException("Gate must have two or more children");

  State *initial_state = new State(StateType::Initial);
  Automaton *new_amaton =
      new Automaton(initial_state, node->get_goal(), node->get_node_type());

  /** nok state */
  State *state_nok_node = new State(StateType::Normal);

  State *state_one_ = new State(StateType::Normal);
  new_amaton->add_state(state_one_);

  /** add nok transitions */
  State *latest_s = state_one_;
  std::vector<State *> jump_states{latest_s};
  for (int i = 1; i < nb_children; i++) {
    State *state_nth = nullptr;

    if (i == nb_children - 1) {
      state_nth = state_nok_node;
    } else {
      state_nth = new State(StateType::Normal);
    }

    new_amaton->add_state(state_nth);
    jump_states.push_back(state_nth);

    std::string child = children[i]->get_goal();
    Channel *channel_ok_nth = new Channel(child + "_ok");
    Synchronization *synchronization_ok_nth =
        new Synchronization(SynchronizationType::Receive, channel_ok_nth);
    Channel *channel_nok_nth = new Channel(child + "_nok");
    Synchronization *synchronization_nok_nth =
        new Synchronization(SynchronizationType::Receive, channel_nok_nth);

    new_amaton->add_transition(latest_s, state_nth, synchronization_ok_nth);
    new_amaton->add_transition(latest_s, state_nth, synchronization_nok_nth);

    amata->add_channel(channel_ok_nth);
    amata->add_channel(channel_nok_nth);

    latest_s = state_nth;
  }

  /* ok automaton path creation */
  latest_s = initial_state;
  for (int i = 0; i < nb_children; i++) {
    State *state_nth = new State(StateType::Normal);
    new_amaton->add_state(state_nth);

    State *state_nok_nth = jump_states[i];
    std::string child = children[i]->get_goal();

    Channel *channel_ok_nth = new Channel(child + "_ok");
    Synchronization *synchronization_ok_nth =
        new Synchronization(SynchronizationType::Receive, channel_ok_nth);
    new_amaton->add_transition(latest_s, state_nth, synchronization_ok_nth);
    amata->add_channel(channel_ok_nth);

    Channel *channel_nok_nth = new Channel(child + "_nok");
    Synchronization *synchronization_nok_nth =
        new Synchronization(SynchronizationType::Receive, channel_nok_nth);
    new_amaton->add_transition(latest_s, state_nok_nth,
                               synchronization_nok_nth);
    amata->add_channel(channel_nok_nth);

    latest_s = state_nth;
  }

  /* OK state */
  State *state_ok_node = new State(StateType::Normal);
  Channel *channel_ok_node = new Channel(node->get_goal() + "_ok");
  Synchronization *synchronization_ok_node =
      new Synchronization(SynchronizationType::Send, channel_ok_node);

  Label *unique_label = new Label(node->get_goal());

  /* Creation of label transition */
  new_amaton->add_state(state_ok_node);
  new_amaton->add_transition(*(++(new_amaton->get_states().rbegin())),
                             state_ok_node, unique_label);
  amata->add_channel(channel_ok_node);

  /* Self-loops */
  new_amaton->add_transition(state_ok_node, state_ok_node,
                             synchronization_ok_node);

  /* nok self-loop */
  Channel *channel_nok_node = new Channel(node->get_goal() + "_nok");
  Synchronization *synchronization_nok_node =
      new Synchronization(SynchronizationType::Send, channel_nok_node);

  new_amaton->add_transition(state_nok_node, state_nok_node,
                             synchronization_nok_node);
  amata->add_channel(channel_nok_node);

  amata->add_automaton(new_amaton);
}

void Translator::snand_to_automaton_full(TreeNode *node, Automata *amata) {
  std::vector<TreeNode *> children = node->get_children();
  int nb_children = children.size();

  // check that the gate has only two children
  if (nb_children != 2)
    throw TranslatorException("Gate must have two children");

  State *initial_state = new State(StateType::Initial);
  Automaton *new_amaton =
      new Automaton(initial_state, node->get_goal(), node->get_node_type());

  State *state_one_ = new State(StateType::Normal);
  new_amaton->add_state(state_one_);

  /** nok state */
  State *state_nok_node = new State(StateType::Normal);
  new_amaton->add_state(state_nok_node);

  std::string defence = children[1]->get_goal();
  Channel *channel_ok_nth = new Channel(defence + "_ok");
  Synchronization *synchronization_ok_nth =
      new Synchronization(SynchronizationType::Receive, channel_ok_nth);
  Channel *channel_nok_nth = new Channel(defence + "_nok");
  Synchronization *synchronization_nok_nth =
      new Synchronization(SynchronizationType::Receive, channel_nok_nth);

  new_amaton->add_transition(state_one_, state_nok_node,
                             synchronization_ok_nth);
  new_amaton->add_transition(state_one_, state_nok_node,
                             synchronization_nok_nth);

  amata->add_channel(channel_ok_nth);
  amata->add_channel(channel_nok_nth);

  /* ok automaton path creation */
  State *latest_s = initial_state;
  std::vector<State *> jump_states{state_one_, state_nok_node};
  std::vector<std::string> suffix{"_ok", "_nok"};
  for (int i = 0; i < nb_children; i++) {
    State *state_nth = new State(StateType::Normal);
    new_amaton->add_state(state_nth);

    State *state_nok_nth = jump_states[i];
    std::string child = children[i]->get_goal();

    Channel *channel_ok_nth = new Channel(child + suffix[i]);
    Synchronization *synchronization_ok_nth =
        new Synchronization(SynchronizationType::Receive, channel_ok_nth);
    new_amaton->add_transition(latest_s, state_nth, synchronization_ok_nth);
    amata->add_channel(channel_ok_nth);

    Channel *channel_nok_nth = new Channel(child + suffix[1 - i]);
    Synchronization *synchronization_nok_nth =
        new Synchronization(SynchronizationType::Receive, channel_nok_nth);
    new_amaton->add_transition(latest_s, state_nok_nth,
                               synchronization_nok_nth);
    amata->add_channel(channel_nok_nth);

    latest_s = state_nth;
  }

  /* OK state */
  State *state_ok_node = new State(StateType::Normal);
  Channel *channel_ok_node = new Channel(node->get_goal() + "_ok");
  Synchronization *synchronization_ok_node =
      new Synchronization(SynchronizationType::Send, channel_ok_node);

  Label *unique_label = new Label(node->get_goal());

  /* Creation of label transition */
  new_amaton->add_state(state_ok_node);
  new_amaton->add_transition(*(++(new_amaton->get_states().rbegin())),
                             state_ok_node, unique_label);
  amata->add_channel(channel_ok_node);

  /* Self-loops */
  new_amaton->add_transition(state_ok_node, state_ok_node,
                             synchronization_ok_node);

  /* nok self-loop */
  Channel *channel_nok_node = new Channel(node->get_goal() + "_nok");
  Synchronization *synchronization_nok_node =
      new Synchronization(SynchronizationType::Send, channel_nok_node);

  new_amaton->add_transition(state_nok_node, state_nok_node,
                             synchronization_nok_node);
  amata->add_channel(channel_nok_node);

  amata->add_automaton(new_amaton);
}

void Translator::or_to_automaton_full(TreeNode *node, Automata *amata) {
  int nb_children = node->get_children().size();

  // check that the gate has two or more children
  if (nb_children < 2)
    throw TranslatorException("Gate must have two or more children");

  State *initial_state = new State(StateType::Initial);
  Automaton *new_amaton =
      new Automaton(initial_state, node->get_goal(), node->get_node_type());

  State *state_one = new State(StateType::Normal);
  new_amaton->add_state(state_one);

  /** add nok transitions */
  State *latest_s = state_one;
  std::vector<State *> jump_states{latest_s};
  for (int i = 1; i < nb_children; i++) {
    State *state_nth = new State(StateType::Normal);

    new_amaton->add_state(state_nth);
    jump_states.push_back(state_nth);

    for (auto &i : node->get_children()) {
      Channel *channel_ok_nth = new Channel(i->get_goal() + "_ok");
      Synchronization *synchronization_ok_nth =
          new Synchronization(SynchronizationType::Receive, channel_ok_nth);
      Channel *channel_nok_nth = new Channel(i->get_goal() + "_nok");
      Synchronization *synchronization_nok_nth =
          new Synchronization(SynchronizationType::Receive, channel_nok_nth);

      new_amaton->add_transition(latest_s, state_nth, synchronization_ok_nth);
      new_amaton->add_transition(latest_s, state_nth, synchronization_nok_nth);

      amata->add_channel(channel_ok_nth);
      amata->add_channel(channel_nok_nth);
    }

    latest_s = state_nth;
  }

  /** OK state */
  State *state_ok_node = new State(StateType::Normal);
  Channel *channel_ok_node = new Channel(node->get_goal() + "_ok");
  Synchronization *synchronization_ok_node =
      new Synchronization(SynchronizationType::Send, channel_ok_node);

  Label *unique_label = new Label(node->get_goal());

  /* Creation of label transition */
  new_amaton->add_state(state_ok_node);
  new_amaton->add_transition(latest_s, state_ok_node, unique_label);
  amata->add_channel(channel_ok_node);

  /* Self-loops */
  new_amaton->add_transition(state_ok_node, state_ok_node,
                             synchronization_ok_node);

  /* NOK state */
  State *state_nok_node = new State(StateType::Normal);

  /* ok automaton path creation */
  latest_s = initial_state;
  for (int i = 0; i < nb_children; i++) {
    State *state_nth = nullptr;

    if (i == nb_children - 1) {
      state_nth = state_nok_node;
    } else {
      state_nth = new State(StateType::Normal);
    }
    new_amaton->add_state(state_nth);

    State *state_nok_nth = jump_states[i];

    for (auto &n : node->get_children()) {
      Channel *channel_ok_nth = new Channel(n->get_goal() + "_nok");
      Synchronization *synchronization_ok_nth =
          new Synchronization(SynchronizationType::Receive, channel_ok_nth);
      new_amaton->add_transition(latest_s, state_nth, synchronization_ok_nth);
      amata->add_channel(channel_ok_nth);

      Channel *channel_nok_nth = new Channel(n->get_goal() + "_ok");
      Synchronization *synchronization_nok_nth =
          new Synchronization(SynchronizationType::Receive, channel_nok_nth);
      new_amaton->add_transition(latest_s, state_nok_nth,
                                 synchronization_nok_nth);
      amata->add_channel(channel_nok_nth);
    }
    latest_s = state_nth;
  }

  /* nok self-loop */
  Channel *channel_nok_node = new Channel(node->get_goal() + "_nok");
  Synchronization *synchronization_nok_node =
      new Synchronization(SynchronizationType::Send, channel_nok_node);

  new_amaton->add_transition(state_nok_node, state_nok_node,
                             synchronization_nok_node);
  amata->add_channel(channel_nok_node);

  amata->add_automaton(new_amaton);
}