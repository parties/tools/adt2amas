# Specify the minimum version for CMake
cmake_minimum_required(VERSION 3.8.0)

# project information
project(adt2amas_exec C CXX)

# Components
add_subdirectory(linear_expression)
add_subdirectory(adtree)
add_subdirectory(amas)
add_subdirectory(algorithms)

# translator library
add_library(adt2amas_lib translator.cpp)
target_link_libraries(adt2amas_lib adtree_lib amas_lib linear_expr_lib algorithms_lib)
target_include_directories(adt2amas_lib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

# CLI library
set(CLI11_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../third-party/cli11)
add_library(cli11 INTERFACE)
target_include_directories(cli11 INTERFACE ${CLI11_INCLUDE_DIR})

# The main programn
add_executable(adt2amas adt2amas.cpp)
set_property(TARGET adt2amas PROPERTY CXX_STANDARD 17)
target_link_libraries(adt2amas adt2amas_lib cli11)

# Installation folder
install(TARGETS adt2amas DESTINATION ${INSTALL_FOLDER})
