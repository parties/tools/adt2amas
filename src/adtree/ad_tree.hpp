#ifndef AD_TREE_HPP
#define AD_TREE_HPP

#include <set>
#include <string>

#include "../exception.hpp"
#include "tree_node.hpp"

/**
 * Error class to handle ADTree errors
 */
class ADTreeException : public Exception {
 public:
  explicit ADTreeException(const std::string &message) : Exception(message) {}
};

/**
 * @ingroup adtree
 *
 * Represents an Attack-Defence Tree
 */
class ADtree {
 private:
  /** Identifier of the ADtree */
  int id;

  static int current_id;

  /** Root node of the ADtree */
  TreeNode *root_;

  /** Identifiers of the nodes belonging to the ADtree */
  std::set<int> node_associated_ids_;

  /**
   * Auxiliary destroy function.
   *
   * @param[in] node Node to be destroyed
   */
  void destroy_tree(TreeNode *node);

  /**
   * Auxiliary printing function.
   *
   * @param[in] node initial node for printing the tree
   * @param info initial string
   * @param calls number of times that has been called the function
   *
   * @return A string containing the information of the tree
   */
  std::string get_info(TreeNode *node, std::string info, int calls);

  /**
   * Auxiliary printing function.
   *
   * @param[in] node initial node for printing the tree
   * @param info initial string
   * @param calls number of times that has been called the function
   * @param depth depth of the tree that will be printed
   *
   * @return A string containing the information of the tree
   */
  std::string get_info(TreeNode *node, std::string info, int calls, int depth);

  /** Auxiliary function for removing a node from the tree
   *
   * @param[in] node node to be removed
   */
  void remove_node_in_tree(TreeNode *node);

  /**
   * Auxiliary function for removing a node with no parents.
   *
   * @param node node to be removed
   */
  void remove_children_no_parents(TreeNode *node);

 public:
  /**
   * Build a new ADtree.
   *
   * @param[in] root root node of the new ADTree.
   */
  ADtree(TreeNode *root);
  ADtree();
  ~ADtree();

  /**
   * Get the identifier of the ADtree
   *
   * @return an integer
   */
  int get_id();

  /**
   * Get the root node of the ADtree
   *
   * @return a TreeNode node
   */
  TreeNode *get_root();

  /**
   * Get the identifiers of the nodes belonging to the ADtree
   *
   * @return a std::set of integers
   */
  std::set<int> get_node_associated_ids();

  /**
   * Set the identifiers of the nodes belonging to the ADTree
   *
   * @param node_associated_ids std::set of identifiers
   */
  void set_node_associated_ids(std::set<int> node_associated_ids);

  /**
   * Get the information related to the ADtree.
   *
   * @param depth Depth of the tree. -1 refers to the full tree.
   *
   * @return a string with the information
   */
  std::string get_info(int depth = -1);

  /**
   * Get the information related to the ADtree.
   *
   * @param node root node of the tree
   * @param depth Depth of the tree. -1 refers to the full tree.
   *
   * @return a string with the information
   */
  std::string get_info(TreeNode *node, int depth);

  /**
   * Add a child to a node
   *
   * @param[in,out] new_child child node
   * @param[in,out] parent parent of the node
   */
  void add_child(TreeNode *new_child, TreeNode *parent);

  /**
   * Add a child to a node in a specific position
   *
   * @param[in,out] new_child child node
   * @param[in,out] parent parent of the node
   * @param pos position of the child (from left to right). Default: 0
   */
  void add_child_pos(TreeNode *new_child, TreeNode *parent, int pos = 0);

  /**
   * Add a parent to a node
   *
   * @param[in,out] new_parent parent node
   * @param[in,out] child child node
   */
  void add_parent(TreeNode *new_parent, TreeNode *child);

  /**
   * Add a parent to a node in a specific position
   *
   * @param[in,out] new_parent parent node
   * @param[in,out] child child node
   * @param pos position of the child (from left to right). Default: 0
   */
  void add_parent_pos(TreeNode *new_parent, TreeNode *child, int pos = 0);

  /**
   * Removes a child from a node.
   *
   * @param[in,out] cur_child node to be removed
   * @param[in,out] parent parent of the node to be removed
   */
  void remove_child(TreeNode *cur_child, TreeNode *parent);

  /**
   * Removes the child in a specific position from a node.
   *
   * @param pos position of the child
   * @param[in,out] parent parent of the node to be removed
   */
  void remove_child_pos(int pos, TreeNode *parent);

  /**
   * Removes a parent from a node.
   *
   * @param[in,out] cur_parent node to be removed
   * @param[in,out] child child of the node to be removed
   */
  void remove_parent(TreeNode *cur_parent, TreeNode *child);

  /**
   * Removes a parent in a specific position from a node.
   *
   * @param pos position of the node
   * @param[in,out] child child of the node to be removed
   */
  void remove_parent_pos(int pos, TreeNode *child);
};

#endif
