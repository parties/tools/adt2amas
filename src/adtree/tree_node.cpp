#include "tree_node.hpp"
#include <set>
#include <string>
#include <vector>

using namespace std;

TreeNode::TreeNode(string node_type, NodeRole node_role, string goal,
                   vector<Attribute*> attributes, set<int> child_associated_ids,
                   set<int> parent_associated_ids, vector<TreeNode*> children,
                   vector<TreeNode*> parents)
    : node_type_(node_type),
      node_role_(node_role),
      goal_(goal),
      attributes_(attributes),
      child_associated_ids_(child_associated_ids),
      parent_associated_ids_(parent_associated_ids),
      children_(children),
      parents_(parents) {
  id = ++current_id;
}

TreeNode::~TreeNode() {}

/* Gets */

int TreeNode::get_id() { return id; }

NodeRole TreeNode::get_node_role() { return node_role_; }

string TreeNode::get_goal() { return goal_; }

vector<Attribute*> TreeNode::get_attributes() { return attributes_; }

set<int> TreeNode::get_child_associated_ids() { return child_associated_ids_; }

set<int> TreeNode::get_parent_associated_ids() {
  return parent_associated_ids_;
}

vector<TreeNode*> TreeNode::get_children() { return this->children_; }

vector<TreeNode*> TreeNode::get_parents() { return this->parents_; }

/* Sets */

void TreeNode::set_node_role(NodeRole node_role) { node_role_ = node_role; }

void TreeNode::set_goal(string goal) { goal_ = goal; }

void TreeNode::set_attributes(vector<Attribute*> attributes) {
  attributes_ = attributes;
}

void TreeNode::set_child_associated_ids(set<int> child_associated_ids) {
  child_associated_ids_ = child_associated_ids;
}

void TreeNode::set_parent_associated_ids(set<int> parent_associated_ids) {
  parent_associated_ids_ = parent_associated_ids;
}

void TreeNode::set_children(vector<TreeNode*> children) {
  this->children_ = children;
}

void TreeNode::set_parents(vector<TreeNode*> parents) {
  this->parents_ = parents;
}

/* Printer methods */

string TreeNode::get_info() {
  string info_node;
  info_node += "(id = " + to_string(this->id) + "; ";
  info_node += "Node role = " + to_string(this->node_role_) + "; ";
  info_node += "Goal = " + this->goal_ + "; ";
  // info_node += "Value = " + to_string(this->value_) + "; ";
  info_node += "Parents = " + this->to_string_parents() + "; ";
  info_node += "Children = " + this->to_string_children() + ")";

  return info_node;
}

string TreeNode::to_string_parents() {
  string parents = "";
  for (const auto& i : this->parents_)
    parents += i == (this->parents_).back() ? to_string(i->id)
                                            : to_string(i->id) + ", ";
  return parents;
}

string TreeNode::to_string_children() {
  string children = "";
  for (const auto& i : this->children_)
    children += i == (this->children_).back() ? to_string(i->id)
                                              : to_string(i->id) + ", ";
  return children;
}

string TreeNode::get_node_type() { return node_type_; }

void TreeNode::accept(ADTreeVisitor& visitor){};

int TreeNode::current_id = -1;
