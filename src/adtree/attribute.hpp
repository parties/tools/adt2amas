#ifndef ATTRIBUTE_HPP
#define ATTRIBUTE_HPP

#include <string>
#include "linear_expression.hpp"

/**
 * @ingroup adtree_attribute
 *
 * Represents the node's attributes
 */
class Attribute {
 protected:
  /** value of the attribute. */
  int value_;

  /** name of the attribute. */
  std::string type_;

  /** function to compute the value's attribute. */
  LinearExpression* computation_function_;

 public:
  /**
   * Creates a new attribute
   *
   * @param type name of the attribute
   * @param value initial value of the attribute
   * @param[in] computation_function function to compute new values
   */
  Attribute(std::string type = "", int value = 0,
            LinearExpression* computation_function = nullptr);

  /**
   * Gets the value of the attribute.
   *
   * @return an integer with the value.
   */
  int get_value();

  /**
   * Sets the value of the attribute.
   *
   * @param value new value.
   */
  void set_value(int value);

  /**
   * Gets the name of the attribute.
   *
   * @return a string with the name.
   */
  std::string get_type();

  /**
   * Sets the function to compute the values of the attribute.
   *
   * @param[in] linear_expr linear expression representing the computation
   * function.
   */
  void set_computation_function(LinearExpression* linear_expr);

  /**
   * Gets the computation function
   *
   * @return a linear expression representing the computation function.
   */
  LinearExpression* get_computation_function();
};

#endif
