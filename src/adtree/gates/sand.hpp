#ifndef SAND_HPP
#define SAND_HPP

#include <string>
#include <vector>
#include "gate.hpp"

/**
 * @ingroup adtree_node
 *
 * Represents a tree SAND gate.
 */
class Sand : public Gate {
 public:
  /**
   * Creates a new AND gate
   *
   * @param gate_role gate role
   * @param goal gate's name
   * @param attributes list of gate's attributes
   */
  Sand(NodeRole gate_role = NodeRole::Attack, std::string goal = "",
       std::vector<Attribute*> attributes = {});

  ~Sand();

  std::string get_info();
};

#endif
