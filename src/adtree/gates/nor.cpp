#include "nor.hpp"

using namespace std;

Nor::Nor(NodeRole node_role, string goal, vector<Attribute*> attributes,
         Inequality* condition)
    : Gate("Nor", node_role, goal, attributes) {
  condition_ = condition;
}
Nor::~Nor() {}

string Nor::get_info() {
  string info_node = "Nor";
  info_node += "(id = " + to_string(this->id) + "; ";
  info_node += "Node role = " + to_string(this->node_role_) + "; ";
  info_node += "Goal = " + this->goal_ + "; ";
  // info_node += "Value = " + to_string(this->value_) + "; ";
  info_node += "Parents = " + this->to_string_parents() + "; ";
  info_node += "Children = " + this->to_string_children() + ")";

  return info_node;
}

Inequality* Nor::get_condition() { return this->condition_; }
