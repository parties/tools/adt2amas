#ifndef SNAND_HPP
#define SNAND_HPP

#include <string>
#include <vector>
#include "gate.hpp"

/**
 * @ingroup adtree_node
 *
 * Represents a tree SNAND gate.
 */
class Snand : public Gate {
 public:
  /**
   * Creates a new AND gate
   *
   * @param gate_role gate role
   * @param goal gate's name
   * @param attributes list of gate's attributes
   */
  Snand(NodeRole gate_role = NodeRole::Attack, std::string goal = "",
        std::vector<Attribute*> attributes = {});

  ~Snand();

  std::string get_info();
};

#endif
