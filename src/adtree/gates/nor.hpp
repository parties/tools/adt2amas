#ifndef NOR_HPP
#define NOR_HPP

#include <string>
#include <vector>
#include "gate.hpp"
#include "inequality.hpp"

/**
 * @ingroup adtree_node
 *
 * Represents a tree NOR gate.
 */
class Nor : public Gate {
 private:
  Inequality* condition_;

 public:
  /**
   * Creates a new AND gate
   *
   * @param gate_role gate role
   * @param goal gate's name
   * @param attributes list of gate's attributes
   * @param condition constraint to be successfull
   */
  Nor(NodeRole gate_role = NodeRole::Attack, std::string goal = "",
      std::vector<Attribute*> attributes = {}, Inequality* condition = nullptr);

  ~Nor();

  std::string get_info();

  /**
   * Gets the gate's constraint to be succesfull
   *
   * @return an inequality pointer
   */
  Inequality* get_condition();
};

#endif
