#ifndef OR_HPP
#define OR_HPP

#include <string>
#include <vector>
#include "gate.hpp"

/**
 * @ingroup adtree_node
 *
 * Represents a tree OR gate.
 */
class Or : public Gate {
 public:
  /**
   * Creates a new AND gate
   *
   * @param gate_role gate role
   * @param goal gate's name
   * @param attributes list of gate's attributes
   */
  Or(NodeRole gate_role = NodeRole::Attack, std::string goal = "",
     std::vector<Attribute*> attributes = {});

  ~Or();

  std::string get_info();
};

#endif
