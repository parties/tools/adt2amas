#include "and.hpp"

using namespace std;

And::And(NodeRole node_role, string goal, vector<Attribute*> attributes)
    : Gate("And", node_role, goal, attributes) {}
And::~And() {}

string And::get_info() {
  string info_node = "And";
  info_node += "(id = " + to_string(this->id) + "; ";
  info_node += "Node role = " + to_string(this->node_role_) + "; ";
  info_node += "Goal = " + this->goal_ + "; ";
  // info_node += "Value = " + to_string(this->value_) + "; ";
  info_node += "Parents = " + this->to_string_parents() + "; ";
  info_node += "Children = " + this->to_string_children() + ")";

  return info_node;
}
