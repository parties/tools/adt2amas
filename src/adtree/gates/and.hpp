#ifndef AND_HPP
#define AND_HPP

#include <string>
#include <vector>
#include "attribute.hpp"
#include "gate.hpp"

/**
 * @ingroup adtree_node
 *
 * Represents a tree AND gate.
 */
class And : public Gate {
 public:
  /**
   * Creates a new AND gate
   *
   * @param gate_role gate role
   * @param goal gate's name
   * @param attributes list of gate's attributes
   */
  And(NodeRole gate_role = NodeRole::Attack, std::string goal = "",
      std::vector<Attribute*> attributes = {});

  ~And();

  std::string get_info();
};

#endif
