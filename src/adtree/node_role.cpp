#include "node_role.hpp"

using namespace std;

string to_string(NodeRole node_type) {
  string _nt = "";
  switch (node_type) {
    case NodeRole::Defence:
      _nt = "Defence";
      break;
    case NodeRole::Attack:
      _nt = "Attack";
    default:
      break;
  }
  return _nt;
}
