#include "gate.hpp"

using namespace std;

Gate::Gate(string gate_type, NodeRole gate_role, string goal,
           vector<Attribute*> attributes)
    : TreeNode(gate_type, gate_role, goal, attributes) {}
Gate::~Gate() {}

void Gate::accept(ADTreeVisitor& visitor) {
  visitor.visit(this);

  for (auto& child : this->get_children()) {
    child->accept(visitor);
  }
}
