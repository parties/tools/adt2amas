#include "leaf.hpp"

using namespace std;

Leaf::Leaf(NodeRole leaf_role, string goal, vector<Attribute*> attributes)
    : TreeNode("Leaf", leaf_role, goal, attributes) {}
Leaf::~Leaf() {}

string Leaf::get_info() {
  string info_node = "Leaf";
  info_node += "(id = " + to_string(this->id) + "; ";
  info_node += "Node role = " + to_string(this->node_role_) + "; ";
  info_node += "Goal = " + this->goal_ + "; ";
  // info_node += "Value = " + to_string(this->value_) + "; ";
  info_node += "Parents = " + this->to_string_parents() + "; ";
  info_node += "Children = " + this->to_string_children() + ")";

  return info_node;
}

void Leaf::accept(ADTreeVisitor& visitor) { visitor.visit(this); }
