#ifndef NODE_ROLE_HPP
#define NODE_ROLE_HPP

#include <string>

/**
 * @ingroup adtree
 *
 * Represents the node type
 */
enum class NodeRole { Defence, Attack };

/**
 * Returns the string representation of the node type.
 *
 * @param node_type node type.
 *
 * @return a string with the node type.
 */
std::string to_string(NodeRole node_type);

#endif
