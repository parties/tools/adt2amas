#include "cost_attribute.hpp"

CostAttribute::CostAttribute(int value, LinearExpression* computation_function)
    : Attribute("cost", value, computation_function) {}
