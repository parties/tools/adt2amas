#ifndef TIME_ATTRIBUTE_HPP
#define TIME_ATTRIBUTE_HPP

#include "attribute.hpp"

/**
 * @ingroup adtree_attribute
 *
 * Represents the time attribute
 */
class TimeAttribute : public Attribute {
 public:
  /**
   * Creates a new time attribute
   *
   * @param value the attribute value
   * @param[in] computation_function the computation function of the attribute
   */
  TimeAttribute(int value, LinearExpression* computation_function = nullptr);
};

#endif
