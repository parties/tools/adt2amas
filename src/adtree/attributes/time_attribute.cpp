#include "time_attribute.hpp"

TimeAttribute::TimeAttribute(int value, LinearExpression* computation_function)
    : Attribute("time", value, computation_function) {}
