#ifndef COST_ATTRIBUTE_HPP
#define COST_ATTRIBUTE_HPP

#include "attribute.hpp"

/**
 * @ingroup adtree_attribute
 *
 * Represents the cost attribute
 */
class CostAttribute : public Attribute {
 public:
  /**
   * Creates a new cost attribute
   *
   * @param value the attribute value
   * @param[in] computation_function the computation function of the attribute
   */
  CostAttribute(int value, LinearExpression* computation_function = nullptr);
};

#endif
