#ifndef FACTORY_HPP
#define FACTORY_HPP

#include <algorithm>
#include <iterator>
#include <string>
#include <vector>

#include "attribute.hpp"
#include "attributes/cost_attribute.hpp"
#include "attributes/time_attribute.hpp"
#include "constant.hpp"
#include "function.hpp"
#include "operator.hpp"
#include "operators/addition.hpp"
#include "tree_node.hpp"
#include "variable.hpp"

std::vector<std::string> get_children_goals(std::vector<TreeNode*> children,
                                            std::string prefix = "") {
  std::vector<std::string> children_goals{};
  std::transform(children.begin(), children.end(),
                 std::back_inserter(children_goals),
                 [prefix](TreeNode* c) { return prefix + c->get_goal(); });
  return children_goals;
}

std::vector<LinearExpression*> get_children_goals_variables(
    std::vector<TreeNode*> children, std::string prefix = "") {
  std::vector<LinearExpression*> children_goals{};

  std::transform(
      children.begin(), children.end(), std::back_inserter(children_goals),
      [prefix](TreeNode* c) { return new Variable(prefix + c->get_goal()); });
  return children_goals;
}

std::vector<Attribute*> sand_factory_attributes(
    int cost, int time, std::vector<TreeNode*> children) {
  std::vector<LinearExpression*> cost_children_goals =
      get_children_goals_variables(children, "cost_");

  cost_children_goals.insert(cost_children_goals.begin(), new Constant(cost));

  std::vector<LinearExpression*> time_children_goals =
      get_children_goals_variables(children, "time_");

  time_children_goals.insert(time_children_goals.begin(), new Constant(time));

  Operator* cost_function = new Addition(cost_children_goals);
  CostAttribute* cost_attr = new CostAttribute(cost, cost_function);

  Operator* time_function = new Addition(time_children_goals);
  TimeAttribute* time_attr = new TimeAttribute(time, time_function);

  return {cost_attr, time_attr};
}

std::vector<Attribute*> leaf_factory_attributes(int cost, int time) {
  CostAttribute* cost_attr = new CostAttribute(cost, new Constant(cost));
  TimeAttribute* time_attr = new TimeAttribute(time, new Constant(time));
  return {cost_attr, time_attr};
}

std::vector<Attribute*> and_factory_attributes(
    int cost, int time, std::vector<TreeNode*> children) {
  std::vector<std::string> cost_children_goals =
      get_children_goals(children, "cost_");
  std::vector<std::string> time_children_goals =
      get_children_goals(children, "time_");

  Operator* cost_function = new Addition(
      {new Constant(cost), new Function("costC", cost_children_goals)});

  Operator* time_function = new Addition(
      {new Constant(time), new Function("timeC", time_children_goals)});

  CostAttribute* cost_attr = new CostAttribute(cost, cost_function);
  TimeAttribute* time_attr = new TimeAttribute(time, time_function);

  return {cost_attr, time_attr};
}

std::vector<Attribute*> nand_factory_attributes(int cost, int time,
                                                TreeNode* child) {
  Operator* cost_function = new Addition(
      {new Constant(cost), new Variable("cost_" + child->get_goal())});

  Operator* time_function = new Addition(
      {new Constant(time), new Variable("time_" + child->get_goal())});

  CostAttribute* cost_attr = new CostAttribute(cost, cost_function);
  TimeAttribute* time_attr = new TimeAttribute(time, time_function);

  return {cost_attr, time_attr};
}

// TODO(jaime): Check with the paper
std::vector<Attribute*> snand_factory_attributes(int cost, int time,
                                                 TreeNode* child) {
  Operator* cost_function = new Addition(
      {new Constant(cost), new Variable("cost_" + child->get_goal())});

  Operator* time_function = new Addition(
      {new Constant(time), new Variable("time_" + child->get_goal())});

  CostAttribute* cost_attr = new CostAttribute(cost, cost_function);
  TimeAttribute* time_attr = new TimeAttribute(time, time_function);

  return {cost_attr, time_attr};
}

std::vector<Attribute*> or_factory_attributes(int cost, int time,
                                              std::string node_goal) {
  Operator* cost_function =
      new Addition({new Constant(cost), new Variable("cost_" + node_goal)});

  Operator* time_function =
      new Addition({new Constant(time), new Variable("time_" + node_goal)});

  CostAttribute* cost_attr = new CostAttribute(cost, cost_function);
  TimeAttribute* time_attr = new TimeAttribute(time, time_function);

  return {cost_attr, time_attr};
}

#endif
