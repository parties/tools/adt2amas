#ifndef LEAF_HPP
#define LEAF_HPP

#include <string>
#include <vector>

#include "node_role.hpp"
#include "parser/adtree_visitor.hpp"
#include "tree_node.hpp"

class ADTreeVisitor;

/**
 * @ingroup adtree_node
 *
 * Represents a tree's leaf.
 */
class Leaf : public TreeNode {
 public:
  /**
   * Creates a new leaf
   *
   * @param leaf_role leaf role
   * @param goal leaf's name
   * @param attributes list of leaf's attributes
   */
  Leaf(NodeRole leaf_role = NodeRole::Attack, std::string goal = "",
       std::vector<Attribute *> attributes = {});
  ~Leaf();

  /**
   * Gets the string representation of the node.
   *
   * @return the information related to the node.
   */
  std::string get_info();

  /**
   * A public method implementing the visitor pattern
   *
   * @param visitor is a visitor instance
   */
  void accept(ADTreeVisitor &visitor);
};

#endif
