#include "attribute.hpp"
#include <string>

using namespace std;

Attribute::Attribute(string type, int value,
                     LinearExpression* computation_function)
    : type_(type), value_(value), computation_function_(computation_function) {}

int Attribute::get_value() { return this->value_; }

void Attribute::set_value(int value) { this->value_ = value; }

string Attribute::get_type() { return this->type_; }

void Attribute::set_computation_function(LinearExpression* linear_expr) {
  this->computation_function_ = linear_expr;
};
LinearExpression* Attribute::get_computation_function() {
  return this->computation_function_;
};
