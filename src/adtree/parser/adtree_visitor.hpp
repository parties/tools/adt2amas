#ifndef ADTREE_VISITOR_HPP
#define ADTREE_VISITOR_HPP

#include "gate.hpp"
#include "leaf.hpp"

class Gate;
class Leaf;

/**
 * @ingroup adtree_visitor
 * Visitor interface for the ADTree module
 */
class ADTreeVisitor {
 public:
  /**
   * Visits a gate
   *
   * @param[in] gate gate
   */
  virtual void visit(Gate* gate) = 0;

  /**
   * Visits a leaf
   *
   * @param[in] leaf leaf
   */
  virtual void visit(Leaf* leaf) = 0;
};

#endif  // ADTREE_VISITOR_HPP
