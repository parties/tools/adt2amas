#ifndef ATTRIBUTE_VISITOR
#define ATTRIBUTE_VISITOR

#include <string>
#include <vector>
#include "adtree_visitor.hpp"
#include "gate.hpp"
#include "leaf.hpp"

/**
 * @ingroup adtree_visitor
 * Visitor attributes for the ADTree module
 */
class AttributeVisitor : public ADTreeVisitor {
 public:
  /**
   * Returns a new Attribute visitor
   *
   * @param attribute_type type of the attribute to be collected by the visitor.
   */
  AttributeVisitor(std::string attribute_type);

  void visit(Gate* gate);
  void visit(Leaf* leaf);

  /**
   * Gets the collected attributes
   */
  std::vector<int> get_values();

 private:
  /** vector of values */
  std::vector<int> values_;

  /** type of attributed collected */
  std::string type_;
};

#endif  // ATTRIBUTE_VISITOR
