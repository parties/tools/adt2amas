#include "attribute_visitor.hpp"

#include <algorithm>
#include <iostream>
#include <vector>

#include "attribute.hpp"

using namespace std;

AttributeVisitor::AttributeVisitor(string attribute_type)
    : type_(attribute_type){};

void AttributeVisitor::visit(Gate* gate) {
  vector<Attribute*> attrs = gate->get_attributes();

  for (auto& a : attrs) {
    if (a->get_type() == this->type_) {
      this->values_.push_back(a->get_value());
    }
  }
};

void AttributeVisitor::visit(Leaf* leaf) {
  vector<Attribute*> attrs = leaf->get_attributes();

  for (auto& a : attrs) {
    if (a->get_type() == this->type_) {
      this->values_.push_back(a->get_value());
    }
  }
};

vector<int> AttributeVisitor::get_values() { return this->values_; }
