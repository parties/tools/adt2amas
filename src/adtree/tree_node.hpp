#ifndef TREE_NODE_HPP
#define TREE_NODE_HPP

#include <set>
#include <string>
#include <vector>
#include "attribute.hpp"
#include "node_role.hpp"

class ADTreeVisitor;

/**
 * @ingroup adtree
 *
 * Represents a tree's node.
 */
class TreeNode {
 protected:
  /** node identifier */
  int id;

  /** node role */
  NodeRole node_role_;

  /** node type */
  std::string node_type_;

  static int current_id;

  /** node goal */
  std::string goal_;

  /** node's attributes */
  std::vector<Attribute *> attributes_;

  /** node's children */
  std::vector<TreeNode *> children_;

  /** identifiers of the node's children */
  std::set<int> child_associated_ids_;

  /** node's parent */
  std::vector<TreeNode *> parents_;

  /** identifiers of the node's parent */
  std::set<int> parent_associated_ids_;

 public:
  /**
   * Creates a new node.
   *
   * @param node_type node type
   * @param node_role node role
   * @param goal node name
   * @param attributes list of node's attributes
   * @param child_associated_ids set of the identifiers of node's children
   * @param parent_associated_ids set of the identifiers of node's parents
   * @param children list with the node's children
   * @param parents list with the node's parents
   */
  TreeNode(std::string node_type = "", NodeRole node_role = NodeRole::Attack,
           std::string goal = "", std::vector<Attribute *> attributes = {},
           std::set<int> child_associated_ids = {},
           std::set<int> parent_associated_ids = {},
           std::vector<TreeNode *> children = {},
           std::vector<TreeNode *> parents = {});

  ~TreeNode();

  /**
   * Gets the node's identifier
   *
   * @return integer representing the identifier
   */
  int get_id();

  /**
   * Gets the node role.
   *
   * @return the node role
   */
  NodeRole get_node_role();

  /** Get the node's name
   *
   * @return string of the name
   */
  std::string get_goal();

  /** Gets the type of the node
   *
   * @return string of the type
   */
  std::string get_node_type();

  /**
   * Gets the list of the node's attributes
   *
   * @return a vector with the attributes
   */
  std::vector<Attribute *> get_attributes();

  /**
   * Gets the set of the identifiers of the node's children
   *
   * @return a set with the identifiers
   */
  std::set<int> get_child_associated_ids();

  /**
   * Gets the set of the identifiers of the node's parents
   *
   * @return a set with the identifiers
   */
  std::set<int> get_parent_associated_ids();

  /**
   * Gets the list of the node's children
   *
   * @return a vector with the children
   */
  std::vector<TreeNode *> get_children();

  /**
   * Gets the list of the node's parents
   *
   * @return a vector with the parents
   */
  std::vector<TreeNode *> get_parents();

  /**
   * Sets the node role
   *
   * @param node_role node role
   */
  void set_node_role(NodeRole node_role);

  /**
   * Sets the set of identifiers of the node's children.
   *
   * @param child_associated_ids set of identifiers
   */
  void set_child_associated_ids(std::set<int> child_associated_ids);

  /**
   * Sets the set of identifiers of the node's parents.
   *
   * @param parent_associated_ids set of identifiers
   */
  void set_parent_associated_ids(std::set<int> parent_associated_ids);

  /**
   * Sets the list of node's children.
   *
   * @param children list of nodes
   */
  void set_children(std::vector<TreeNode *> children);

  /**
   * Sets the list of node's parent.
   *
   * @param parents list of nodes
   */
  void set_parents(std::vector<TreeNode *> parents);

  /**
   * Sets the node' name
   *
   * @param goal node's name
   */
  void set_goal(std::string goal);

  /**
   * Sets the list of the node's attributes.
   *
   * @param attributes node's attributes
   */
  void set_attributes(std::vector<Attribute *> attributes);

  /**
   * Gets the string representation of the node's parents
   *
   * @return a string of the node's parents.
   */
  std::string to_string_parents();

  /**
   * Gets the string representation of the node's children
   *
   * @return a string of the node's children.
   */
  std::string to_string_children();

  /**
   * Gets the string representation of the node.
   *
   * @return the information related to the node.
   */
  virtual std::string get_info();

  /**
   * A public method implementing the visitor pattern
   *
   * @param visitor is a visitor instance
   */
  virtual void accept(ADTreeVisitor &visitor);
};

#endif
