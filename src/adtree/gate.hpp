#ifndef GATE_HPP
#define GATE_HPP

#include <string>
#include <vector>

#include "attribute.hpp"
#include "node_role.hpp"
#include "parser/adtree_visitor.hpp"
#include "tree_node.hpp"

class ADTreeVisitor;

/**
 * @ingroup adtree_node
 *
 * Represents a tree's gate.
 */
class Gate : public TreeNode {
 public:
  /**
   * Creates a new gate
   *
   * @param gate_type gate type
   * @param gate_role gate role
   * @param goal gate's name
   * @param attributes list of gate's attributes
   */
  Gate(std::string gate_type = "", NodeRole gate_role = NodeRole::Attack,
       std::string goal = "", std::vector<Attribute *> attributes = {});

  ~Gate();

  /**
   * A public method implementing the visitor pattern
   *
   * @param visitor is a visitor instance
   */
  void accept(ADTreeVisitor &visitor);
};

#endif
