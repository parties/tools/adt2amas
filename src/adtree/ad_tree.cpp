#include "ad_tree.hpp"

using namespace std;

// Tree implementation adapted from:
// https://gist.github.com/toboqus/def6a6915e4abd66e922

int ADtree::current_id = 0;

ADtree::ADtree(TreeNode *root) : root_(root) {
  id = ++current_id;
  node_associated_ids_.insert(root->get_id());
}

ADtree::~ADtree() {}
ADtree::ADtree() {}

/* Gets */

int ADtree::get_id() { return id; }

TreeNode *ADtree::get_root() { return this->root_; }

set<int> ADtree::get_node_associated_ids() { return node_associated_ids_; }

/* Sets */

void ADtree::set_node_associated_ids(set<int> node_associated_ids) {
  node_associated_ids_ = node_associated_ids;
}

/* Destroy */

void ADtree::destroy_tree(TreeNode *node) {
  if (node != nullptr) {
    for (auto &i : node->get_children()) {
      remove_child(i, node);
      destroy_tree(i);
    }
  }
}

/* Print functions */

string ADtree::get_info(int depth) {
  return depth == -1 ? get_info(this->root_, "", 0)
                     : get_info(this->root_, depth);
}
string ADtree::get_info(TreeNode *node, int depth) {
  if (depth < 0) throw ADTreeException("depth can not be negative");

  auto node_in_tree = this->node_associated_ids_.find(node->get_id()) !=
                      this->node_associated_ids_.end();

  if (!node_in_tree) throw ADTreeException("node must be in the tree");
  return get_info(node, "", 0, depth);
}

string ADtree::get_info(TreeNode *node, string info, int calls) {
  info = node->get_info() + "\n";
  info.insert(0, calls, '\t');
  for (auto &i : node->get_children()) info += get_info(i, info, calls + 1);
  return info;
}

string ADtree::get_info(TreeNode *node, string info, int calls, int depth) {
  info = node->get_info() + "\n";
  info.insert(0, calls, '\t');
  if (calls < depth) {
    for (auto &i : node->get_children())
      info += get_info(i, info, calls + 1, depth);
  }
  return info;
}

/* Add functions */

void ADtree::add_child(TreeNode *new_child, TreeNode *parent) {
  // parent must be in the tree
  auto parent_in_tree = this->node_associated_ids_.find(parent->get_id()) !=
                        this->node_associated_ids_.end();
  if (!parent_in_tree)
    throw ADTreeException(
        "parent must be in the tree to add an external child");

  string node_behaviour = parent->get_node_type();

  // Check if the parent is a counter gate
  bool countermeasure_gate =
      (node_behaviour == "Nand" || node_behaviour == "Nor" ||
       node_behaviour == "Snand" || node_behaviour == "Snor") &&
      parent->get_children().size() == 2;

  if (new_child->get_id() != parent->get_id() && !countermeasure_gate) {
    set<int> tmp_set = parent->get_child_associated_ids();
    auto child_insert_id = tmp_set.insert(new_child->get_id());
    parent->set_child_associated_ids(tmp_set);

    if (child_insert_id.second) {
      vector<TreeNode *> tmp_vector = parent->get_children();
      tmp_vector.push_back(new_child);
      parent->set_children(tmp_vector);
      this->node_associated_ids_.insert(new_child->get_id());
      add_parent(parent, new_child);
    }
  }
}

void ADtree::add_parent(TreeNode *new_parent, TreeNode *child) {
  // child and parent must be in the tree
  auto child_in_tree = this->node_associated_ids_.find(child->get_id()) !=
                       this->node_associated_ids_.end();
  auto parent_in_tree = this->node_associated_ids_.find(new_parent->get_id()) !=
                        this->node_associated_ids_.end();
  if (!child_in_tree || !parent_in_tree)
    throw ADTreeException(
        "child and parent must be in the tree to add this node as a parent to "
        "this child");

  if (new_parent->get_id() != child->get_id()) {
    set<int> tmp_set = child->get_parent_associated_ids();
    auto parent_insert_id = tmp_set.insert(new_parent->get_id());
    child->set_parent_associated_ids(tmp_set);
    if (parent_insert_id.second) {
      vector<TreeNode *> tmp_vector = child->get_parents();
      tmp_vector.push_back(new_parent);
      child->set_parents(tmp_vector);
      add_child(child, new_parent);
    }
  }
}

void ADtree::add_child_pos(TreeNode *new_child, TreeNode *parent, int pos) {
  if (pos < 0) throw ADTreeException("pos can't be negative");
  if (pos > parent->get_child_associated_ids().size())
    throw ADTreeException(
        "pos is greater than children's number of this node.");

  auto parent_in_tree = this->node_associated_ids_.find(parent->get_id()) !=
                        this->node_associated_ids_.end();
  if (!parent_in_tree)
    throw ADTreeException(
        "parent must be in the tree to add an external child");

  string node_behaviour = parent->get_node_type();
  auto negated_gate = (node_behaviour == "Nand" || node_behaviour == "Nor" ||
                       node_behaviour == "Snand" || node_behaviour == "Snor") &&
                      parent->get_children().size() == 2;

  if (new_child->get_id() != parent->get_id() && !negated_gate) {
    set<int> tmp_set = parent->get_child_associated_ids();
    auto child_insert_id = tmp_set.insert(new_child->get_id());
    parent->set_child_associated_ids(tmp_set);
    if (child_insert_id.second) {
      vector<TreeNode *> tmp_vector = parent->get_children();
      auto it = tmp_vector.begin();
      it = tmp_vector.insert(it + pos, new_child);
      parent->set_children(tmp_vector);
      this->node_associated_ids_.insert(new_child->get_id());
      add_parent(parent, new_child);
    }
  }
}

void ADtree::add_parent_pos(TreeNode *new_parent, TreeNode *child, int pos) {
  if (pos < 0) throw ADTreeException("pos can't be negative");
  if (pos > child->get_parent_associated_ids().size())
    throw ADTreeException("pos is greater than parent's number of this node.");

  auto child_in_tree = this->node_associated_ids_.find(child->get_id()) !=
                       this->node_associated_ids_.end();
  auto parent_in_tree = this->node_associated_ids_.find(new_parent->get_id()) !=
                        this->node_associated_ids_.end();
  if (!child_in_tree || !parent_in_tree)
    throw ADTreeException(
        "child and new parent must be in the tree to add this parent to this "
        "child");

  if (new_parent->get_id() != child->get_id()) {
    set<int> tmp_set = child->get_parent_associated_ids();
    auto parent_insert_id = tmp_set.insert(new_parent->get_id());
    child->set_parent_associated_ids(tmp_set);

    if (parent_insert_id.second) {
      vector<TreeNode *> tmp_vector = child->get_parents();
      auto it = tmp_vector.begin();
      it = tmp_vector.insert(it + pos, new_parent);
      child->set_parents(tmp_vector);
      add_child(child, new_parent);
    }
  }
}

/* Remove functions */

void ADtree::remove_child(TreeNode *cur_child, TreeNode *parent) {
  // Check that parent and child are in the tree
  bool parent_in_tree = this->node_associated_ids_.find(parent->get_id()) !=
                        this->node_associated_ids_.end();
  bool cur_child_in_tree =
      this->node_associated_ids_.find(cur_child->get_id()) !=
      this->node_associated_ids_.end();

  if (!parent_in_tree || !cur_child_in_tree)
    throw ADTreeException(
        "both child and parent must be in the tree to remove their child");

  set<int> tmp_set = parent->get_child_associated_ids();
  bool child_is_in_id = (tmp_set.find(cur_child->get_id())) != (tmp_set.end());

  if (child_is_in_id) {
    vector<TreeNode *> tmp_vector = parent->get_children();
    auto it = tmp_vector.begin();
    for (; it != tmp_vector.end() && (*it)->get_id() != cur_child->get_id();
         it++) {
    }
    it = tmp_vector.erase(it);
    parent->set_children(tmp_vector);

    set<int> tmp_set = parent->get_child_associated_ids();
    tmp_set.erase(cur_child->get_id());
    parent->set_child_associated_ids(tmp_set);

    remove_parent(parent, cur_child);
    remove_node_in_tree(cur_child);
  }
}

void ADtree::remove_parent(TreeNode *cur_parent, TreeNode *child) {
  // Check that parent and child are in the tree
  bool cur_parent_in_tree =
      this->node_associated_ids_.find(cur_parent->get_id()) !=
      this->node_associated_ids_.end();
  bool child_in_tree = this->node_associated_ids_.find(child->get_id()) !=
                       this->node_associated_ids_.end();

  if (!child_in_tree || !cur_parent_in_tree)
    throw ADTreeException(
        "both parent and child must be in the tree to remove their parent");

  set<int> tmp_set = child->get_parent_associated_ids();
  auto parent_is_in_id = tmp_set.find(cur_parent->get_id()) != tmp_set.end();

  if (parent_is_in_id) {
    vector<TreeNode *> tmp_vector = child->get_parents();
    auto it = tmp_vector.begin();
    for (; it != tmp_vector.end() && (*it)->get_id() != cur_parent->get_id();
         it++) {
    }
    it = tmp_vector.erase(it);
    child->set_parents(tmp_vector);

    set<int> tmp_set = child->get_parent_associated_ids();
    tmp_set.erase(cur_parent->get_id());
    child->set_parent_associated_ids(tmp_set);

    if (tmp_vector.empty()) remove_children_no_parents(child);
    remove_child(child, cur_parent);
  }
}

void ADtree::remove_child_pos(int pos, TreeNode *parent) {
  if (pos < 0) throw ADTreeException("pos can't be negative");
  if (pos >= parent->get_child_associated_ids().size())
    throw ADTreeException(
        "pos is greater than children's number of this node.");

  if (parent->get_child_associated_ids().size() <= 0)
    throw ADTreeException("no children to remove");

  TreeNode *tmp_node = nullptr;
  tmp_node = parent->get_children()[pos];
  remove_child(tmp_node, parent);
}

void ADtree::remove_parent_pos(int pos, TreeNode *child) {
  if (pos < 0) throw ADTreeException("pos can't be negative");
  if (pos >= child->get_parent_associated_ids().size())
    throw ADTreeException("pos is greater than parent's number of this node.");

  if (child->get_parent_associated_ids().size() <= 0)
    throw ADTreeException("no parents to remove");

  TreeNode *tmp_node = nullptr;
  tmp_node = child->get_parents()[pos];
  remove_parent(tmp_node, child);
}

void ADtree::remove_node_in_tree(TreeNode *node) {
  bool remove_child_in_tree = 1;
  for (auto &i : node->get_parent_associated_ids()) {
    if (this->node_associated_ids_.find(i) != this->node_associated_ids_.end())
      remove_child_in_tree = 0;
  }
  if (remove_child_in_tree == 1) {
    this->node_associated_ids_.erase(node->get_id());
    for (auto &i : node->get_children()) remove_node_in_tree(i);
  }
}

void ADtree::remove_children_no_parents(TreeNode *node) {
  for (auto &i : node->get_children()) remove_child(i, node);
}
