#ifndef SRC_ALGORITHMS_TREE_HPP_
#define SRC_ALGORITHMS_TREE_HPP_

#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <vector>

#include "../exception.hpp"

/**
 * Error class to handle tree errors
 */
class TreeException : public Exception {
 public:
  explicit TreeException(const std::string &message) : Exception(message) {}
};

/**
 * A directed graph that supports only a single edge between vertices.
 */
template <typename L, typename V>
class Tree {
 public:
  /** Default construction */
  Tree() = default;

  /**
   * checks whether a vertex belongs to the tree
   *
   * @param id vertex's identifier
   */
  bool in_tree(const L &id) const {
    return this->_edges.find(id) != this->_edges.end();
  }

  /**
   * Add a vertex.
   *
   * @param id identifier of the vertex
   * @param data data of the vertex
   */
  virtual void add_vertex(const L &id, const V &data) {
    if (this->in_tree(id)) {
      throw TreeException("vertex (" + id + ") already exists");
    }

    // add vertex iff it does not belong to the tree
    Vertex v{id, data, std::vector<L>{}, std::vector<L>{}};
    this->_edges.emplace(id, v);
  }

  /**
   * Remove a vertex and the edges from/to it
   *
   * @param id identifier of the vertex
   * */
  virtual void remove_vertex(const L &id) {
    if (!this->in_tree(id)) {
      throw TreeException("vertex (" + id + ") does not exist");
    }

    // remove edges to children
    for (const auto &child : this->get_children(id)) {
      this->remove_edge(id, child);
    }

    // remove edges from parents
    for (const auto &parent : this->get_parents(id)) {
      this->remove_edge(parent, id);
    }

    // remove vertex
    this->_edges.erase(id);
  }

  /**
   * Remove the subtree from vertex
   *
   * @param vertex identifier of the root of the subtree
   */
  void remove_subtree(const L &vertex) {
    // vertex does not exist
    if (!this->in_tree(vertex)) {
      throw TreeException("Vertex (" + vertex + ") does not exist.");
    }

    for (const auto &child : this->get_children(vertex)) {
      // remove until a join of branches.
      if (this->get_parents(child).size() <= 1) {
        this->remove_subtree(child);
      }
    }

    this->remove_vertex(vertex);
  }

  /**
   * Add an edge from a vertex to another.
   *
   * @param from connect from
   * @param to connect to
   */
  void add_edge(const L &from, const L &to) {
    if (!(this->in_tree(from) && this->in_tree(to))) {
      throw TreeException("Vertices (" + from + ") and (" + to +
                          ") must belong to the tree.");
    }

    this->_edges.at(from).children.push_back(to);
    this->_edges.at(to).parents.push_back(from);
  }

  /**
   * Removes an edge between  vertices
   *
   * @param from source vertex of the edge
   * @param to target vertex of the edge
   * */
  void remove_edge(const L &from, const L &to) {
    if (!(this->in_tree(from) && this->in_tree(to))) {
      throw TreeException("Vertices (" + from + ") and (" + to +
                          ") must belong to the tree.");
    }

    // update children relation
    std::vector<L> children = this->_edges.at(from).children;
    children.erase(std::remove(children.begin(), children.end(), to),
                   children.end());
    this->_edges.at(from).children = children;

    // update parents relation
    std::vector<L> parents = this->_edges.at(to).parents;
    parents.erase(std::remove(parents.begin(), parents.end(), from),
                  parents.end());
    this->_edges.at(to).parents = parents;
  }

  /**
   * Get children of given vertex.
   *
   * @param id get the children of this vertex
   * @return children of the given vertex
   */
  const std::vector<L> get_children(const L &id) const {
    // vertex does not exist
    if (!this->in_tree(id)) {
      throw TreeException("Vertex (" + id + ") does not exist.");
    }

    return this->_edges.at(id).children;
  }

  /**
   * Get parents of given vertex.
   *
   * @param id get the parents of this vertex
   * @return parents of the given vertex
   */
  const std::vector<L> get_parents(const L &id) const {
    // vertex does not exist
    if (!this->in_tree(id)) {
      throw TreeException("Vertex (" + id + ") does not exist.");
    }

    return this->_edges.at(id).parents;
  }

  /**
   * Get vertex data
   *
   * @param id get the data of this vertex
   * @return vertex data
   */
  V get_vertex_data(const L &id) const {
    if (!this->in_tree(id)) {
      throw TreeException("Vertex (" + id + ") does not exist.");
    }
    return this->_edges.at(id).data;
  }

  /**
   * Set vertex data
   *
   * @param id identifier of the vertex to modify
   * @param data new data of the vertex
   */
  virtual void set_vertex_data(const L &id, const V &data) {
    if (!this->in_tree(id)) {
      throw TreeException("Vertex (" + id + ") does not exist.");
    }

    this->_edges.at(id).data = data;
  }

  /**
   * Get the root of the tree
   *
   * @return root vertex
   */
  L get_root() const { return this->_root; }

  /**
   * Set the root of the tree
   *
   * @param id set as root this vertex
   */
  void set_root(const L &id) {
    if (!this->in_tree(id)) {
      throw TreeException("Vertex (" + id + ") does not exist.");
    }

    this->_root = id;
  }

  /**
   * Get the leaves of the tree
   *
   * @return leaves of the tree
   */
  std::vector<L> get_leaves() const {
    return this->get_leaves(this->get_root());
  }

  /**
   * Get the leaves of the subtree whose root is the given vertex
   *
   * @param vertex root node of the subtree
   *
   * @return leaves of the subtree
   */
  std::vector<L> get_leaves(const L &vertex) const {
    if (!this->in_tree(vertex)) {
      throw TreeException("Vertex (" + vertex + ") does not exist.");
    }

    std::stack<L> stack;
    std::vector<L> leaves;

    stack.push(vertex);

    /** Depth-First Search */
    while (!stack.empty()) {
      L current = stack.top();
      stack.pop();

      std::vector<L> children = this->get_children(current);

      if (children.empty()) {
        leaves.push_back(current);
      } else {
        for (const L &child : children) {
          stack.push(child);
        }
      }
    }

    return leaves;
  }

  /**
   * Returns the vertices of the tree
   *
   * @return vertices of the tree
   */
  std::vector<L> get_vertices() const {
    std::vector<L> vertices;
    std::transform(this->_edges.begin(), this->_edges.end(),
                   std::back_inserter(vertices),
                   [](const auto &e) { return e.first; });
    return vertices;
  }

  /**
   * Returns the ancestors of a vertex
   *
   * @param vertex
   * @return std::vector<L>
   */
  std::vector<L> get_ancestors(const L &vertex) const {
    std::vector<L> ancestors;

    std::stack<L> stack;
    stack.push(vertex);

    while (!stack.empty()) {
      L current_node = stack.top();
      stack.pop();

      for (const L &p : this->get_parents(current_node)) {
        if (std::find(ancestors.begin(), ancestors.end(), p) ==
            ancestors.end()) {
          stack.push(p);
          ancestors.push_back(p);
        }
      }
    }

    return ancestors;
  }

  /**
   * Returns whether the tree is empty or not
   *
   * @returns True if the tree is empty, false otherwise
   */
  bool is_empty() const { return this->_edges.empty(); }

  /**
   * Print the tree
   */
  void print() const {
    std::queue<L> queue;
    queue.push(this->get_root());

    while (!queue.empty()) {
      L current = queue.front();
      queue.pop();

      std::cout << this->get_vertex_data(current) << std::endl;
      for (auto &child : this->get_children(current)) {
        std::cout << " - " << child << std::endl;
        queue.push(child);
      }
    }
  }

 private:
  /** Structure represeting a vertex */
  struct Vertex {
    L id;                     // identifier of the vertex
    V data;                   // data of the vertex
    std::vector<L> parents;   // parents of the vertex
    std::vector<L> children;  // children of the vertex
  };

  // root vertex
  L _root;

  // edges
  std::map<L, Vertex> _edges;
};

#endif  // SRC_ALGORITHMS_TREE_HPP_
