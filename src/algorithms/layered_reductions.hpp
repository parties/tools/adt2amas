
#ifndef SRC_ALGORITHMS_LAYERED_REDUCTIONS_HPP
#define SRC_ALGORITHMS_LAYERED_REDUCTIONS_HPP

#include <algorithm>
#include <map>
#include <numeric>
#include <stack>
#include <string>
#include <vector>

#include "../exception.hpp"
#include "ad_tree.hpp"
#include "automata.hpp"
#include "automaton.hpp"
#include "operators/addition.hpp"
#include "tree.hpp"

/**
 * Error class to handle reduction errors
 */
class LayeredReductionsException : public Exception {
 public:
  explicit LayeredReductionsException(const std::string& message)
      : Exception(message) {}
};

/**
 * Create a new array with all sub-array elements concatenated into it
 * recursively.
 *
 * @tparam T
 * @param orig Vector of vectors of type T
 * @return std::vector<T>
 */
template <typename T>
std::vector<T> flatten(const std::vector<std::vector<T>>& orig) {
  std::vector<T> ret;
  for (const auto& v : orig) ret.insert(ret.end(), v.begin(), v.end());
  return ret;
}

/**
 * Return the elements in the first vector that are not in the second one.
 *
 * @tparam T
 * @tparam T1
 * @tparam T2
 * @tparam F
 * @param vector_a first vector
 * @param vector_b second vector
 * @param equal function that allows to compare the elements of both vectors
 * @return std::vector<T1>
 */
template <typename T1, typename T2, typename F>
std::vector<T1> except(std::vector<T1>& vector_a, std::vector<T2>& vector_b,
                       F equal) {
  std::vector<T1> result;
  std::copy_if(vector_a.begin(), vector_a.end(), std::back_inserter(result),
               [&vector_b, equal](T1 a) {
                 return (std::find_if(vector_b.begin(), vector_b.end(),
                                      [a, equal](T2 b) {
                                        return equal(a, b);
                                      }) == vector_b.end());
               });

  return result;
}

/**
 * Return the elements in the first vector that are in the second one.
 *
 * @tparam T1
 * @tparam T2
 * @tparam F
 * @param vector_a first vector
 * @param vector_b second vector
 * @param equal function that allows to compare the elements of both vectors
 * @return std::vector<T1>
 */
template <typename T1, typename T2, typename F>
std::vector<T1> included(std::vector<T1>& vector_a, std::vector<T2>& vector_b,
                         F equal) {
  std::vector<T1> result;
  std::copy_if(vector_a.begin(), vector_a.end(), std::back_inserter(result),
               [&vector_b, equal](T1 a) {
                 return (std::find_if(vector_b.begin(), vector_b.end(),
                                      [a, equal](T2 b) {
                                        return equal(a, b);
                                      }) != vector_b.end());
               });

  return result;
}

/** Define node labels as strings */
typedef std::string label;

/**
 * Definition of the content of tree nodes
 */
class Node {
 public:
  Node(const std::string& label, Automaton* automaton)
      : label(label), automaton(automaton) {}

  std::string label;    /** label of the node */
  Automaton* automaton; /** automaton of the node */

  int depth = 0;  /** distance from the root */
  int height = 0; /** height of the node's tallest subtree*/
};

/**
 * Definition of Synchronization Tree (SG)
 */
class SG : public Tree<label, Node> {
 public:
  /** Mapping from depths to tree nodes */
  typedef std::map<int, std::set<label>> depths_map;

  /**
   * Computes the depths of nodes
   */
  void compute_depths() {
    std::stack<std::pair<label, int>> stack;  // (node's label, depth)

    if (this->is_empty()) return;

    label root = this->get_root();
    stack.push(std::make_pair(root, 0));

    while (!stack.empty()) {
      std::pair<label, int> current_node = stack.top();
      stack.pop();

      label node_label = current_node.first;
      int node_depth = current_node.second;
      this->depths[node_depth].insert(node_label);

      this->height = std::max(this->height, node_depth);

      for (const auto& child : this->get_children(node_label)) {
        stack.push(std::make_pair(child, node_depth + 1));
      }
    }
  }

  /**
   * Get the number of children of a node
   *
   * @param node Parent node
   * @return int
   */
  int get_number_children_node(const label& node) const {
    auto children = this->get_children(node);
    return children.size();
  }

  /**
   * Get the number of children at a specific depth
   *
   * @param depth depth
   * @return int
   */
  int get_number_children_depth(int depth) const {
    if (this->depths.empty()) {
      throw LayeredReductionsException("Depths mapping is empty");
    }

    if (depth > this->height) {
      throw LayeredReductionsException(
          "Depth is greater than height of the tree");
    }

    auto nodes = this->depths.at(depth);
    int sum = std::accumulate(nodes.begin(), nodes.end(), 0,
                              [this](int acc, label n) {
                                return acc + this->get_number_children_node(n);
                              });
    return sum;
  }

  int height = 0;    /** Height of the tree */
  depths_map depths; /** Mapping from depth to tree nodes */
};

/**
 * Class encapsulating the layered reductions for EAMAS models
 */
class LayeredReductions {
 public:
  /** Structure encapsulating the results */
  struct TransitionsOutput {
    std::vector<Transition*> t;    /** transitions t */
    std::vector<Transition*> lstc; /** transitions lstcC */
  };

  /**
   * Construct a new Layered Reductions object
   *
   * @param adtree ADTree model
   * @param automata EAMAS model
   */
  LayeredReductions(ADtree* adtree, Automata* automata) {
    this->tree = this->getSynchronizationTree(adtree, automata);
    this->tree.compute_depths();
  }

  /**
   * Translates an ADTree into a Synchronization Tree
   *
   * @param adtree ADTree model
   * @param automata EAMAS model
   *
   * @return SG
   */
  SG getSynchronizationTree(ADtree* adtree, Automata* automata) {
    SG sg = SG();

    std::stack<TreeNode*> stack;
    TreeNode* root = adtree->get_root();
    stack.push(root);

    label root_label = root->get_goal();
    sg.add_vertex(root_label,
                  Node(root_label, automata->get_automaton(root_label)));

    while (!stack.empty()) {
      TreeNode* node = stack.top();
      stack.pop();

      for (TreeNode* child : node->get_children()) {
        const std::string child_label = child->get_goal();
        const Node sg_node =
            Node(child_label, automata->get_automaton(child_label));

        sg.add_vertex(child_label, sg_node);
        sg.add_edge(node->get_goal(), child_label);

        stack.push(child);
      }
    }

    sg.set_root(root->get_goal());
    return sg;
  }

  /**
   * Compute the lstc and t transitions for a node
   *
   * @param node Node's label
   * @return TransitionsOutput
   */
  TransitionsOutput lstC(const label& node) {
    SG tree = this->get_tree();
    std::vector<label> children = this->tree.get_children(node);

    /** Get synchronization actions of MN */
    Automaton* automatonN = tree.get_vertex_data(node).automaton;
    std::vector<Action*> actionsN = automatonN->get_actions();

    /** Get children of MN */
    std::vector<Automaton*> automatonC_vector;
    std::transform(
        children.begin(), children.end(), std::back_inserter(automatonC_vector),
        [&tree](const label& n) { return tree.get_vertex_data(n).automaton; });

    /** Get synchronization actions of MNC */
    std::vector<std::vector<Action*>> actionsC_vector;
    std::transform(automatonC_vector.begin(), automatonC_vector.end(),
                   std::back_inserter(actionsC_vector),
                   [this](Automaton* a) { return a->get_actions(); });
    std::vector<Action*> actionsC = flatten(actionsC_vector);

    /** Get actions of MN that does not synchronises with any children */
    std::vector<Action*> t_actions =
        except(actionsN, actionsC, [this](Action* a, Action* b) -> bool {
          return this->get_action_label(a) == this->get_action_label(b);
        });

    /** Get transitions that does not synchronizes with any children */
    std::vector<Transition*> transitions_N = automatonN->get_transitions();
    std::vector<Transition*> t_transitions =
        included(transitions_N, t_actions, [this](Transition* t, Action* a) {
          return (this->get_action_label(a) ==
                  this->get_action_label(t->get_action()));
        });

    /** Get lst transitions */
    std::vector<Transition*> lstc_transitions;
    for (Transition* t : t_transitions) {
      State* source = t->get_source_state();

      /** Possible lst_transitions */
      std::vector<Transition*> lstc_candidates;
      std::copy_if(transitions_N.begin(), transitions_N.end(),
                   std::back_inserter(lstc_candidates),
                   [source](Transition* t) {
                     return t->get_destination_state() == source;
                   });

      /** lstc_transitions */
      std::vector<Transition*> lstc_tmp =
          included(lstc_candidates, actionsC, [this](Transition* t, Action* a) {
            return this->get_action_label(a) ==
                   this->get_action_label(t->get_action());
          });

      lstc_transitions.insert(lstc_transitions.end(), lstc_tmp.begin(),
                              lstc_tmp.end());
    }

    // remove transitions with label actions
    t_transitions.erase(std::remove_if(
        t_transitions.begin(), t_transitions.end(),
        [](Transition* t) { return t->get_action()->get_type() == "Label"; }));

    return TransitionsOutput{t_transitions, lstc_transitions};
  }

  /**
   * Apply reduction to a specific depth
   *
   * @param depth depth
   */
  void reduction_at_depth(int depth) {
    std::set<label> nodes = this->tree.depths[depth];
    int n_children_depth = this->tree.get_number_children_depth(depth);

    /** update each node at depth */
    for (const label& node : nodes) {
      TransitionsOutput transitions = this->lstC(node);

      int n_children_node = this->tree.get_number_children_node(node);
      std::string variable = "v" + std::to_string(depth);

      /** update each transition in lstc */
      for (Transition* t : transitions.lstc) {
        std::vector<Assignment*> update = t->get_updates();
        update.push_back(new Assignment(
            variable, new Addition({new Variable(variable),
                                    new Constant(n_children_node)})));
        t->set_updates(update);
      }

      /** update each transition in t */
      for (Transition* t : transitions.t) {
        std::vector<Inequality*> guard = t->get_guard();
        guard.push_back(new Inequality(new Variable(variable),
                                       new Constant(n_children_depth),
                                       InequalityOperator::Equal));
        t->set_guard(guard);
      }
    }
  }

  /**
   * Get the label of an action
   *
   * @param action
   * @return std::string
   */
  std::string get_action_label(Action* action) const {
    std::string t_action = action->get_type();

    if (t_action == "Synchronization") {
      return ((Synchronization*)action)->get_channel()->get_name();
    } else if (t_action == "Label") {
      return ((Label*)action)->get_name();
    } else {
      throw LayeredReductionsException("Action type is incorrect");
    }
  }

  /**
   * Apply the reduction to all the layers of the tree
   */
  void apply_reduction() {
    for (int depth = 0; depth < this->tree.height; depth++) {
      this->reduction_at_depth(depth);
    }
  }

  /** Return the Synchronization Tree */
  SG get_tree() const { return this->tree; }

 private:
  SG tree; /** Synchronization tree */
};

#endif  // SRC_ALGORITHMS_LAYERED_REDUCTIONS_HPP
