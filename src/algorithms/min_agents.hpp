#ifndef SRC_ALGORITHMS_MIN_AGENTS_HPP_
#define SRC_ALGORITHMS_MIN_AGENTS_HPP_

#include <algorithm>
#include <bitset>
#include <cmath>
#include <exception>
#include <iostream>
#include <iterator>
#include <map>
#include <numeric>
#include <queue>
#include <regex>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

#include "../exception.hpp"
#include "adtree/ad_tree.hpp"
#include "adtree/node_role.hpp"
#include "adtree/parser/attribute_visitor.hpp"
#include "tree.hpp"

/**
 * Error class to handle normalization errors
 */
class PreprocessingException : public Exception {
 public:
  explicit PreprocessingException(const std::string &message)
      : Exception(message) {}
};

/**
 * Node with the information for the preprocessing algorithm
 */
class NormalizeNode {
 public:
  /** Type of normalize nodes */
  enum class Type { Leaf, Seq, Null, And, Snand, Sand, Nand, Or, Nor };

  /**
   * Constructor
   *
   * @param goal node's goal
   * @param duration node's duration
   * @param role node's role
   * @param type node's type
   */
  NormalizeNode(const std::string &goal, int duration, const NodeRole &role,
                const Type &type)
      : goal(goal), duration(duration), role(role), type(type) {}

  std::string goal; /** label of the node */
  int duration;     /** duration of the node */
  NodeRole role;    /** role of the node */
  Type type;        /** type of the node */

  int level = 0; /** distance from the root */
  int depth = 0; /** height of the node’s tallest subtree of 0-duration nodes */

  /**
   * Returns if the node is a counter node
   */
  bool is_counter_node() {
    return (this->type == Type::Nand) || (this->type == Type::Nor) ||
           (this->type == Type::Snand);
  }

  /**
   * Return the string representation of the node type
   */
  std::string type_to_string(const Type &type) const {
    switch (type) {
      case Type::Leaf:
        return "LEAF";
      case Type::Seq:
        return "SEQ";
      case Type::Null:
        return "NULL";
      case Type::And:
        return "AND";
      case Type::Snand:
        return "SNAND";
      case Type::Sand:
        return "SAND";
      case Type::Nand:
        return "NAND";
      case Type::Or:
        return "OR";
      case Type::Nor:
        return "NOR";
      default:
        return "UNDEFINED";
    }
  }

  /** << operator overload */
  friend std::ostream &operator<<(std::ostream &output,
                                  const NormalizeNode &v) {
    output << "NormalizeNode(goal=" << v.goal << ", duration=" << v.duration
           << ", role=" << to_string(v.role)
           << ", type=" << v.type_to_string(v.type) << ", depth=" << v.depth
           << ", level=" << v.level << ");";
    return output;
  }
};

// identifiers of vertices are strings
typedef std::string label;

/**
 * Class representing the graph for the preprocessing
 */
class ProcessingGraph : public Tree<label, NormalizeNode> {
 public:
  /**
   * Add a vertex.
   *
   * @param id identifier of the vertex
   * @param data data of the vertex
   */
  void add_vertex(const label &id, const NormalizeNode &data) {
    Tree::add_vertex(id, data);

    if (data.type == NormalizeNode::Type::Seq) {
      this->_n++;
    }
  }

  /**
   * Remove a vertex and the edges from/to it
   *
   * @param id identifier of the vertex
   * */
  void remove_vertex(const label &id) {
    NormalizeNode::Type type_node = this->get_vertex_data(id).type;

    Tree::remove_vertex(id);

    if (type_node == NormalizeNode::Type::Seq) {
      this->_n--;
    }
  }

  /**
   * Set vertex data
   *
   * @param id identifier of the vertex to modify
   * @param data new data of the vertex
   */
  void set_vertex_data(const label &id, const NormalizeNode &data) {
    NormalizeNode::Type old_type_node = this->get_vertex_data(id).type;
    Tree::set_vertex_data(id, data);

    if (old_type_node != data.type) {
      if (old_type_node == NormalizeNode::Type::Seq) {
        this->_n--;
      } else if (data.type == NormalizeNode::Type::Seq) {
        this->_n++;
      }
    }
  }

  /**
   * Returns the number of sequence nodes in the tree
   */
  int get_n() const { return this->_n; }

  /**
   * Returns the level of a vertex
   *
   * @param vertex identifier of the vertex
   */
  int get_level(const label &vertex) const {
    return this->get_vertex_data(vertex).level;
  }

  /**
   * Modifies the level of a vertex
   *
   * @param vertex identifier of the vertex to be modified
   * @param level new level
   */
  void set_level(const label &vertex, int level) {
    NormalizeNode data = this->get_vertex_data(vertex);
    data.level = level;
    this->set_vertex_data(vertex, data);
  }

  /**
   * Returns the depth of a vertex
   *
   * @param identifier of the vertex
   */
  int get_depth(const label &vertex) const {
    return this->get_vertex_data(vertex).depth;
  }

  /**
   * Modifies the depth of a vertex
   *
   * @param vertex identifier of the vertex to be modified
   * @param depth new depth
   */
  void set_depth(const label &vertex, int depth) {
    NormalizeNode data = this->get_vertex_data(vertex);
    data.depth = depth;
    this->set_vertex_data(vertex, data);
  }

  /**
   * Return the type of a vertex
   *
   * @param vertex identifier of the vertex
   */
  NormalizeNode::Type get_type(const label &vertex) const {
    return this->get_vertex_data(vertex).type;
  }

  /**
   * Modifies the type of a node.
   * Sets duration to 1 when the new type is sequence, 0 otherwise.
   *
   * @param vertex identifier of the vertex to be modified
   * @param type new type
   *
   */
  void set_type(const label &vertex, NormalizeNode::Type type) {
    NormalizeNode tmp = this->get_vertex_data(vertex);

    int duration = (type == NormalizeNode::Type::Seq);

    NormalizeNode new_data(tmp.goal, duration, tmp.role, type);
    this->set_vertex_data(vertex, new_data);
  }

  /**
   * Returns the role of a vertex
   *
   * @param vertex identifier of the vertex
   * @return role of the vertex
   */
  NodeRole get_role(const label &vertex) const {
    return this->get_vertex_data(vertex).role;
  }

  /**
   * Returns whether a node is a counter node
   *
   * @param vertex identifier of the vertex
   */
  bool is_counter_node(const label &vertex) const {
    return this->get_vertex_data(vertex).is_counter_node();
  }

  /**
   * Returns the counter nodes of the graph
   */
  std::vector<label> get_counter_nodes() const {
    std::stack<label> stack;
    std::set<label> discovered;
    std::vector<label> counter_nodes;

    label root = this->get_root();
    discovered.insert(root);
    stack.push(root);

    while (!stack.empty()) {
      label vertex = stack.top();
      stack.pop();

      // check if the vertex is a counter node
      if (this->is_counter_node(vertex)) {
        counter_nodes.push_back(vertex);
      }

      for (const label &child : this->get_children(vertex)) {
        if (discovered.find(child) == discovered.end()) {
          discovered.insert(child);
          stack.push(child);
        }
      }
    }

    return counter_nodes;
  }

  /**
   * Computes the level of the subtree's vertices
   */
  void compute_levels() {
    std::stack<label> stack;

    if (this->is_empty()) return;

    // reset levels
    for (label v : this->get_vertices()) {
      this->set_level(v, 0);
    }

    label root = this->get_root();
    stack.push(root);

    // Depth-First Search
    while (!stack.empty()) {
      label current_node = stack.top();
      stack.pop();

      int next_level = this->get_level(current_node);
      if (this->get_type(current_node) == NormalizeNode::Type::Seq) {
        next_level += 1;
      }

      for (const auto &child : this->get_children(current_node)) {
        // process sequence node
        int child_level = this->get_level(child);
        int next_child_level = std::max(child_level, next_level);
        this->set_level(child, next_child_level);

        stack.push(child);
      }
    }
  }

  /**
   * Computes the depth of the tree's vertices
   */
  void compute_depths() {
    // reset depths
    for (label v : this->get_vertices()) {
      this->set_depth(v, 0);
    }

    return this->compute_depths(this->get_root());
  }

  /**
   * Computes the depth of the subtree's vertices recursively
   *
   * @param vertex root of the subtree to be analysed
   */
  void compute_depths(const label &vertex) {
    std::vector<label> children = this->get_children(vertex);

    for (const auto &child : children) {
      if (this->get_depth(child) == 0) {
        this->compute_depths(child);
      }
    }

    if (!children.empty()) {
      int new_depth;

      std::vector<int> children_depths;
      std::transform(
          children.begin(), children.end(), std::back_inserter(children_depths),
          [this](const label &child) { return this->get_depth(child); });

      switch (this->get_type(vertex)) {
        case NormalizeNode::Type::Seq:
          new_depth = 1 + children_depths.front();
          break;
        case NormalizeNode::Type::And:
          new_depth =
              *std::max_element(children_depths.begin(), children_depths.end());
          break;
        case NormalizeNode::Type::Or:
          new_depth =
              *std::min_element(children_depths.begin(), children_depths.end());
          break;
        default:
          new_depth = children_depths.front();
          break;
      }

      // update depth of the vertex
      this->set_depth(vertex, new_depth);
    }
  }

  /**
   * Return if the graph contains the special case of OR gate
   */
  bool is_special_case() const {
    if (this->is_empty()) return false;

    std::stack<label> stack;

    label root = this->get_root();
    int depth_root = this->get_depth(root);
    stack.push(root);

    // Depth-First Search
    while (!stack.empty()) {
      label current_node = stack.top();
      stack.pop();

      if (this->get_type(current_node) == NormalizeNode::Type::Or) {
        int depth_node = this->get_depth(current_node);
        int level_node = this->get_level(current_node);

        bool is_special = (depth_node + level_node < depth_root);
        if (is_special) return true;
      }

      for (const auto &child : this->get_children(current_node)) {
        stack.push(child);
      }
    }

    return false;
  }

 private:
  int _n = 0;
};

/**
 * Class encapsulating the preprocessing algorithm of an ADTree
 */
class Preprocessing {
 public:
  struct Output {
    ProcessingGraph preprocessed_graph;  // preprocessed graph
    std::vector<label> counter_nodes;    // active counter nodes
    std::vector<ProcessingGraph>
        flat_graphs;  // possible combinations due to the special OR gates
  };

  /**
   * Creates the preprocessing graph
   *
   * @param adtree ADtree model to be preprocessed
   * @param attribute node attribute to preprocess
   */
  explicit Preprocessing(ADtree *adtree,
                         const std::string &attribute = "time") {
    ProcessingGraph graph = this->normalize_time(adtree, attribute);
    this->enforce_scheduling(graph);

    this->_graphs = this->handle_counter_nodes(graph);

    for (auto &output : this->_graphs) {
      output.flat_graphs = this->handle_or_nodes(output.preprocessed_graph);
    }
  }

  /**
   * Returns all the possible combinations for a number of counter nodes
   *
   * @param size number of counter nodes
   */
  std::vector<std::vector<int>> get_combinations(int size) const {
    std::vector<std::vector<int>> v(1 << size, std::vector(size, 0));

    for (int i = 1; i < v.size(); i++) {
      for (int j = 0; j < size; j++) {
        v[i][j] = (i >> j) & 1;
      }
    }

    return v;
  }

  /**
   * Returns the computed gcf
   */
  int get_gcf() const { return this->_gcf; }

  /**
   * Returns the preprocessing graphs with the active counter nodes
   */
  std::vector<Output> get_graphs() const { return this->_graphs; }

 private:
  int _gcf;
  std::vector<Output> _graphs;

  /** data structure for storing sequence of nodes */
  struct Sequence {
    label top;     // first node of the sequence
    label bottom;  // last node of the sequence
    int length;    // length of the sequence
  };

  /**
   * Computes the greatest common factor of time durations across all nodes of
   * an ADTree
   *
   * @param adtree get the gcf of this ADtree
   * @param attribute get the gcf of this node's attribute
   * @return gcf of the given ADtree
   */
  int compute_gcf(ADtree *adtree, const std::string &attribute) const {
    TreeNode *root = adtree->get_root();
    AttributeVisitor visitor(attribute);
    root->accept(visitor);

    std::vector<int> values = visitor.get_values();
    int result =
        std::accumulate(values.begin(), values.end(), 0, std::gcd<int, int>);

    return result;
  }

  /**
   * Computes the number of SEQ nodes equivalent to an ADTree node
   *
   * @param node ADTree node to be normalized
   * @param attribute node's attribute to be normalized
   * @param gcf gcf of the ADTree
   *
   * @return number of sequence nodes
   */
  int compute_sequence_length(TreeNode *node, const std::string &attribute,
                              int gcf) const {
    std::vector<Attribute *> attributes = node->get_attributes();

    // find the attribute's value
    std::vector<Attribute *>::iterator it = std::find_if(
        attributes.begin(), attributes.end(),
        [&attribute](Attribute *a) { return a->get_type() == attribute; });

    // number of sequence nodes to be added
    return *it ? (*it)->get_value() / gcf : 0;
  }

  /**
   * Maps the ADTree node type to the normalized one.
   *
   * @param node ADTree node
   *
   * @return normalized node type
   */
  NormalizeNode::Type get_normalize_node_type(TreeNode *node) const {
    std::string node_type = node->get_node_type();

    if (node_type == "Leaf") {
      return NormalizeNode::Type::Leaf;
    } else if (node_type == "And") {
      return NormalizeNode::Type::And;
    } else if (node_type == "Sand") {
      return NormalizeNode::Type::Sand;
    } else if (node_type == "Nand") {
      return NormalizeNode::Type::Nand;
    } else if (node_type == "Snand") {
      return NormalizeNode::Type::Snand;
    } else if (node_type == "Or") {
      return NormalizeNode::Type::Or;
    } else if (node_type == "Nor") {
      return NormalizeNode::Type::Nor;
    } else {
      throw PreprocessingException("ADtree node " + node_type +
                                   " cannot be cast to normalize node.");
    }
  }

  /**
   * Computes the correct label for the sequence node
   *
   * @param node ADtree node
   * @param pos position of the sequence node
   *
   * @return node's label
   */
  label get_normalize_label(TreeNode *node, int pos = 0) const {
    std::string goal = node->get_goal();
    std::string extension = (pos == 0) ? "'" : "_" + std::to_string(pos);
    return goal + extension;
  }

  /**
   * Normalizes a node and adds the sequence of nodes to the preprocessing graph
   *
   * @param node ADTree node to be normalized
   * @param attribute node's attribute
   * @param gcf gcf of the ADTree
   * @param graph graph to add the normalize node
   *
   * @return sequence of normalize nodes
   */
  Sequence add_normalize_node(TreeNode *node, const std::string &attribute,
                              int gcf, ProcessingGraph &graph) {
    // get role
    NodeRole role = node->get_node_role();

    // add zero duration node
    label zero_dur_node_label = this->get_normalize_label(node);
    graph.add_vertex(zero_dur_node_label,
                     NormalizeNode(zero_dur_node_label, 0, role,
                                   this->get_normalize_node_type(node)));

    // normalize time
    int nb_sequence_nodes = this->compute_sequence_length(node, attribute, gcf);

    // create chain of sequence nodes
    label latest_node_label = zero_dur_node_label;
    for (int i = 1; i <= nb_sequence_nodes; i++) {
      label label_seq = this->get_normalize_label(node, i);
      graph.add_vertex(label_seq, NormalizeNode(label_seq, 1, role,
                                                NormalizeNode::Type::Seq));
      graph.add_edge(label_seq, latest_node_label);

      // update next child
      latest_node_label = label_seq;
    }

    return Sequence{latest_node_label, zero_dur_node_label, nb_sequence_nodes};
  }

  /**
   * Normalizes the time of the ADtree's nodes
   *
   * @param adtree adtree to be normalized
   * @param attribute attribute identifier
   */
  ProcessingGraph normalize_time(ADtree *adtree, const std::string &attribute) {
    std::stack<TreeNode *> stack;
    std::map<TreeNode *, Sequence> normalize_map;
    ProcessingGraph graph;

    TreeNode *root = adtree->get_root();

    // calculate the gcf
    this->_gcf = this->compute_gcf(adtree, attribute);

    // process root node
    Sequence sequence_nodes =
        this->add_normalize_node(root, attribute, this->get_gcf(), graph);
    normalize_map.insert(std::make_pair(root, sequence_nodes));

    // update root node
    graph.set_root(sequence_nodes.top);

    // Depth-First Search
    stack.push(root);
    while (!stack.empty()) {
      TreeNode *current_node = stack.top();
      Sequence sequence_nodes_current = normalize_map.at(current_node);
      stack.pop();

      for (TreeNode *child : current_node->get_children()) {
        Sequence sequence_nodes_child =
            this->add_normalize_node(child, attribute, this->get_gcf(), graph);
        normalize_map.insert(std::make_pair(child, sequence_nodes_child));

        // process edge
        graph.add_edge(sequence_nodes_current.bottom, sequence_nodes_child.top);

        stack.push(child);
      }
    }
    return graph;
  }

  /**
   * Returns the label corresponding to a null node resulting of a SAND node
   *
   * @param graph preprocessing graph
   * @param vertex  identifier of the SAND node
   * @param child index of the child
   *
   * @return label of the null node
   */
  label get_null_node_label(ProcessingGraph &graph, const label &vertex,
                            int child) const {
    // Check that is a SAND node
    if (graph.get_type(vertex) != NormalizeNode::Type::Sand) {
      throw PreprocessingException("Vertex " + vertex + " is not a SAND.");
    }

    return vertex + "_" + std::to_string(child + 1);
  }

  /**
   * Applies to the graph the scheduling enforcement generated by the
   * sequential ADTrees nodes.
   *
   * @param graph preprocessing graph to apply the scheduling enforcement
   */
  void enforce_scheduling(ProcessingGraph &graph) {
    std::stack<label> stack;
    stack.push(graph.get_root());

    // Depth First Search
    while (!stack.empty()) {
      label current_node_id = stack.top();
      NormalizeNode current_node = graph.get_vertex_data(current_node_id);
      stack.pop();
      bool current_is_sand = current_node.type == NormalizeNode::Type::Sand;

      std::vector<label> children = graph.get_children(current_node_id);
      int nb_children = children.size();

      for (int i = 0; i < nb_children; i++) {
        // create NULL node
        if (current_is_sand) {
          label null_label =
              this->get_null_node_label(graph, current_node_id, i);
          graph.add_vertex(null_label,
                           NormalizeNode(null_label, 0, current_node.role,
                                         NormalizeNode::Type::Null));

          // add children of NULL node
          graph.add_edge(null_label, children[i]);

          // add parents of NULL node
          if (i < nb_children - 1) {
            for (const auto &p : graph.get_leaves(children[i + 1])) {
              graph.add_edge(p, null_label);
            }
          }
        }

        stack.push(children[i]);
      }

      // remove SAND node
      if (current_is_sand) {
        label latest_null_node =
            this->get_null_node_label(graph, current_node_id, nb_children - 1);

        for (const auto &p : graph.get_parents(current_node_id)) {
          graph.add_edge(p, latest_null_node);
        }

        graph.remove_vertex(current_node_id);

        // update root node
        if (graph.get_root() == current_node_id) {
          graph.set_root(latest_null_node);
        }
      }
    }
  }

  /**
   * Returns the child of the node that has an inverse role
   *
   * @param graph preprocessing graph
   * @param vertex identifier of the vertex to analyse
   *
   * @returns counter child
   */
  label get_counter_child(ProcessingGraph &graph, const label &vertex) const {
    NodeRole role = graph.get_role(vertex);

    for (const label &child : graph.get_children(vertex)) {
      if (graph.get_role(child) != role) {
        return child;
      }
    }

    throw PreprocessingException("Vertex " + vertex +
                                 " has no a counter child.");
  }

  /**
   * Search for an vertex's ancestor of type OR and removes the subtree
   *
   * @param graph preprocessing graph
   * @param vertex identifier of the vertex to analyse
   *
   */
  void remove_subtree_from_ancestor(ProcessingGraph &graph,
                                    const label &vertex) {
    label root_tree = graph.get_root();

    label v = vertex;
    std::vector<label> parents = graph.get_parents(v);

    label root_branch = root_tree;
    while (!parents.empty()) {
      label p = parents.front();

      if (graph.get_type(p) == NormalizeNode::Type::Or) {
        root_branch = v;
        break;
      }

      v = p;
      parents = graph.get_parents(p);
    }

    if (root_branch == root_tree) {
      // reset the graph
      graph = ProcessingGraph();
    } else {
      graph.remove_subtree(root_branch);
    }
  }

  /**
   * NOTE: implement this if it's necessary
   */
  void handle_composite_defences() {}

  /**
   * A NOR node succeeds if its children with the same role succeed or its
   * counter child fails.
   *
   * @param graph preprocessing graph
   * @param vertex NOR node
   * @param operating_counter whether the counter child is successfull or not
   *
   */
  void handle_nor_node(ProcessingGraph &graph, const label &vertex,
                       bool operating_counter) {
    // Check that is a NOR node
    if (graph.get_type(vertex) != NormalizeNode::Type::Nor) {
      throw PreprocessingException("Vertex " + vertex + " is not a NOR.");
    }

    // replace vertex type by NULL
    graph.set_type(vertex, NormalizeNode::Type::Null);

    // delete counter branch
    label counter_child = this->get_counter_child(graph, vertex);
    graph.remove_subtree(counter_child);

    // delete all the children
    if (!operating_counter) {
      for (const auto &child : graph.get_children(vertex)) {
        graph.remove_subtree(child);
      }
    }
  }

  /**
   * A NAND node succeeds if its children with the same role is successfull
   * and its counter child is not.
   *
   * @param graph preprocessing graph
   * @param vertex NAND node
   * @param operating_code whether the counter child is successfull or not
   *
   */
  void handle_nand_node(ProcessingGraph &graph, const label &vertex,
                        bool operating_counter) {
    // Check that is a NAND node
    if ((graph.get_type(vertex) != NormalizeNode::Type::Nand) &&
        (graph.get_type(vertex) != NormalizeNode::Type::Snand)) {
      throw PreprocessingException("Vertex " + vertex +
                                   " is not a NAND/SNAND.");
    }

    // delete counter branch
    label counter_child = this->get_counter_child(graph, vertex);
    graph.remove_subtree(counter_child);

    if (!operating_counter) {
      // replace vertex type by NULL
      graph.set_type(vertex, NormalizeNode::Type::Null);
    } else {
      // delete all the subtrees and delete parents recursively until an OR is
      // reached
      this->remove_subtree_from_ancestor(graph, vertex);
    }
  }

  /**
   * Takes into account each possible choice of counter nodes
   *
   * @param graph preprocessing graph
   * @param counter_nodes list of counter nodes in the graph
   * @param values list with the status (operating) of each counter node
   *
   * @return active counter nodes
   */
  std::vector<label> handle_counter_nodes(ProcessingGraph &graph,
                                          std::vector<label> counter_nodes,
                                          std::vector<int> values) {
    std::map<label, int> map;
    std::transform(counter_nodes.begin(), counter_nodes.end(), values.begin(),
                   std::inserter(map, map.end()),
                   std::make_pair<label const &, int const &>);

    // filter active counter nodes
    std::vector<label> active_counter_nodes;
    for (int i = 0; i < counter_nodes.size(); i++) {
      if (values[i]) active_counter_nodes.push_back(counter_nodes[i]);
    }

    std::queue<label> queue;
    label root = graph.get_root();
    queue.push(root);

    // Breadth-First Search
    while (!queue.empty()) {
      label current_node = queue.front();
      queue.pop();

      // skip if node has been delete before
      if (!graph.in_tree(current_node)) {
        continue;
      }

      // process node
      NormalizeNode::Type node_type = graph.get_type(current_node);
      if ((node_type == NormalizeNode::Type::Nand) ||
          (node_type == NormalizeNode::Type::Snand)) {
        this->handle_nand_node(graph, current_node, map.at(current_node));
      } else if (node_type == NormalizeNode::Type::Nor) {
        this->handle_nor_node(graph, current_node, map.at(current_node));
      }

      // continue exploring branch if the node has not been deleted
      if (graph.in_tree(current_node)) {
        for (const auto &child : graph.get_children(current_node)) {
          queue.push(child);
        }
      }
    }

    return active_counter_nodes;
  }

  /**
   * Generates all the preprocessing graphs depending on the number of counter
   * nodes
   *
   * @param graph preprocessing graph
   * @return processing graphs with the active counter nodes
   */
  std::vector<Output> handle_counter_nodes(const ProcessingGraph &graph) {
    // generate all the possible combinations
    std::vector<label> counter_nodes = graph.get_counter_nodes();
    std::vector<std::vector<int>> combinations =
        this->get_combinations(counter_nodes.size());

    // generate all non empty graphs
    std::vector<Output> graphs;
    for (int i = 0; i < combinations.size(); i++) {
      ProcessingGraph new_graph(graph);

      std::vector<label> active_counter_nodes =
          this->handle_counter_nodes(new_graph, counter_nodes, combinations[i]);

      if (!new_graph.is_empty()) {
        new_graph.compute_depths();
        new_graph.compute_levels();

        graphs.push_back(Output{new_graph, active_counter_nodes, {}});
      }
    }

    return graphs;
  }

  /**
   * Prune branches that cannot lead to an optimal assignment.
   */
  void prune_spurious_branches(ProcessingGraph &graph) {
    if (graph.is_empty()) return;

    std::stack<label> stack;
    label root = graph.get_root();
    stack.push(root);

    // Depth-First Search
    while (!stack.empty()) {
      label current_node = stack.top();
      stack.pop();

      // remove branches that cannot lead to an optimal assignment
      if (graph.get_type(current_node) == NormalizeNode::Type::Or) {
        std::vector<label> children = graph.get_children(current_node);
        std::sort(children.begin(), children.end(),
                  [graph](const label &lhs, const label &rhs) {
                    return graph.get_depth(lhs) < graph.get_depth(rhs);
                  });

        for (auto it = children.begin() + 1; it != children.end(); ++it) {
          graph.remove_subtree(*it);
        }
      }

      for (const auto &child : graph.get_children(current_node)) {
        stack.push(child);
      }
    }
  }

  /**
   * Return all the possible combinations of the graph based on the OR gates.
   *
   * @param graph graph to be processed
   * @return graph with OR gates containing only one child
   */
  std::vector<ProcessingGraph> remove_or_nodes(const ProcessingGraph &graph) {
    std::vector<ProcessingGraph> graphs;
    if (graph.is_empty()) return graphs;

    // initial graph
    std::stack<label> current_stack;
    ProcessingGraph current_graph;

    label root = graph.get_root();
    current_stack.push(root);
    current_graph.add_vertex(root, graph.get_vertex_data(root));
    current_graph.set_root(root);

    std::stack<std::pair<ProcessingGraph, std::stack<label>>> tmp_graphs;
    tmp_graphs.push(std::make_pair(current_graph, current_stack));

    while (!tmp_graphs.empty()) {
      std::pair<ProcessingGraph, std::stack<label>> value = tmp_graphs.top();
      current_graph = value.first;
      current_stack = value.second;
      tmp_graphs.pop();

      bool or_found = false;
      while (!(current_stack.empty() || or_found)) {
        label node = current_stack.top();
        current_stack.pop();

        or_found = (graph.get_type(node) == NormalizeNode::Type::Or);

        // handle children
        for (const label &child : graph.get_children(node)) {
          if (!or_found) {
            current_stack.push(child);
            current_graph.add_vertex(child, graph.get_vertex_data(child));
            current_graph.add_edge(node, child);
          } else {
            ProcessingGraph new_graph = current_graph;
            std::stack<label> new_stack = current_stack;

            new_stack.push(child);
            new_graph.add_vertex(child, graph.get_vertex_data(child));
            new_graph.add_edge(node, child);

            tmp_graphs.push(std::make_pair(new_graph, new_stack));
          }
        }
      }

      // the graph has been fully processed
      if (current_stack.empty() && !or_found) {
        current_graph.compute_depths();
        current_graph.compute_levels();
        graphs.push_back(current_graph);
      }
    }

    return graphs;
  }

  /**
   * Handle the OR gates in a preprocessed graph
   *
   * @param g graph to be processed
   * @return std::vector<ProcessingGraph>
   */
  std::vector<ProcessingGraph> handle_or_nodes(const ProcessingGraph &g) {
    std::vector<ProcessingGraph> graphs;

    // get or gates
    bool or_gates = g.is_special_case();

    if (or_gates) {
      graphs = this->remove_or_nodes(g);
    } else {
      ProcessingGraph graph = g;

      // Prune the spurious branches
      this->prune_spurious_branches(graph);

      // Compute depth of nodes
      graph.compute_depths();

      // Compute level of nodes
      graph.compute_levels();
      graphs.push_back(graph);
    }

    return graphs;
  }
};

/**
 * Class encapsulating the minimal agent assignment algorithm
 */
class MinimalAgentAssignment {
 public:
  /** information of assignment */
  struct AssignmentInfo {
    int agent;
    int slot;
  };

  /** alias for an assignment */
  typedef std::map<label, AssignmentInfo> assignment_t;

  /** alias for the scheduling */
  typedef std::vector<std::vector<std::set<label>>> scheduling;

  /** alias for the mapping from level to vertices */
  typedef std::map<int, std::set<label>> levels;

  /**
   * Applies the minimal agent assignment algorithm
   *
   * @param graph graph after pre-processing
   */
  explicit MinimalAgentAssignment(const ProcessingGraph &graph) {
    this->_graph = graph;
    this->schedule_minimal_time_and_agents();
  }

  /**
   * Returns the underlying graph
   */
  ProcessingGraph get_graph() const { return this->_graph; }

  /**
   * Returns the assignment of agents for each node
   */
  assignment_t get_assignment() const { return this->_assignment; }

  /**
   * Returns the scheduling table
   */
  scheduling get_scheduling() const {
    scheduling table(this->slots, std::vector<std::set<label>>(this->agents));

    for (const assignment_t::value_type &a : this->_assignment) {
      table[a.second.slot - 1][a.second.agent - 1].insert(a.first);
    }

    return table;
  }

  /**
   * Returns the number of agents
   */
  int get_agents() const { return this->agents; }

  /**
   * Returns the number of slots
   */
  int get_slots() const { return this->slots; }

  /**
   * Comparator
   *
   * @param a First minimal assignment
   * @param b Second minimal assignment
   * @return true if the first assignment is less than the second one
   */
  bool operator<(const MinimalAgentAssignment &assignment) const {
    return (this->slots != assignment.slots) ? this->slots < assignment.slots
                                             : this->agents < assignment.agents;
  }

 private:
  ProcessingGraph _graph;
  assignment_t _assignment;
  int agents;
  int slots;

  /** information of scheduling */
  struct Scheduling {
    assignment_t assignment;
    int n_remain;
  };

  /**
   * Return the sequential nodes at each level
   *
   * @return levels
   */
  levels get_seq_levels(const ProcessingGraph &graph) const {
    std::stack<label> stack;
    levels level_map;

    if (graph.is_empty()) return level_map;

    label root = graph.get_root();
    stack.push(root);

    // Depth-First Search
    while (!stack.empty()) {
      label current_node = stack.top();
      stack.pop();

      if (graph.get_type(current_node) == NormalizeNode::Type::Seq) {
        level_map[graph.get_level(current_node)].insert(current_node);
      }

      for (const auto &child : graph.get_children(current_node)) {
        stack.push(child);
      }
    }

    return level_map;
  }

  /**
   * Return the maximum width of the levels
   *
   * @param l levels
   * @return maximum width
   */
  int max_width_of_levels(const levels &levels_map) const {
    std::vector<int> widths;

    std::transform(levels_map.begin(), levels_map.end(),
                   std::back_inserter(widths),
                   [](const std::pair<int, std::set<label>> &l) {
                     return l.second.size();
                   });

    int max_width = *std::max_element(widths.begin(), widths.end());
    return max_width;
  }

  /**
   * Get the deepest nodes in a set
   * @param S set
   * @return vector containing the deepest nodes
   */
  std::vector<label> deepest_nodes(const std::set<label> &S) const {
    std::vector<label> nodes;

    label deepest_node = *std::max_element(
        S.begin(), S.end(), [this](const label &v1, const label &v2) {
          return this->_graph.get_depth(v1) < this->_graph.get_depth(v2);
        });

    int deep = this->_graph.get_depth(deepest_node);

    std::copy_if(
        S.begin(), S.end(), std::back_inserter(nodes),
        [deep, this](label n) { return this->_graph.get_depth(n) == deep; });
    return nodes;
  }

  /**
   * Get the nodes in a specific slot in the scheduling
   *
   * @param assigment_map  assignment mapping
   * @param slot slot
   * @return  vector with the nodes in the slot
   */
  std::vector<label> nodes_in_slot(const assignment_t &assigment_map,
                                   int slot) const {
    std::vector<label> nodes;

    for (const auto &a : assigment_map) {
      if (a.second.slot == slot) {
        nodes.push_back(a.first);
      }
    }
    return nodes;
  }

  /**
   * Computes a scheduling which is not necessarily the most optimal.
   *
   * @param level_map mapping containing the nodes in each level of the graph
   * @param graph_n number of sequential nodes in the graph
   * @param num_slots maximum number of slots in the scheduling
   * @param num_agents number of agents to test
   *
   * @return Scheduling
   */
  Scheduling compute_scheduling(const levels &level_map, int graph_n,
                                int num_slots, int num_agents) {
    int slot = num_slots;
    int nodes_remain = graph_n;

    int l = 0;
    std::set<label> S;
    assignment_t current_assignment;

    while (nodes_remain > 0 && slot > 0) {
      int agent = 1;

      // sequential nodes at level l
      std::set<label> nodes_at_level = level_map.at(l);
      S.insert(nodes_at_level.begin(), nodes_at_level.end());

      while ((agent <= num_agents) && !S.empty()) {
        // forall N' in S, N.depth >= N'.depth
        std::vector<label> candidates = this->deepest_nodes(S);

        // forall N' in S st. N'.slot=slot
        std::vector<label> nodes_slots =
            this->nodes_in_slot(current_assignment, slot);

        // N' not in ancestors(N)
        std::vector<label> filtered_candidates;
        std::copy_if(
            candidates.begin(), candidates.end(),
            std::back_inserter(filtered_candidates),
            [this, nodes_slots](const label &n) {
              std::vector<label> ancestors = this->_graph.get_ancestors(n);
              return std::find_if(
                         nodes_slots.begin(), nodes_slots.end(),
                         [&ancestors](const label &n_) {
                           // n' not in ancestors(n)
                           return (std::find(ancestors.begin(), ancestors.end(),
                                             n_) != ancestors.end());
                         }) == nodes_slots.end();
            });

        if (filtered_candidates.empty()) {
          break;
        }

        // current node
        label current_node = filtered_candidates.front();

        // assignment
        current_assignment[current_node] = AssignmentInfo{agent, slot};

        agent++;
        nodes_remain--;
        S.erase(current_node);
      }

      // reshuffle
      this->reshuffle_slot(current_assignment, slot, agent - 1);

      l++;
      slot--;
    }

    // returns the computed assignment and the number of nodes remaining to
    // schedule
    return Scheduling{current_assignment, nodes_remain};
  }

  /**
   * Computes a schedule with the minimal time and also minimising the number
   * of agents
   */
  void schedule_minimal_time_and_agents() {
    label root = this->_graph.get_root();

    // Skip DAG with null duration
    int graph_n = this->_graph.get_n();
    if (graph_n == 0) return;

    // sequential nodes at each level
    levels level_map = this->get_seq_levels(this->_graph);

    // number of slots
    int num_slots = this->_graph.get_depth(root);
    this->slots = num_slots;

    // lower bound on the number of agents
    int lower_bound = std::ceil(static_cast<double>(graph_n) /
                                static_cast<double>(num_slots)) -
                      1;

    // Max width of levels is the initial upper bound
    int initial_upper_bound = this->max_width_of_levels(level_map);
    int upper_bound = initial_upper_bound;

    while ((upper_bound - lower_bound) > 1) {
      // number of agents
      int num_agents =
          lower_bound +
          std::floor(static_cast<double>(upper_bound - lower_bound) / 2.0);

      // computes a possible optimal scheduling
      Scheduling scheduling =
          this->compute_scheduling(level_map, graph_n, num_slots, num_agents);

      // schedule OK
      if (scheduling.n_remain == 0) {
        upper_bound = num_agents;
        this->agents = num_agents;
        this->_assignment = scheduling.assignment;
      } else {  // schedule not OK
        lower_bound = num_agents;
      }
    }

    // computes the scheduling with the maximum number of agents possible
    if (initial_upper_bound == upper_bound) {
      this->agents = initial_upper_bound;
      Scheduling scheduling =
          this->compute_scheduling(level_map, graph_n, num_slots, this->agents);
      this->_assignment = scheduling.assignment;
    }

    // assign zero nodes
    this->assign_zero_nodes();
  }

  /**
   * Returns whether a vertex has an agent assigned
   *
   * @param vertex identifier of the vertex
   * @return True if an agent has been assigned, false otherwise
   */
  bool has_assigned_agent(const label &vertex) const {
    auto it = this->_assignment.find(vertex);
    return (it != this->_assignment.end()) && ((*it).second.agent != 0);
  }

  /**
   * Checks whether the children of a vertex have been already assigned.
   *
   * @param vertex identifier of the vertex to be analysed
   *
   * @return True if all the vertex's children are assigned, false otherwise
   */
  bool all_children_assigned(const label &vertex) const {
    for (const label &child : this->_graph.get_children(vertex)) {
      if (this->_graph.get_depth(child) != 0 &&
          !this->has_assigned_agent(child)) {
        return false;
      }
    }

    return true;
  }

  /**
   * Remove the elements of the set t from the set s (s\t)
   */
  void set_difference(std::set<label> &s, const std::set<label> &t) {
    for (const label &l : t) {
      s.erase(l);
    }
  }

  /**
   * Assigns zero-duration nodes to the same agent and the same time slot as
   * their parents if the parents are a sequence of nodes
   */
  void assign_zero_nodes() {
    std::set<label> S;
    std::set<label> to_remove;
    std::vector<label> vertices = this->_graph.get_vertices();

    // Add to set S the nodes that are not assigned to an agent yet
    std::copy_if(
        vertices.begin(), vertices.end(), std::inserter(S, S.end()),
        [this](const auto &v) { return !this->has_assigned_agent(v); });

    to_remove.clear();
    for (const label &v : S) {
      std::vector<label> parents = this->_graph.get_parents(v);
      if (parents.size() == 1 &&
          this->_graph.get_type(parents.front()) == NormalizeNode::Type::Seq) {
        this->_assignment[v] = this->_assignment[parents.front()];
        to_remove.insert(v);
      }
    }
    this->set_difference(S, to_remove);

    while (!S.empty()) {
      // process NULL, OR and LEAF nodes
      to_remove.clear();
      for (const label &v : S) {
        if ((this->_graph.get_type(v) == NormalizeNode::Type::Null) ||
            (this->_graph.get_type(v) == NormalizeNode::Type::Or) ||
            (this->_graph.get_type(v) == NormalizeNode::Type::Leaf)) {
          // Get assignment as its only child
          std::vector<label> children = this->_graph.get_children(v);
          if (children.size() == 1 &&
              this->has_assigned_agent(children.front())) {
            this->_assignment[v] = this->_assignment[children.front()];
            to_remove.insert(v);
          }

          // Get assignment as its parent if v has no child
          if (children.empty() ||
              (children.size() == 1 &&
               this->_graph.get_depth(children.front()) == 0)) {
            std::vector<label> parents = this->_graph.get_parents(v);
            label parent_node =
                *std::min_element(parents.begin(), parents.end(),
                                  [this](const label &v1, const label &v2) {
                                    return this->_assignment.at(v1).slot <
                                           this->_assignment.at(v2).slot;
                                  });
            if (this->has_assigned_agent(parent_node)) {
              this->_assignment[v] = this->_assignment[parent_node];
              to_remove.insert(v);
            }
          }
        }
      }
      this->set_difference(S, to_remove);

      // process AND nodes
      to_remove.clear();
      for (const label &v : S) {
        if (this->_graph.get_type(v) == NormalizeNode::Type::And) {
          // Get assignment as its parent
          std::vector<label> parents = this->_graph.get_parents(v);
          if (this->_graph.get_depth(v) == 0 && parents.size() == 1 &&
              this->has_assigned_agent(parents.front())) {
            this->_assignment[v] = this->_assignment[parents.front()];
            to_remove.insert(v);
          }

          // Get assignment as its child that occurs last
          if (this->_graph.get_depth(v) != 0 &&
              this->all_children_assigned(v)) {
            std::vector<label> children = this->_graph.get_children(v);

            label child_node =
                *std::max_element(children.begin(), children.end(),
                                  [this](const label &v1, const label &v2) {
                                    return this->_assignment.at(v1).slot <
                                           this->_assignment.at(v2).slot;
                                  });

            this->_assignment[v] = this->_assignment[child_node];
            to_remove.insert(v);
          }
        }
      }
      this->set_difference(S, to_remove);
    }
  }

  /**
   * Ensures that the same agent is responsible for all sequence nodes in
   * sequences obtained during the time normalisation step
   *
   * @param slot slot to be reshuffled
   * @param nb_agents number of agents assigned
   */
  void reshuffle_slot(assignment_t &current_assignment, int slot,
                      int nb_agents) {
    for (int agent = 1; agent <= nb_agents; agent++) {
      auto it =
          std::find_if(current_assignment.begin(), current_assignment.end(),
                       [slot, agent](const auto &kv_pair) {
                         return (kv_pair.second.agent == agent) &&
                                (kv_pair.second.slot == slot);
                       });

      label current_node = (*it).first;

      // agent assigned to parent node
      std::vector<label> parents = this->_graph.get_parents(current_node);

      if (!parents.empty()) {
        int parent_agent = current_assignment[parents.front()].agent;

        if ((parent_agent != agent) && (parent_agent != 0)) {
          auto it_parent =
              std::find_if(current_assignment.begin(), current_assignment.end(),
                           [parent_agent, slot](const auto &kv_pair) {
                             return (kv_pair.second.agent == parent_agent) &&
                                    (kv_pair.second.slot == slot);
                           });

          if (it_parent != current_assignment.end()) {
            label found_node = (*it_parent).first;
            if (current_node != found_node) {
              current_assignment[found_node] = AssignmentInfo{agent, slot};
            }
          }

          current_assignment[current_node] = AssignmentInfo{parent_agent, slot};
        }
      }
    }
  }
};

/**
 * Class encapsulating the parser to tikz
 */
class TikzPrinter {
 public:
  /**
   * Parses a processing tree into tikz
   *
   * @param output redirection the output
   * @param tree graph to be parsed
   * @param labels whether display the vertex's depth and level
   * @param collapsed whether display all the nodes of branches or collapsed
   */
  void parser(std::ostream &output, const ProcessingGraph &tree,
              bool labels = true, bool collapsed = true) {
    std::string tikz_body = this->generate_tikz(tree, labels, collapsed);
    output << this->wrap_to_document(this->wrap_to_figure(tikz_body));
  }

 private:
  /**
   * Returns the colors of a vertex
   *
   * @param vertex vertex to inspect
   * @return string of the color
   */
  std::string get_color(const NormalizeNode &vertex) const {
    return vertex.role == NodeRole::Attack ? "red" : "green";
  }

  /**
   * Returns if a vertex is an AND or OR node
   *
   * @param vertex vertex to inspect
   * @return True if the vertex is a AND or OR node, False otherwise
   */
  bool is_gate(const NormalizeNode &vertex) const {
    return (vertex.type == NormalizeNode::Type::And) ||
           (vertex.type == NormalizeNode::Type::Or);
  }

  /**
   * Returns the tikz shape of a vertex
   *
   * @param vertex vertex to be inspected
   * @param nb_children number of children of the vertex
   * @return string to display a node
   */
  std::string get_shape(const NormalizeNode &vertex, int nb_children) const {
    std::string children = (nb_children > 1) ? ", logic gate inputs=" +
                                                   std::string(nb_children, 'n')
                                             : "";
    switch (vertex.type) {
      case NormalizeNode::Type::Seq:
        return "SEQ";
      case NormalizeNode::Type::Null:
        return "NULL";
      case NormalizeNode::Type::And:
        return "AND" + children;
      case NormalizeNode::Type::Or:
        return "OR" + children;
      default:
        return "state";
    }
  }

  /**
   * Wrap the tikz figure to a latex document
   *
   * @param figure body of the tikz figure
   * @return string with the tikz figure inside a document tag
   */
  std::string wrap_to_document(std::string figure) const {
    std::string header =
        "\\documentclass{standalone}\n"
        "\\usepackage[dvipsnames,svgnames,table]{xcolor}\n"
        "\\usepackage{tikz}\n"
        "\\usetikzlibrary{automata, positioning, arrows, shapes, "
        "shapes.gates.logic, shapes.gates.logic.US}\n"
        "\\begin{document}\n";

    std::string footer = "\\end{document}\n";

    return header + figure + footer;
  }

  /**
   * Wrap a tikz code to a tikzpicture environment
   *
   * @param body tikz code
   * @return string with thee the tikz code inside a figure tag
   */
  std::string wrap_to_figure(std::string body) const {
    std::string begin = "\\begin{tikzpicture}[node distance=1.8cm]\n";

    std::string style =
        "  \\tikzstyle{SEQ}=[diamond]\n"
        "  \\tikzstyle{NULL}=[trapezium, trapezium left angle=120, trapezium "
        "right angle=120, minimum size=8mm]\n"
        "  \\tikzstyle{AND}=[and gate US, rotate=90 ]\n"
        "  \\tikzstyle{OR}=[or gate US, rotate=90 ]\n"
        "  \\tikzset{every node/.style={ultra thick, draw=red, minimum "
        "size=6mm}}\n";

    std::string end = "\\end{tikzpicture}\n";

    return begin + style + body + end;
  }

  /**
   * Generates the tikz representation of a vertex
   *
   * @param vertex vertex to be parsed
   * @param parent parent of the vertex
   * @param nb_children number of children of the vertex
   * @param labels whether display the vertex's depth and level
   * @param xshift horizontal offset of the vertex's position
   * @param style extra style to be added
   *
   * @return tikz representation of the vertex
   */
  std::string generate_tikz_vertex(const NormalizeNode &vertex,
                                   const NormalizeNode &parent, int nb_children,
                                   bool labels, double xshift = 0,
                                   const std::string &style = "") const {
    std::regex re("_(.*)");
    std::string goal = vertex.goal;
    std::string parent_goal = parent.goal;
    std::string position = "";
    std::string label = "{\\ensuremath{\\mathtt{" +
                        std::regex_replace(goal, re, "_{$1}") + "}}}";

    bool is_root = goal == parent_goal;

    if (this->is_gate(vertex)) label = "{\\rotatebox {-90}" + label + "}";

    if (!is_root) {
      if (this->is_gate(vertex)) {
        position += ", yshift=4mm, below = 1.4cm of " + parent_goal + ".south";
      } else {
        position += ", below of=" + parent_goal;
      }
    }

    std::string extra_style = (style != "") ? ", " + style : "";

    std::string tikz = "  \\node[draw=" + this->get_color(vertex) + ", " +
                       this->get_shape(vertex, nb_children) + extra_style +
                       ", xshift=" + std::to_string(xshift) + "cm " + position +
                       "] (" + goal + ") " + label + ";\n";

    if (labels) {
      tikz += this->generate_tikz_labels(vertex, is_root);
    }

    return tikz;
  }

  /**
   * Generates the tikz of an edge
   *
   * @param tree graph where the edge belongs to
   * @param from source vertex of the edge
   * @param to target vertex of the edge
   * @param collapsed whether the edge represents a collapsed sequence
   * @param position position of the edge in the source vertex
   *
   * @return tikz representation of the edge
   */
  std::string generate_tikz_edge(const ProcessingGraph &tree,
                                 const NormalizeNode &from,
                                 const NormalizeNode &to, bool collapsed,
                                 const int position = 1) const {
    std::string from_position = from.goal;
    std::string to_position = to.goal;

    std::string edge_type = collapsed ? "dotted, red, thick" : "solid";

    // source vertex is a gate node
    if (this->is_gate(from)) {
      // special case when an OR is processed
      if (tree.get_children(from.goal).size() == 1) {
        from_position += ".west";
      } else {
        from_position += ".input " + std::to_string(position);
      }
    }

    // target vertex is a gate node
    if (this->is_gate(to)) {
      to_position += ".east";
    }

    std::string tikz = "  \\draw[" + edge_type + "] (" + from_position +
                       ") edge (" + to_position + ");\n";
    return tikz;
  }

  /**
   * Returns the length of a chain of sequence nodes
   *
   * @param tree processing graph
   * @param vertex vertex at the beginning of the chain
   * @param length accumulated length
   *
   * @return length of the chain
   */
  int get_length_sequence_nodes(const ProcessingGraph &tree,
                                const label &vertex, int length = 0) const {
    std::vector<label> children = tree.get_children(vertex);

    if (tree.get_type(vertex) != NormalizeNode::Type::Seq ||
        children.size() != 1) {
      return length;
    }

    return this->get_length_sequence_nodes(tree, children.front(), length + 1);
  }

  /**
   * Returns the vertex after skipping a number of sequence nodes in the chain
   *
   * @param tree processing graph
   * @param vertex vertex at the beginning of the chain
   * @param steps number of nodes to skip
   * @param position accumulated steps
   */
  label skip_sequence_nodes(const ProcessingGraph &tree, const label &vertex,
                            int steps, int position = 0) const {
    std::vector<label> children = tree.get_children(vertex);

    if (tree.get_type(vertex) != NormalizeNode::Type::Seq ||
        children.size() != 1 || position == steps) {
      return vertex;
    }

    return this->skip_sequence_nodes(tree, children.front(), steps,
                                     position + 1);
  }

  /**
   * Parses a information of a vertex
   *
   * @param vertex vertex to be inspected
   * @param label information to be displayed
   * @param color  text color
   * @param xshift horizontal shift
   * @param yshift vertical shift
   * @param anchor text anchor
   *
   * @return tikz label of the vertex
   */
  std::string generate_tikz_label(const NormalizeNode &vertex,
                                  std::string label,
                                  std::string color = "black",
                                  std::string xshift = "0mm",
                                  std::string yshift = "0cm",
                                  std::string anchor = "center") const {
    return "  \\node[draw=none, " + color + ", xshift=" + xshift +
           ", yshift=" + yshift + "] at (" + vertex.goal + "." + anchor +
           ") {\\small{\\ensuremath{\\mathtt{" + label + "}}}};\n";
  }

  /**
   * Generates all the informations of a vertex
   *
   * @param vertex vertex to be inspected
   * @param is_root whether the vertex is the root or not
   *
   * @return tikz representation of all the labels
   */
  std::string generate_tikz_labels(const NormalizeNode &vertex,
                                   bool is_root) const {
    std::string tikz = "";
    std::string level_color = "blue";
    std::string depth_color = "green!60!black";

    std::string left_xshift = "-2mm";
    std::string left_anchor = "west";

    std::string right_xshift = "2mm";
    std::string right_anchor = "east";

    std::string yshift = "0mm";
    std::string title_yshift = "7mm";

    if (this->is_gate(vertex)) {
      left_anchor = "north";
      right_anchor = "south";
    }

    if (is_root) {
      tikz +=
          this->generate_tikz_label(vertex, "level", level_color, right_xshift,
                                    title_yshift, right_anchor);

      tikz += this->generate_tikz_label(vertex, "depth", depth_color,
                                        left_xshift, title_yshift, left_anchor);
    }

    tikz += this->generate_tikz_label(vertex, std::to_string(vertex.level),
                                      level_color, right_xshift, yshift,
                                      right_anchor);

    tikz += this->generate_tikz_label(vertex, std::to_string(vertex.depth),
                                      depth_color, left_xshift, yshift,
                                      left_anchor);

    return tikz;
  }

  /**
   * Generates the tikz of a processing tree
   *
   * @param tree graph to be parsed
   * @param labels whether display the vertex's information
   * @param collapsed whether display all the tree or collapsed branches
   *
   * @return tikz representation of the tree
   */
  std::string generate_tikz(const ProcessingGraph &tree, bool labels,
                            bool collapsed) const {
    if (tree.is_empty()) return "";

    std::stack<label> stack;
    std::set<label> visited;

    std::string tikz = "";
    label root = tree.get_root();

    double siblings_separation = 2.5;

    NormalizeNode root_info = tree.get_vertex_data(root);

    int nb_children = tree.get_children(root).size();
    tikz +=
        this->generate_tikz_vertex(root_info, root_info, nb_children, labels);
    stack.push(root);

    while (!stack.empty()) {
      label vertex = stack.top();
      NormalizeNode vertex_info = tree.get_vertex_data(vertex);
      std::vector<label> children = tree.get_children(vertex);
      stack.pop();

      // mark vertex as visited
      visited.insert(vertex);

      // collapse sequence of nodes
      bool skipped = false;
      if (collapsed && tree.get_type(vertex) == NormalizeNode::Type::Seq) {
        int length = this->get_length_sequence_nodes(tree, vertex);

        if (length > 5) {
          children = {this->skip_sequence_nodes(tree, vertex, length - 3)};
          skipped = true;
        }
      }

      double xshift = -siblings_separation * ((children.size() - 1) / 2.0);
      int child_number = 1;
      for (const label &child : children) {
        NormalizeNode child_info = tree.get_vertex_data(child);

        if (visited.find(child) == visited.end()) {
          nb_children = tree.get_children(child).size();
          tikz += this->generate_tikz_vertex(child_info, vertex_info,
                                             nb_children, labels, xshift);
          stack.push(child);
        }

        // create edge
        tikz += this->generate_tikz_edge(tree, vertex_info, child_info, skipped,
                                         child_number);
        xshift += siblings_separation;
        child_number++;
      }
    }

    return tikz;
  }
};

/**
 * Class encapsulating the visualization of the scheduling table
 */
class TablePrinter {
 public:
  /**
   * Returns the scheduling table as ASCII
   *
   * @param minimal_assignment assignment to be printed
   */
  std::string ascii(const MinimalAgentAssignment minimal_assignment) {
    int agents = minimal_assignment.get_agents();
    int slots = minimal_assignment.get_slots();
    MinimalAgentAssignment::scheduling scheduling =
        minimal_assignment.get_scheduling();

    // store table with the string representation of each assignment
    std::vector<std::vector<std::string>> scheduling_compact(
        slots, std::vector<std::string>(agents));
    // store the max width of each column in the table
    std::vector<int> longest_label(agents, 0);

    for (int s = 0; s < slots; s++) {
      for (int a = 0; a < agents; a++) {
        std::string label = this->to_string(scheduling[s][a]);
        scheduling_compact[s][a] = label;
        longest_label[a] =
            std::max(longest_label[a], static_cast<int>(label.size()));
      }
    }

    int slots_spaces = std::max(this->get_spaces(slots), 5);
    int agents_spaces = std::max(
        std::accumulate(longest_label.begin(), longest_label.end(), agents - 1),
        6);

    std::vector<int> line_spaces(longest_label);
    line_spaces.insert(line_spaces.begin(), slots_spaces);

    // first row
    std::string text = this->line({slots_spaces, agents_spaces}) + "\n";
    text += "|" + this->repeat(slots_spaces, ' ') + "|" +
            this->generate_center_text(agents_spaces, "agents") + "|\n";

    // second row
    text += "|" + this->generate_center_text(slots_spaces, "slots") + "|";
    for (int i = 0; i < agents; i++) {
      text +=
          this->generate_center_text(longest_label[i], std::to_string(i + 1)) +
          "|";
    }
    text += "\n" + this->line(line_spaces) + "\n";

    // body rows
    for (int slot = 0; slot < slots; slot++) {
      // slot cell
      text +=
          "|" +
          this->generate_center_text(slots_spaces, std::to_string(slot + 1)) +
          "|";

      // agent cells
      for (int agent = 0; agent < agents; agent++) {
        text += this->generate_center_text(longest_label[agent],
                                           scheduling_compact[slot][agent]) +
                "|";
      }

      text += "\n";
    }

    text += this->line(line_spaces);

    return text + "\n";
  }

  /**
   * Return the latex representation of the scheduling table
   *
   * @param minimal_assignment assignment to be printed
   */
  std::string latex(const MinimalAgentAssignment minimal_assignment) {
    int agents = minimal_assignment.get_agents();
    int slots = minimal_assignment.get_slots();
    MinimalAgentAssignment::scheduling scheduling =
        minimal_assignment.get_scheduling();

    std::regex re("_(.*)");
    std::vector<std::vector<std::string>> scheduling_compact(
        slots, std::vector<std::string>(agents));
    for (int s = 0; s < slots; s++) {
      for (int a = 0; a < agents; a++) {
        std::string label =
            "{\\ensuremath{\\mathtt{" +
            std::regex_replace(this->to_string(scheduling[s][a]), re, "_{$1}") +
            "}}}";
        scheduling_compact[s][a] = label;
      }
    }

    std::string options = "\\rowcolors{2}{lightgray!30}{white}\n";
    std::string begin =
        "\\begin{longtable}{c|" + this->repeat(agents, "l|") + "}\n";
    std::string end = "\n\\end{longtable}\n";

    std::string header = "\\diagbox[]{slot}{agent}";
    for (int a = 1; a <= agents; a++) header += "&" + std::to_string(a);
    header += "\\\\\n\\hline\n\\endhead\n\\hline\n\\endfoot\n";

    std::string body;
    for (int slot = 0; slot < slots; slot++) {
      body += std::to_string(slot + 1);
      for (int agent = 0; agent < agents; agent++) {
        body += "&" + scheduling_compact[slot][agent];
      }
      body += "\\\\\n";
    }

    std::string text = options + begin + header + body + end;
    return this->wrap_to_document(text) + "\n";
  }

  /**
   * Wraps a table in a document latex tag
   *
   * @param table latex representation of the table
   * @return table inside a document tag
   */
  std::string wrap_to_document(std::string table) const {
    std::string header =
        "\\documentclass{article}\n"
        "\\usepackage{longtable}\n"
        "\\usepackage[dvipsnames,svgnames,table]{xcolor}\n"
        "\\usepackage{diagbox}\n"
        "\\begin{document}\n";

    std::string footer = "\\end{document}\n";
    return header + table + footer;
  }

  /**
   * Returns the string representation of a horizontal line of the table
   *
   * @param column_sizes sizes of the columns of the table
   * @return string of the line
   */
  std::string line(std::vector<int> column_sizes) const {
    std::string s;
    for (const auto &column_size : column_sizes) {
      if (s.size() == 0) s += "+";
      s += this->repeat(column_size, '-') + "+";
    }
    return s;
  }

  /**
   * Returns the string representation of a set of nodes
   *
   * @param nodes nodes to be represented as a string
   * @return string representation of the nodes
   */
  std::string to_string(std::set<label> nodes) const {
    std::string s;
    for (const auto &label : nodes) {
      if (s.size() != 0) s += ",";
      s += label;
    }
    return s;
  }

  /**
   * Returns the number of spaces occupied by a number
   *
   * @param value value to be inspected
   * @return number of spaces
   */
  int get_spaces(int value) const {
    std::string string_value = std::to_string(value);
    return string_value.size();
  }

  /**
   * Generate a string with the character c repeated n times
   *
   * @param n number of times
   * @param c character to be repeated
   * @return a string
   */
  std::string repeat(int n, char c) const { return std::string(n, c); }

  /**
   * Generate a string with the string s repeated n times
   *
   * @param n number of times
   * @param s string to be repeated
   * @return a string
   */
  std::string repeat(int n, std::string s) const {
    std::string text;
    for (int i = 0; i < n; i++) text += s;
    return text;
  }

  /**
   * Returns a string with an equal number of spaces at the begin and at
   * the end wrapping the text
   *
   * @param n space to be filled by the text
   * @param text text to be centered
   * @return a string containing the text centered
   */
  std::string generate_center_text(int n, std::string text) const {
    int text_spaces = text.size();
    if (text_spaces >= n) return text;

    double remain_spaces = (n - text_spaces) / 2.0;

    return this->repeat(std::ceil(remain_spaces), ' ') + text +
           this->repeat(std::floor(remain_spaces), ' ');
  }
};

#endif  // SRC_ALGORITHMS_MIN_AGENTS_HPP_
