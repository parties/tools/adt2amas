#ifndef AUTOMATA_HPP
#define AUTOMATA_HPP

#include <set>
#include <string>
#include <vector>

#include "../exception.hpp"
#include "automaton.hpp"
#include "channel.hpp"
#include "parser/amas_visitor.hpp"

class Automaton;
class AMASVisitor;

/**
 * Error class to handle Automata errors
 */
class AutomataException : public Exception {
 public:
  explicit AutomataException(const std::string &message) : Exception(message) {}
};

/**
 * \ingroup eamas
 *
 * Automata entity, a bunch of automaton and channels to synchronize them.
 * Basically, it is a class to model and control a system which has several
 * agents (Automaton) and channels. The former communicate and synchronize
 * through the latter.
 */
class Automata {
 private:
  //! A private attribute of set type to collect integers.
  /*!
      It is used to save Automaton ids to take advantage of set definition since
     ids must be unique and can not exist twice same Automaton instances in
     Automata.
  */
  std::set<int> automaton_ids_;

  //! A private attribute of set type to collect integers.
  /*!
      It is used to save Channel ids to take advantage of set definition since
     ids must be unique and can not exist twice same Channel instances in
     Automata.
  */
  std::set<int> channel_ids_;

  //! A private attribute of vector type to collect Automaton pointers.
  /*!
      It is used to save Automaton pointers which are in the Automata. The
     pointers here must be correlated with ids saved in automaton_ids_.
  */
  std::vector<Automaton *> vector_automaton_;

  //! A private attribute of vector type to collect Channel pointers.
  /*!
      It is used to save Channel pointers which are in the Automata. The
     pointers here must be correlated with ids saved in channel_ids_.
  */
  std::vector<Channel *> channels_;

 public:
  //! Automata constructor.
  /*!
An instance of Automata is created empty and following, it is possible to add
Automaton and Channel instances.
*/
  Automata();

  //! Action destructor.
  ~Automata();

  /* Gets */

  //! A public method to get vector pointers of Automaton instances.
  /*!
      Member part of getters.
      \return A vector of type Automaton pointers.
  */
  std::vector<Automaton *> get_vector_automaton();

  //! A public method to get ids of Automaton instances associated with this
  //! Automata instance.
  /*!
      Member part of getters.
      \return A set of type int which are Automaton ids associated with this
     Automata instance.
  */
  std::set<int> get_automaton_ids();

  //! A public method to get vector pointers of Channel instances.
  /*!
      Member part of getters.
      \return A vector of type Channel pointers.
  */
  std::vector<Channel *> get_channels();

  //! A public method to get a information type of string.
  /*!
      Member part of getters.
      \return A string which contain information about an Automata instance,
     their Automaton and Channel instances.
  */
  std::string get_info();

  //! A public method to return an automaton by its name.
  /*!
      Member part of getters.
      \return A pointer of the automaton that was found.
  */
  Automaton *get_automaton(const std::string name);

  //! A public method implementing the visitor pattern
  /*!
      @param visitor is a visitor instance
   */
  void accept(AMASVisitor &visitor);

  /* Sets */

  //! A public method to add an Automaton instance to an Automata instance.
  /*!
      @param[in] new_automaton
      is an Automaton instance to be added. Strictly, it can not exist in the
     Automata.
  */
  void add_automaton(Automaton *new_automaton);

  //! A public method to add a Channel instance to an Automata instance.
  /*!
      @param[in] new_channel
      is a Channel instance to be added.
  */
  void add_channel(Channel *new_channel);

  // Removing...

  //! A public method to remove a Automaton instance to an Automata instance.
  /*!
      @param[in] cur_automaton is the current Automaton instance to be removed.
     Strictly, it must exist in the Automata.
  */
  void remove_automaton(Automaton *cur_automaton);

  //! A public method to remove a Channel instance to an Automata instance.
  /*!
      @param[in] cur_channel is the current Channel instance to be removed.
     Strictly, it must exist in the Automata.
  */
  void remove_channel(Channel *cur_channel);
};

#endif
