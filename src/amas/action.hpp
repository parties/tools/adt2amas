#ifndef ACTION_HPP
#define ACTION_HPP

#include <string>
#include <vector>

//! \ingroup eamas_action
//! Action entity, associated to a transition to represent either a
//! Synchronization or a Label implementations.
/*! Action is an abstract class since it has at least one pure virtual
   function. Additionally, by definition, it refers to the event after a guard
   is satisfied and before an update takes place. It is strongly associated with
   a transition and makes sense because is an interface for the Label and
   Synchronization classes.
 */
class Action {
 public:
  //! A pure virtual member.
  virtual std::string get_info() = 0;

  //! A pure virtual member.
  virtual std::vector<std::string> get_info_vector() = 0;

  //! A pure virtual member
  virtual std::string get_type() = 0;
};

#endif
