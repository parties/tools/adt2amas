#ifndef AUTOMATON_HPP
#define AUTOMATON_HPP

#include <set>
#include <string>
#include <vector>

#include "../exception.hpp"
#include "action.hpp"
#include "assignment.hpp"
#include "parser/amas_visitor.hpp"
#include "state.hpp"
#include "transition.hpp"

class AMASVisitor;
class State;
class Transition;

/**
 * Error class to handle Automaton errors
 */
class AutomatonException : public Exception {
 public:
  explicit AutomatonException(const std::string &message)
      : Exception(message) {}
};

//! \ingroup eamas
//! Automaton entity, compose by states and transitions to connect them.
/*!
    A class to model an agent which is an abstract machine with states and
   transitions among them. Each agent (Automaton) has an unique id number.
  */
class Automaton {
 private:
  //! A private attribute of integer type to save id number of an Automaton
  //! instance.
  /*!
      this variable is responsible for save a counter value that is incremented
     in current_id. It happens each time an Automaton instance is created.
  */
  int id;

  //! A private attribute of string type to save the name of th automaton
  std::string name_;

  //! A private attribute of string type to save the type of node that
  //! represents the automaton
  std::string type_;

  //! A private static attribute of integer type to increment and to have a
  //! record about id numbers.
  /*!
      this variable increments anytime an Automaton instance is created because
     of static property, it does not start each time from 0.
  */
  static int current_id;

  //! A private attribute of a vector of assignment type to store the variable
  //! declarations.
  std::vector<Assignment *> variables_;

  //! A private attribute of State pointer type to point a state which shall be
  //! of initial type.
  /*!
      An Automaton instance must always have a state associated in this
     variable. It also have to be in states_.
  */
  State *initial_state_;

  //! A private attribute of set type to collect integers.
  /*!
      It is used to save State ids to take advantage of set definition since ids
     must be unique and can not exist twice same State instances in Automaton.
  */
  std::set<int> state_ids_;

  //! A private attribute of set type to collect integers.
  /*!
      It is used to save Transition ids to take advantage of set definition
     since ids must be unique and can not exist twice same Transition instances
     in Automaton.
  */
  std::set<int> transition_ids_;

  //! A private attribute of vector type to collect State pointers.
  /*!
      It is used to save State pointers which are in the Automaton. The pointers
     here must be correlated with ids saved in state_ids_.
  */
  std::vector<State *> states_;

  //! A private attribute of vector type to collect Transition pointers.
  /*!
      It is used to save Transition pointers which are in the Automaton. The
     pointers here must be correlated with ids saved in transition_ids_.
  */
  std::vector<Transition *> transitions_;

  //! A private method to remove a Transition instance to an Automaton instance
  //! because removing a state.
  /*!
      This method delete all transitions in other State instances which point to
     cur_state before deleting it. It is private because is called to check just
     internally by remove_state calling.

      \param[in] cur_state is the current State instance to be removed.
     Strictly, it must exist in the Automaton.
  */
  void remove_transition_due_removing_state(State *cur_state);

  //! A private method to check if initial_state_ has more transitions.
  /*!
      This method has an assert which stop execution when try to delete the last
     Transition instance associated with initial_state_. It is private because
     is used to check just internally in remove_transition and
     remove_transition_due_removing_state.

      \param[in] cur_transition is the current Transition instance to be
     removed. Strictly, it must exist in the Automaton.
  */
  void more_transitions_initial_state(Transition *cur_transition);

 public:
  //! Automaton constructor.
  /*!
      An instance of Automaton is created.
      @param initial_state mandatory pointer to State for their initial_state_
     of this Automaton instance.
      @param name automaton's name
      @param type node type representing the automaton
  */
  Automaton(State *initial_state, std::string name = "", std::string type = "");

  //! Automaton destructor.
  /*!
      It deletes their pointer to initial_state_.
  */
  ~Automaton();

  /* Gets */
  //! A public method to get the id number of an Automaton instance.
  /*!
      Member part of getters.
      \return A integer type corresponding to an id number.
  */
  int get_id();

  //! A public method to get the name of an Automaton instance.
  /*!
        Member part of getters.
        \return A string type corresponding to the name.
    */
  std::string get_name();

  //! A public method to get the type that represents the Automaton instance.
  /*!
        Member part of getters.
        \return A string type corresponding to the type.
    */
  std::string get_type();

  //! A public method to get the vector of variables of an Automaton instance.
  /*!
        Member part of getters.
        \return A vector of Assignment type corresponding to the variables.
  */
  std::vector<Assignment *> get_variables();

  //! A public method to get the id number of an Automaton instance.
  /*!
      Member part of getters.
      \return A integer type corresponding to an id number.
  */
  State *get_initial_state();

  //! A public method to get ids of State instances associated with this
  //! Automaton instance.
  /*!
      Member part of getters.
      \return A set of type int which are State ids associated with this
     Automaton instance.
  */
  std::set<int> get_state_ids();

  //! A public method to get ids of Transition instances associated with this
  //! Automaton instance.
  /*!
      Member part of getters.
      \return A set of type int which are Transition ids associated with this
     Automaton instance.
  */
  std::set<int> get_transition_ids();

  //! A public method to get vector pointers of State instances.
  /*!
      Member part of getters.
      \return A vector of type State pointers.
  */
  std::vector<State *> get_states();

  //! A public method to get vector pointers of Action instances.
  /*!
      Member part of getters.
      \return A vector of type Action pointers.
  */

  std::vector<Action *> get_actions();

  //! A public method to get vector pointers of Transition instances.
  /*!
      Member part of getters.
      \return A vector of type Transition pointers.
  */
  std::vector<Transition *> get_transitions();

  //! A public method to get a information type of string.
  /*!
      Member part of getters.
      \return A string which contain information about an Automaton instance,
     their State and Transition instances. Indentation applied for each
     Transition instance associated with a particular State.
  */
  std::string get_info();

  //! A public method implementing the visitor pattern
  /*!
      @param visitor is a visitor instance
   */
  void accept(AMASVisitor &visitor);

  /* Sets */

  //! A public method to set the initial State of an Automaton instance.
  /*!
      Member is part of setters.
      \param[in] initial_state
      a State pointer to change current initial state.
  */
  void set_initial_state(State *initial_state);

  /* Other methods */
  // Adding...

  //! A public method to add a Transition instance to an Automaton instance.
  /*!
      \param[in] source_state
      is a State instance to be assigned as the State from which a Transition
     goes to.

     \param[in] destination_state is a State instance to be assigned as
     the State to which a Transition points to.

     \param[in] action is an Action
     instance to be assigned as the Action event associated with the Transition
     to be added.

     \param updates is a List of Update belonging to the transition.

     \param guard is the transition's guard

      Strictly, source_state and destination_state must be already in the
     Automaton. This method makes a call to Transition constructor hence has
     same entries.
  */
  void add_transition(State *source_state, State *destination_state,
                      Action *action, std::vector<Assignment *> updates = {},
                      std::vector<Inequality *> guard = {});

  //! A public method to add an State instance to an Automaton instance.
  /*!
      \param[in] new_state
      is a State instance to be added. Strictly, it can not exist in the
     Automata.
  */
  void add_state(State *new_state);

  //! A public method to add a Assignment instance to an Automaton instance.
  /*!
    \param[in] new_variable is a Assignment instance to be added.
  */
  void add_variable(Assignment *new_variable);

  // Removing...
  /**
   * A public method to remove a Transition instance to an Automaton instance
   * (overloaded).
   *
   * @param cur_transition is the current Transition instance to be removed.
   * Strictly, it must exist in the Automaton.
   */
  void remove_transition(Transition *cur_transition);

  /**
   * A public method to remove a Transition instance to an Automaton instance
   * (overloaded).
   *
   * @param transition_id is the current Transition instance id to be removed.
   * Strictly, it must exist in the Automaton.
   */
  void remove_transition(int transition_id);

  /**
   * A public method to remove a State instance to an Automaton instance.
   *
   * @param cur_state is the current State instance to be removed. Strictly, it
   * must exist in the Automaton and can not be equal to initial_state_.
   */
  void remove_state(State *cur_state);

  /**
   * A public method to remove their initial_state_ instance to an Automaton
   * instance.
   *
   * It is mandatory, to have deleted completely Transitions.
   */
  void remove_initial_state();
};

#endif
