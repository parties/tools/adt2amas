#include "synchronization.hpp"

using namespace std;

Synchronization::Synchronization(SynchronizationType synchronization_type,
                                 Channel *channel)
    : synchronization_type_(synchronization_type), channel_(channel) {}

Synchronization::~Synchronization() {}

/* Gets */

SynchronizationType Synchronization::get_synchronization_type() {
  return this->synchronization_type_;
}

Channel *Synchronization::get_channel() { return this->channel_; }

string Synchronization::get_type() { return "Synchronization"; }

string Synchronization::get_info() {
  string info_sync =
      to_string(this->synchronization_type_) + this->channel_->get_name();
  return info_sync;
}

void Synchronization::accept(AMASVisitor &visitor) { visitor.visit(this); }

vector<string> Synchronization::get_info_vector() {
  vector<string> info_vector;
  info_vector.push_back(to_string(this->synchronization_type_));
  info_vector.push_back(this->channel_->get_name());
  return info_vector;
}
/* Sets */

void Synchronization::set_synchronization_type(
    SynchronizationType synchronization_type) {
  this->synchronization_type_ = synchronization_type;
}

void Synchronization::set_channel(Channel *channel) {
  this->channel_ = channel;
}
