#include "state.hpp"
#include <string>
#include <vector>

using namespace std;

State::State(StateType state_type) : state_type_(state_type) {
  ++current_id;
  id = current_id;
}

State::~State() {}

/* Gets */

int State::get_id() { return this->id; }

StateType State::get_state_type() { return this->state_type_; }

vector<Transition*> State::get_transitions() { return this->transitions_; }

string State::get_info(int tabs) {
  string info_state = "State " + to_string(this->id) + " (" +
                      to_string(this->state_type_) + "):\n";
  int j = 1;
  string space_tabs = string(tabs, '\t');
  for (auto& i : this->transitions_) {
    info_state += space_tabs + i->get_info();
    if (j == this->transitions_.size())
      info_state += "";
    else
      info_state += "\n";
    j++;
  }
  return info_state;
}

bool State::is_initial() {
  return this->get_state_type() == StateType::Initial;
}

void State::accept(AMASVisitor& visitor) { visitor.visit(this); }

/* Sets */

void State::set_state_type(StateType state_type) {
  this->state_type_ = state_type;
}

void State::set_transitions(vector<Transition*> transitions) {
  this->transitions_ = transitions;
}

int State::current_id = -1;
