#ifndef SYNCHRONIZATION_TYPE_HPP
#define SYNCHRONIZATION_TYPE_HPP

#include <string>

/**
 * @ingroup eamas_action
 *
 * SynchronizationType enum class.
 *
 * Eventually, automaton does synchronization which must be a match: Send or
 * Receive.
 */
enum class SynchronizationType { Send, Receive };

/**
 * Returns the string representation of the synchronization type.
 *
 * @param sync_type synchronization type.
 *
 * @return a string with the synchronization type.
 */
std::string to_string(SynchronizationType sync_type);

#endif
