#ifndef AMAS_TIKZ_VISITOR_HPP
#define AMAS_TIKZ_VISITOR_HPP

#include <string>

#include "amas_visitor.hpp"

/**
 * @ingroup eamas_visitor
 *
 * Visitor Tikz parser for the AMAS module
 */
class AMASTikzVisitor : public AMASVisitor {
 public:
  AMASTikzVisitor();

  void visit(State* state);
  void visit(Transition* transition);
  void visit(Automaton* automaton);
  void visit(Automata* automata);
  void visit(Synchronization* synchronization);
  void visit(Label* label);

  /**
   * Gets the Tikz representation of an object
   *
   * @return the string of tikz
   */
  std::string get_tikz();

 private:
  /** text buffer */
  std::string text_ = "";

  /** latest visited state */
  int latest_state_;

  /** initial state of the automaton */
  int initial_state_;

  /** visited automaton */
  std::string automaton_;

  /** state position on the tikz figure */
  int state_postion_ = 0;

  /** transition bending on the tikz figure */
  int transition_bend_ = 0;
};

#endif  // TIKZ_VISITOR_HPP
