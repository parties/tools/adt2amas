#include "amas_tikz_visitor.hpp"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <regex>
#include <set>
#include <vector>

#include "parser/linear_expression_tikz_visitor.hpp"

using namespace std;

AMASTikzVisitor::AMASTikzVisitor() {}

/**
 * Returns the label of the state
 *
 * @param state state to be analyzed
 * @param automaton automaton of the state
 * @param initial_state initial state of the automaton
 *
 * @return state's label
 */
string get_state_label(State* state, string automaton, int initial_state) {
  string state_label = to_string(state->get_id() - initial_state);
  vector<Transition*> transitions = state->get_transitions();

  if (transitions.size() == 1) {
    Transition* transition = transitions.front();
    int source_state = transition->get_source_state()->get_id();
    int target_source = transition->get_destination_state()->get_id();

    if (source_state == target_source) {
      string transition_label = transition->get_action()->get_info();
      return (transition_label.find("nok") != string::npos)
                 ? "\\neg " + automaton
                 : automaton;
    }
  }

  return state_label;
}

void AMASTikzVisitor::visit(State* state) {
  string type = state->is_initial()
                    ? ", initial"
                    : ", right of =q" + to_string(latest_state_);

  string id = "(q" + to_string(state->get_id()) + ")";
  string label =
      "{$l_{" + get_state_label(state, this->automaton_, this->initial_state_) +
      "}$}";
  string pos = (state_postion_ != 0)
                   ? ", below=" + to_string(state_postion_) + "cm"
                   : "";

  text_ += "\\node[state" + type + pos + "" + "] " + id + " " + label + ";\n";
  latest_state_ = state->get_id();
}

void AMASTikzVisitor::visit(Transition* transition) {
  regex re("(_)");

  string source =
      "(q" + to_string(transition->get_source_state()->get_id()) + ")";
  string target =
      "(q" + to_string(transition->get_destination_state()->get_id()) + ")";

  string type = transition->get_action()->get_type();
  string color_label = (type == "Synchronization") ? "colorSync" : "black";

  string label =
      "\\textcolor{" + color_label + "}{$" +
      regex_replace(transition->get_action()->get_info(), re, "\\$1") + "$}";

  bool is_loop =
      transition->get_destination_state() == transition->get_source_state();

  string loop = is_loop ? "loop right, "
                        : ", bend right=" + to_string(transition_bend_) + ",";

  string edge_options = "above, align=center, ";
  string label_options = is_loop ? "rotate=90, anchor=west" : "";

  string guard_str = "";
  for (auto& g : transition->get_guard()) {
    LinearExpressionTikzVisitor guard_visitor;
    g->accept(guard_visitor);

    guard_str += "\\footnotesize{\\textcolor{colorCondition}{$\\mathtt{" +
                 guard_visitor.get_tikz() + "}$}}\\\\";
  }

  string updates = "";
  for (auto& u : transition->get_updates()) {
    LinearExpressionTikzVisitor visitor;
    u->get_linear_expression()->accept(visitor);

    string variable_tikz = regex_replace(u->get_variable(), re, "\\$1");
    string linear_expression_tikz = visitor.get_tikz();

    updates += "\\footnotesize{\\textcolor{colorAttribute}{$\\mathtt{" +
               variable_tikz + " : = " + linear_expression_tikz + "}$}}\\\\";
  }

  string color_transition =
      (type == "Synchronization") ? "colorTransition" : "black";

  text_ += "\\draw [->, " + color_transition + "] " + source + " edge [" +
           loop + edge_options + "] node [" + label_options + "]{" + guard_str +
           updates + label + "} " + target + ";\n";
}

void AMASTikzVisitor::visit(Synchronization* sync) {}

void AMASTikzVisitor::visit(Label* label) {}

void AMASTikzVisitor::visit(Automaton* automaton) {
  text_ +=
      "\\begin{figure}\n\\centering\n"
      "\\scalebox{.75}{\n"
      "\\begin{tikzpicture}[>=stealth', thick, node distance=3cm, every "
      "node/.style={sloped}]\n";

  this->automaton_ = automaton->get_name();
  this->initial_state_ = automaton->get_initial_state()->get_id();
  auto initial_state_transitions =
      automaton->get_initial_state()->get_transitions();

  vector<int> states_from_initial_s;
  transform(initial_state_transitions.begin(), initial_state_transitions.end(),
            back_inserter(states_from_initial_s),
            [](Transition* t) { return t->get_destination_state()->get_id(); });

  // tikz for states
  auto states = automaton->get_states();
  for (auto it = states.begin(); it != states.end(); ++it) {
    state_postion_ = 0;
    int id = (*it)->get_id();

    // check if there is a transition from the initial state
    if (find(states_from_initial_s.begin(), states_from_initial_s.end(),
             (*it)->get_id()) != states_from_initial_s.end()) {
      latest_state_ = this->initial_state_;

      if (id != states_from_initial_s[0]) {
        state_postion_ = 3;
      }

    } else if (it != states.begin()) {
      latest_state_ = (*prev(it))->get_id();
    }

    (*it)->accept(*this);
  }

  // tikz for transitions
  for (auto& s : states) {
    auto transitions = s->get_transitions();

    transition_bend_ = 0;
    set<int> draw_transitions{};
    for (auto it = transitions.begin(); it != transitions.end(); ++it) {
      int target_id = (*it)->get_destination_state()->get_id();

      if (draw_transitions.find(target_id) != draw_transitions.end()) {
        transition_bend_ += 55;
      }

      (*it)->accept(*this);
      draw_transitions.insert(target_id);
    }
  }

  text_ += "\\end{tikzpicture}\n}\n\\caption{EAMAS model for the ADTree node " +
           this->automaton_ + "} \\label{fig:" + this->automaton_ +
           "}\n\\end{figure}\n\n";
}

void AMASTikzVisitor::visit(Automata* automata) {}

string AMASTikzVisitor::get_tikz() { return text_; }
