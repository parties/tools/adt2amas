#ifndef AMAS_VISITOR_HPP
#define AMAS_VISITOR_HPP

#include "action.hpp"
#include "automata.hpp"
#include "automaton.hpp"
#include "label.hpp"
#include "state.hpp"
#include "synchronization.hpp"
#include "transition.hpp"

class Synchronization;
class Label;
class State;
class Transition;
class Automaton;
class Automata;

/**
 * @ingroup eamas_visitor
 * Visitor interface for the AMAS module
 */
class AMASVisitor {
 public:
  /**
   * Visits a state
   *
   * @param[in] state state
   */
  virtual void visit(State* state) = 0;

  /**
   * Visits a transition
   *
   * @param[in] transition transition
   */
  virtual void visit(Transition* transition) = 0;

  /**
   * Visits a transition's synchronization
   *
   * @param[in] sync transition's synchronization
   */
  virtual void visit(Synchronization* sync) = 0;

  /**
   * visits a transition's label
   *
   * @param[in] label transition's label
   */
  virtual void visit(Label* label) = 0;

  /**
   * Visits an automaton
   *
   * @param[in] automaton automaton
   */
  virtual void visit(Automaton* automaton) = 0;

  /**
   * Visits an automata
   *
   * @param[in] automata automata
   */
  virtual void visit(Automata* automata) = 0;
};

#endif  // AMAS_VISITOR_HPP
