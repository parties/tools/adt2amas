#ifndef AMAS_IMITATOR_VISITOR_HPP
#define AMAS_IMITATOR_VISITOR_HPP

#include <string>
#include "amas_visitor.hpp"

/**
 * @ingroup eamas_visitor
 *
 * Visitor Tikz parser for the AMAS module
 */
class AMASIMITATORVisitor : public AMASVisitor {
 public:
  AMASIMITATORVisitor();

  void visit(State* state);
  void visit(Transition* transition);
  void visit(Automaton* automaton);
  void visit(Automata* automata);
  void visit(Synchronization* synchronization);
  void visit(Label* label);

  /**
   * Gets the imitator model
   *
   * @return the string of imitator model
   */
  std::string get_imitator();

 private:
  /** text buffer */
  std::string text_ = "";

  /** current automaton */
  Automaton* current_automaton;
};

#endif  // AMAS_IMITATOR_VISITOR_HPP
