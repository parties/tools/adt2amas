#include "amas_imitator_visitor.hpp"

#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <typeinfo>
#include <vector>

#include "constant.hpp"
#include "parser/linear_expression_imitator_visitor.hpp"

using namespace std;

string separator = "(************************)\n";

/**
 * Checks if the end of a string is equals to  a suffix
 *
 * @param str string to be analysed
 * @param suffix suffix to be searched
 *
 * @return a boolean
 */
bool ends_with(const string& str, const string& suffix) {
  return str.size() >= suffix.size() &&
         0 == str.compare(str.size() - suffix.size(), suffix.size(), suffix);
}

AMASIMITATORVisitor::AMASIMITATORVisitor(){};

void AMASIMITATORVisitor::visit(State* state) {
  string state_id = "l" + to_string(state->get_id());
  text_ += "loc " + state_id + ": while True wait {}\n";

  State* extra_state = nullptr;

  for (auto& t : state->get_transitions()) {
    // Avoid loops
    if (t->get_source_state()->get_id() !=
        t->get_destination_state()->get_id()) {
      t->accept(*this);
    } else {
      Synchronization* sync = static_cast<Synchronization*>(t->get_action());

      // unfold nok loop only if incoming transition has not the same action
      if (ends_with(sync->get_channel()->get_name(), "nok")) {
        State* source = t->get_source_state();
        std::vector<Transition*> incoming_transitions;

        std::vector<Transition*> all_transitions =
            this->current_automaton->get_transitions();
        std::copy_if(
            all_transitions.begin(), all_transitions.end(),
            std::back_inserter(incoming_transitions),
            [source, t](Transition* candidate_t) {
              return (candidate_t->get_destination_state() == source) &&
                     (candidate_t != t);
            });

        if ((incoming_transitions.size() > 1) ||
            (incoming_transitions.size() == 1 &&
             incoming_transitions.front()->get_action() != t->get_action())) {
          extra_state = new State;
          Transition* t_clone =
              new Transition(state, extra_state, t->get_action(),
                             t->get_updates(), t->get_guard());
          t_clone->accept(*this);
        }
      }
    }
  }

  // parsing extra state if it exists
  if (extra_state) extra_state->accept(*this);
};

void AMASIMITATORVisitor::visit(Transition* transition) {
  string sync = "";
  Action* action = transition->get_action();
  vector<Assignment*> updates = transition->get_updates();
  vector<Inequality*> guard = transition->get_guard();

  /** Optimization of transitions */
  if (action->get_type() == "Label") {
    State* next_state = transition->get_destination_state();
    vector<Transition*> next_transitions = next_state->get_transitions();
    if (next_transitions.size() == 1 &&
        next_transitions.front()->get_destination_state() == next_state) {
      action = next_transitions.front()->get_action();

      // union of guards
      vector<Inequality*> next_guard = next_transitions.front()->get_guard();
      guard.insert(guard.end(), next_guard.begin(), next_guard.end());
    }
  }

  if (action->get_type() == "Synchronization") {
    sync += " sync " +
            static_cast<Synchronization*>(action)->get_channel()->get_name();
  }

  string target =
      "l" + to_string(transition->get_destination_state()->get_id());

  string updates_str = "";
  for (auto& u : updates) {
    LinearExpressionImitatorVisitor visitor;
    u->get_linear_expression()->accept(visitor);

    updates_str += u->get_variable() + "' = " + visitor.get_imitator();
    if (u != updates.back()) {
      updates_str += ", ";
    }
  }

  string condition_str = guard.empty() ? "True" : "";

  for (auto& g : guard) {
    LinearExpressionImitatorVisitor guard_visitor;
    g->accept(guard_visitor);

    condition_str += guard_visitor.get_imitator();
    if (g != guard.back()) {
      condition_str += " & ";
    }
  }

    text_ += "  when " + condition_str + sync + " do {" + updates_str +
             "} goto " + target + ";\n";
  };

void AMASIMITATORVisitor::visit(Automaton* automaton) {
  this->current_automaton = automaton;
  string automaton_name = automaton->get_name();

  vector<Transition*> transitions = automaton->get_transitions();
  vector<State*> states = automaton->get_states();
  set<string> synclabels{};

  // Collecting synclabels
  for (auto& t : transitions) {
    if (t->get_action()->get_type() == "Synchronization") {
      synclabels.insert(static_cast<Synchronization*>(t->get_action())
                            ->get_channel()
                            ->get_name());
    }
  }

  text_ += separator + "automaton " + automaton_name + "\n" + separator;

  text_ += "synclabs: ";
  for (auto& l : synclabels) {
    if (l != *synclabels.begin()) {
      text_ += ", ";
    }
    text_ += l;
  }
  text_ += ";\n";

  for (auto& s : states) {
    s->accept(*this);
  }

  text_ += "end\n\n";
};

void AMASIMITATORVisitor::visit(Automata* automata) {
  string automaton_vector_str = text_;
  vector<Automaton*> automaton_vector = automata->get_vector_automaton();

  set<string> variables{};
  vector<pair<string, string>> initial_states{};
  vector<pair<string, int>> constants{};

  // Visit each automaton
  for (auto& a : automaton_vector) {
    // Collecting initial states
    string automaton_name = a->get_name();
    string automaton_initial_state =
        "l" + to_string(a->get_initial_state()->get_id());
    initial_states.push_back(
        make_pair(automaton_name, automaton_initial_state));

    LinearExpression* linear_expr = nullptr;
    string variable = "";

    // Collecting initial values
    for (auto& a : a->get_variables()) {
      linear_expr = a->get_linear_expression();
      variable = a->get_variable();

      // only constants
      if (linear_expr->get_type() == "Constant") {
        constants.push_back(make_pair(
            variable, static_cast<Constant*>(linear_expr)->get_value()));
      }
    }

    // Collecting variables
    for (auto& t : a->get_transitions()) {
      for (auto& u : t->get_updates()) {
        variable = u->get_variable();
        variables.insert(variable);
      }
    }
  }

  // Add variables
  text_ = "var\n\n";
  for (auto& c : constants) {
    if (c != constants.front()) {
      text_ += ", ";
    }
    text_ += c.first + " = " + to_string(c.second);
  }
  text_ += ": constant;\n\n";

  for (auto& v : variables) {
    if (v != *variables.begin()) {
      text_ += ", ";
    }
    text_ += v;
  }
  text_ += ": discrete;\n\n";

  // Add automaton vector
  text_ += automaton_vector_str;

  // Add initial state
  text_ += "\n" + separator + "(* initial states       *)\n" + separator +
           "init := \n  ";
  for (auto& pair_s : initial_states) {
    if (pair_s != initial_states.front()) {
      text_ += "\n& ";
    }
    text_ += "loc[" + pair_s.first + "] = " + pair_s.second;
  }

  // Add initial values for variables
  for (auto& v : variables) {
    text_ += "\n& " + v + " = 0";
  }
  text_ += "\n;";
};

void AMASIMITATORVisitor::visit(Synchronization* synchronization){};

void AMASIMITATORVisitor::visit(Label* label){};

string AMASIMITATORVisitor::get_imitator() { return text_; };
