#include "transition.hpp"
#include "assignment.hpp"

using namespace std;

Transition::Transition(State *source_state, State *destination_state,
                       Action *action, vector<Assignment *> updates,
                       vector<Inequality *> guard)
    : source_state_(source_state),
      destination_state_(destination_state),
      action_(action),
      updates_(updates),
      guard_(guard) {
  ++current_id;
  id = current_id;
}

Transition::~Transition() {}

/* Gets */

int Transition::get_id() { return this->id; }

State *Transition::get_source_state() { return this->source_state_; }

State *Transition::get_destination_state() { return this->destination_state_; }

Action *Transition::get_action() { return this->action_; }

vector<Assignment *> Transition::get_updates() { return this->updates_; }

std::vector<Inequality *> Transition::get_guard() { return this->guard_; }

string Transition::get_info() {
  /** parse updates */
  string info_updates = "{";
  for (auto it = std::begin(this->updates_); it != std::end(this->updates_);
       ++it) {
    if (it != this->updates_.begin()) {
      info_updates += ", ";
    }
    info_updates += (*it)->get_info();
  }
  info_updates += "}";

  /** parse guard */
  string info_guard = "{";
  for (auto it = std::begin(this->guard_); it != std::end(this->guard_); ++it) {
    if (it != this->guard_.begin()) {
      info_guard += " ^ ";
    }
    info_guard += (*it)->get_info();
  }
  info_guard += "}";

  /** parse actions */
  string info_actions = this->action_->get_info();

  string info_tran = "Transition " + to_string(this->get_id()) + " (" +
                     info_guard + ", " + info_actions + ", " + info_updates +
                     ")";
  return info_tran;
}

void Transition::accept(AMASVisitor &visitor) { visitor.visit(this); }

/* Sets */

void Transition::set_source_state(State *source_state) {
  this->source_state_ = source_state;
}

void Transition::set_destination_state(State *destination_state) {
  this->destination_state_ = destination_state;
}

void Transition::set_action(Action *action) { this->action_ = action; }

void Transition::set_guard(std::vector<Inequality *> guard) {
  this->guard_ = guard;
}

void Transition::set_updates(vector<Assignment *> updates) {
  this->updates_ = updates;
}

int Transition::current_id = 0;

/* Other methods */
