#include "channel.hpp"

using namespace std;

Channel::Channel(string name) : name_(name) {
  ++current_id;
  id = current_id;
}

Channel::~Channel() {}

/* Gets */

int Channel::get_id() { return this->id; }

string Channel::get_name() { return this->name_; }

string Channel::get_info() {
  string info_ch = "Channel " + to_string(this->id) + ": " + this->name_;
  return info_ch;
}
/* Sets */

void Channel::set_name(string name) { this->name_ = name; }

int Channel::current_id = 0;
