#include "synchronization_type.hpp"

using namespace std;

string to_string(SynchronizationType sync_type) {
  string sh_ = "";
  switch (sync_type) {
    case SynchronizationType::Send:
      sh_ = "!";
      break;
    case SynchronizationType::Receive:
      sh_ = "?";
      break;
    default:
      break;
  }
  return sh_;
}
