#ifndef TRANSITION_HPP
#define TRANSITION_HPP

#include "assignment.hpp"
#include "inequality.hpp"
#include "label.hpp"
#include "parser/amas_visitor.hpp"
#include "state.hpp"
#include "synchronization.hpp"

class AMASVisitor;
class State;
class Action;
class Assignment;

/**
 * @ingroup eamas
 *
 * Transition entity.
 *
 * It links states in an automaton and serves in the synchronization process of
 * automaton. It has an unique id number.
 */
class Transition {
 private:
  /**
   * A private attribute of integer type to save id number of a Transition
   * instance. This variable is responsible for save a counter value that is
   * incremented in current_id. It happens each time a Transition instance is
   * created.
   */
  int id;

  /**
   * A private static attribute of integer type to increment and to have a
   * record about id numbers. This variable increments anytime a Transition
   * instance is created because of static property, it does not start each time
   * from 0.
   */
  static int current_id;

  /**
   * A private attribute of State pointer type to save source state from which a
   * Transition instance goes out.
   */
  State *source_state_;

  /**
   * A private attribute of State pointer type to save destination state to
   * which a Transition instance come in.
   */
  State *destination_state_;

  /**
   * A private attribute of Action pointer type to save an action associated
   * with a Transition instance, in particular, it could be either a
   * Synchronization or a Label implementation.
   */
  Action *action_;

  /**
   * A private vector with Updates pointer types to save updates associated with
   * a Transition instance.
   */
  std::vector<Assignment *> updates_;

  /**
   * A private  attribute of a vector of Inequality pointers type to store the
   * transition's guard.
   */
  std::vector<Inequality *> guard_;

 public:
  /**
   * Transition constructor.
   * An instance of Transition is created.
   *
   * @param source_state is a default argument  to assign the State from where a
   * transition go out.
   *
   * @param destination_state is a default argument to assign the State to where
   * a transition go in.
   *
   * @param action is a default argument to assign the Action corresponding to a
   * Transition instance. If none of these arguments are passed to constructor,
   * they are assigned to nullptr.
   *
   * @param updates is a default argument to assign a list of Update belonging
   * to the transition.
   *
   * @param guard is a default argument to assign the guard of the transition.
   */
  Transition(State *source_state = nullptr, State *destination_state = nullptr,
             Action *action = nullptr, std::vector<Assignment *> updates = {},
             std::vector<Inequality *> guard = {});
  /**
   * Transition destructor.
   */
  ~Transition();

  /* Gets */

  /**
   * A public method to get the id number of a Transition instance.
   *
   * @return An integer type corresponding to the id of a Transition instance.
   */
  int get_id();

  /**
   * A public method to get the source State of a Transition instance.
   *
   * @return A pointer State type corresponding to the source State of a
   * Transition.
   */
  State *get_source_state();

  /**
   * A public method to get the destination State of a Transition instance.
   *
   * @return A pointer State type corresponding to the destination State of a
   * Transition.
   */
  State *get_destination_state();

  /**
   * A public method to get the Action of a Transition instance.
   *
   * @return A pointer Action type corresponding to the Action of a Transition.
   */
  Action *get_action();

  /**
   * A public method to get the updates of a Transition instance.
   *
   * @return A vector of Assignment pointers
   */
  std::vector<Assignment *> get_updates();

  /**
   * A public method to the the guard of a Transition instance.
   *
   * @return a vector of Inequality pointers.
   */
  std::vector<Inequality *> get_guard();

  /**
   * A public method to get the info of a Transition instance.
   *
   * @return A string type corresponding to the to the word (Transition)
   * concatenate with id number and info of a Action instance.
   */
  std::string get_info();

  /**
   * A public method implementing the visitor pattern
   *
   * @param visitor is a visitor instance
   */
  void accept(AMASVisitor &visitor);

  /* Sets */

  /**
   * A public method to set the source State of a Transition instance.
   *
   * @param source_state is a pointer State for the new source State to be
   * assigned in source_state_ attribute.
   */
  void set_source_state(State *source_state);

  /**
   * A public method to set the destination State of a Transition instance.
   *
   * @param destination_state is a pointer State for the new source State to be
   * assigned in destination_state_ attribute.
   */
  void set_destination_state(State *destination_state);

  /**
   * A public method to set the Action of a Transition instance.
   *
   * @param action is a pointer Action for the new action to be assigned in
   * action_ attribute.
   */
  void set_action(Action *action);

  /**
   * A public method to set the updates of a Transition instance.
   */
  void set_updates(std::vector<Assignment *> updates);

  /**
   * A public method to set the guard of a Transition instance.
   */
  void set_guard(std::vector<Inequality *> guard);

  /* Others methods*/
};

#endif
