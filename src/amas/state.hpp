#ifndef STATE_HPP
#define STATE_HPP

#include <string>
#include <vector>
#include "parser/amas_visitor.hpp"
#include "state_type.hpp"
#include "transition.hpp"

class AMASVisitor;
class Transition;

/**
 * @ingroup eamas
 *
 * State entity.
 *
 * It is part of Automaton used to denote their states with transtions as
 * abstract machine. It has an unique id number.
 */
class State {
 private:
  /**
   * A private attribute of integer type to save id number of a State instance.
   * This variable is responsible for save a counter value that is incremented
   * in current_id. It happens each time a State instance is created.
   */
  int id;

  /**
   * A private static attribute of integer type to increment and to have a
   * record about id numbers. This variable increments anytime a State instance
   * is created because of static property, it does not start each time from 0.
   */
  static int current_id;

  /**
   * A private attribute of StateType type to define their type of a State
   * instance.
   */
  StateType state_type_;

  /**
   * A private attribute of vector type to collect Transition pointers.
   * It is used to save Transition pointers associated with a State instance.
   */
  std::vector<Transition *> transitions_;

 public:
  /**
   * State constructor.
   * An instance of State is created.
   *
   * @param state_type is a default argument which if not passed, is Normal of
   * the enum class in state_type.hpp
   */
  State(StateType state_type = StateType::Normal);
  /**
   * State destructor.
   */
  ~State();

  /* Gets */
  /**
   * A public method to get the id number of Channel instance.
   *
   * @return An integer type corresponding to the id of a State instance
   */
  int get_id();

  /**
   * A public method to get the state type of a State instance.
   *
   * @return A StateType corresponding to the StateType associated to a State
   * instance.
   */
  StateType get_state_type();

  /**
   * A public method to get vector pointers of Transition instances.
   *
   * @return A vector of type Transition pointers, which are associated to a
   * State instance.
   */
  std::vector<Transition *> get_transitions();

  /**
   * A public method to get the info of a State instance.
   *
   * @param tabs which add an indentation  to the beginning of returning string.
   * By default it is 1.
   * @return A string type corresponding to the word (State) concatenate with id
   * number and state_type of a State instance.
   */
  std::string get_info(int tabs = 1);

  /**
   * A public method implementing the visitor pattern
   *
   * @param visitor is a visitor instance
   */
  void accept(AMASVisitor &visitor);

  /**
   * A public method to test if it is an initial state
   *
   * @return True if it is an initial state, false otherwise
   */
  bool is_initial();

  /* Sets */

  /**
   * A public method to set the state_type of a State instance.
   *
   * @param state_type is a StateType for the new state type to be assigned in
   * state_type_ attribute.
   */
  void set_state_type(StateType state_type);

  /**
   * A public method to set vector of Transition pointers.
   *
   * @param transitions is a vector for the new transition pointers to be
   * assigned in transitions_ attribute.
   */
  void set_transitions(std::vector<Transition *> transitions);
};
#endif
