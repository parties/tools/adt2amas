#include "automata.hpp"

#include <algorithm>
#include <string>

using namespace std;

Automata::Automata() {}

Automata::~Automata() {}

/* Gets */
vector<Automaton *> Automata::get_vector_automaton() {
  return this->vector_automaton_;
}

set<int> Automata::get_automaton_ids() { return this->automaton_ids_; }

vector<Channel *> Automata::get_channels() { return this->channels_; }

string Automata::get_info() {
  string info_amata = "";
  for (auto &i : this->vector_automaton_) {
    info_amata += i->get_info() + "\n";
  }
  for (auto &i : this->channels_) {
    info_amata += i->get_info() + "\n";
  }
  return info_amata;
}

void Automata::accept(AMASVisitor &visitor) {
  for (auto &automaton : this->get_vector_automaton()) {
    automaton->accept(visitor);
  }

  visitor.visit(this);
}

/* Sets */

void Automata::add_automaton(Automaton *new_automaton) {
  auto automaton_in_automata =
      this->automaton_ids_.find(new_automaton->get_id()) !=
      this->automaton_ids_.end();

  if (automaton_in_automata)
    throw AutomataException(
        "automaton can't be in the automata to allow their addition");

  this->automaton_ids_.insert(new_automaton->get_id());
  this->vector_automaton_.push_back(new_automaton);
}

void Automata::add_channel(Channel *new_channel) {
  auto name = new_channel->get_name();

  auto not_channel_in_automata =
      find_if(this->channels_.begin(), this->channels_.end(),
              [&name](Channel *chan) { return chan->get_name() == name; }) ==
      this->channels_.end();

  if (not_channel_in_automata) {
    this->channel_ids_.insert(new_channel->get_id());
    this->channels_.push_back(new_channel);
  }
}

// Removing...

void Automata::remove_automaton(Automaton *cur_automaton) {
  auto automaton_in_automata =
      this->automaton_ids_.find(cur_automaton->get_id()) !=
      this->automaton_ids_.end();

  if (!automaton_in_automata)
    throw AutomataException("automaton must be in the automata to remove it");

  auto it = this->vector_automaton_.begin();
  for (; it != this->vector_automaton_.end() &&
         (*it)->get_id() != cur_automaton->get_id();
       it++) {
  }

  for (int i = (*it)->get_states().size() - 1; i >= 0; i--) {
    if ((*it)->get_states()[i] != (*it)->get_initial_state())
      (*it)->remove_state((*it)->get_states()[i]);
  }
  (*it)->remove_initial_state();

  it = this->vector_automaton_.erase(it);
  this->automaton_ids_.erase(cur_automaton->get_id());
}

void Automata::remove_channel(Channel *cur_channel) {
  auto channel_in_automata = this->channel_ids_.find(cur_channel->get_id()) !=
                             this->channel_ids_.end();
  if (!channel_in_automata)
    throw AutomataException("channel must be in the automata to remove it");

  bool not_in_use = 1;
  for (auto it = this->vector_automaton_.begin();
       it != this->vector_automaton_.end() && not_in_use; it++) {
    vector<Transition *> tmp_vector = (*it)->get_transitions();
    for (auto itt = tmp_vector.begin(); itt != tmp_vector.end() && not_in_use;
         itt++) {
      if ((*itt)->get_action()->get_info_vector().size() > 1 &&
          (*itt)->get_action()->get_info_vector().back() ==
              cur_channel->get_name())
        not_in_use = 0;
    }
  }
  if (!not_in_use)
    throw AutomataException(
        "channel can't be used in the automata to remove it");

  auto it = this->channels_.begin();
  for (;
       it != this->channels_.end() && (*it)->get_id() != cur_channel->get_id();
       it++) {
  }
  it = this->channels_.erase(it);
  this->channel_ids_.erase(cur_channel->get_id());
}

Automaton *Automata::get_automaton(const std::string name) {
  std::vector<Automaton *> automaton_vector = this->get_vector_automaton();

  auto it =
      std::find_if(automaton_vector.begin(), automaton_vector.end(),
                   [&name](Automaton *a) { return a->get_name() == name; });

  if (it == automaton_vector.end()) {
    throw AutomataException("Automaton does not exist");
  }

  return (*it);
}
