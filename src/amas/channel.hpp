#ifndef CHANNEL_HPP
#define CHANNEL_HPP

#include <string>

/**
 * @ingroup eamas_action
 *
 * Channel entity, part of Automata used to synchronize Automaton.
 * This class is used to  allow a way where several agents can synchronize (Send
 * or Receive) regard to name Channel. It has an unique id number.
 */
class Channel {
 private:
  /**
   * A private attribute of integer type to save id number of a Channel
   * instance. This variable is responsible for save a counter value that is
   * incremented in current_id. It happens each time a Channel instance is
   * created.
   */
  int id;

  /**
   * A private static attribute of integer type to increment and to have a
   * record about id numbers. This variable increments anytime a Channel
   * instance is created because of static property, it does not start each time
   * from 0.
   */
  static int current_id;

  /**
   * A private attribute of string type to save a Channel name instance.
   */
  std::string name_;

 public:
  /**
   * Channel constructor.
   * An instance of Channel is created.
   *
   * @param name is a default argument which if not passed, is an empty string.
   */
  Channel(std::string name = "");
  /**
   * Channel destructor.
   */
  ~Channel();

  /* Gets */
  /**
   * A public method to get the id number of a Channel instance.
   *
   * @return An integer type corresponding to the id of a Channel instance
   */
  int get_id();

  /**
   * A public method to get the name of a Channel instance.
   *
   * @return A string type corresponding to the name of a Channel instance.
   */
  std::string get_name();

  /**
   * A public method to get the info of a Channel instance.
   *
   * @return A string type corresponding to the word (Channel) concatenate with
   * id number and name of a Channel instance.
   */
  std::string get_info();

  /* Sets */

  /**
   * A public method to set the name of a Channel instance.
   *
   * @param name is a string for the new name to be assigned in name_ attribute.
   */
  void set_name(std::string name);
};
#endif
