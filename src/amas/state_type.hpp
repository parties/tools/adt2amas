#ifndef STATE_TYPE_HPP
#define STATE_TYPE_HPP

#include <string>

/**
 * @ingroup eamas
 *
 * StateType enum class.
 *
 * by definition there are three types of States: initial, normal and accepted.
 */
enum class StateType { Normal, Initial, Accepted };

// methods

/**
 * A public method to get the info of a Label instance.
 *
 * @param st is StateType to be mapped.
 * @return A string type corresponding to the mapping function implemented for
 * the st argument.
 */
std::string to_string(StateType st);

#endif
