#ifndef LABEL_HPP
#define LABEL_HPP

#include <string>
#include <vector>
#include "action.hpp"
#include "parser/amas_visitor.hpp"

class AMASVisitor;

/**
 * @ingroup eamas_action
 *
 * Label entity, an Action which does not synchronize.
 *
 * It is an implementation for Action interface and set a name for an action
 * which does not synchronize.
 */
class Label : public Action {
 private:
  /**
   * A private attribute of string type to save a Label name instance.
   */
  std::string name_;

 public:
  /**
   * Label constructor.
   * An instance of Label is created.
   *
   * @param name is a default argument which if not passed, is an empty string.
   */
  Label(std::string name = "");

  /**
   * Label destructor.
   */
  ~Label();

  /* Gets */

  /**
   * A public method to get the name of a Label instance.
   *
   * @return A string type corresponding to the name of a Label instance.
   */
  std::string get_name();

  /**
   * A public method to get the info of a Label instance.
   *
   * @return A string type corresponding to the name of a Label instance.
   */
  std::string get_info();

  /**
   * A public method implementing the visitor pattern
   *
   * @param visitor is a visitor instance
   */
  void accept(AMASVisitor &visitor);

  /**
   * A public method to get the info of a Label instance.
   *
   * @return A vector type wih an element  corresponding to the name of a Label
   * instance.
   */
  std::vector<std::string> get_info_vector();

  /* Sets */

  /**
   * A public method to set the name of a Label instance.
   *
   * @param name is the new name to be assigned in name_ attribute.
   */
  void set_name(std::string name);

  /**
   * A public method to get the type of the action
   */
  std::string get_type();
};

#endif
