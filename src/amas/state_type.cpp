#include "state_type.hpp"

using namespace std;

string to_string(StateType st) {
  string _st = "";
  switch (st) {
    case StateType::Normal:
      _st = "Normal";
      break;
    case StateType::Initial:
      _st = "Initial";
      break;
    case StateType::Accepted:
      _st = "Accepted";
      break;
    default:
      break;
  }
  return _st;
}
