#ifndef SYNCHRONIZATION_HPP
#define SYNCHRONIZATION_HPP

#include <string>
#include <vector>
#include "action.hpp"
#include "channel.hpp"
#include "parser/amas_visitor.hpp"
#include "synchronization_type.hpp"

class AMASVisitor;

/**
 * @ingroup eamas_action
 *
 * Synchronization entity, an action used to synchronize automaton.
 *
 * It is an implementation for Action interface and set a type to synchronize
 * automaton through a channel.
 */
class Synchronization : public Action {
 private:
  /**
   * A private attribute of SynchronizationType type to save behavior of a
   * synchronization either be Send or Receive.
   */
  SynchronizationType synchronization_type_;

  /**
   * A private attribute of Channel pointer type to associate a Channel through
   * which a synchronization will be made.
   */
  Channel *channel_;

 public:
  /**
   * Synchronization constructor.
   * An instance of Synchronization is created.
   *
   * @param synchronization_type is a default argument, if not passed, is
   * synchronization type of Send (0).
   * @param channel is a default argument to define Channel, if not passed, is a
   * pointer nullptr.
   */
  Synchronization(
      SynchronizationType synchronization_type = SynchronizationType::Send,
      Channel *channel = nullptr);

  /**
   * Channel destructor.
   */
  ~Synchronization();

  /* Gets */
  /**
   * A public method to get the synchronization type of a Synchronization
   * instance.
   *
   * @return an integer corresponding to synchronization type of Send or
   * Receive.
   */
  SynchronizationType get_synchronization_type();

  /**
   * A public method to get a Channel instance pointer through a Synchronization
   * instance can synchronize.
   */
  Channel *get_channel();

  /**
   * A public method to get the info of a Synchronization instance.
   *
   * @return A string type corresponding to a string conversion of
   * Synchronization type concatenate the name of the Channel instance
   * associated.
   */
  std::string get_info();

  /**
   * A public method implementing the visitor pattern
   *
   * @param visitor is a visitor instance
   */
  void accept(AMASVisitor &visitor);

  /**
   * A public method to get the info of a Synchronization instance.
   *
   * @return A vector<string> type corresponding to a push back of a string
   * conversion of Synchronization type and a push back of the name of the
   * Channel instance associated.
   */
  std::vector<std::string> get_info_vector();

  /* Sets */
  /**
   * A public method to set the synchronization type of a Synchronization
   * instance.
   *
   * @param synchronization_type is a string for the new synchronization type to
   * be assigned in synchronization_type_ attribute.
   */
  void set_synchronization_type(SynchronizationType synchronization_type);

  /**
   * A public method to set the Channel instance pointer of a Synchronization
   * instance.
   *
   * @param channel is a pointer channel for the new channel to be assigned in
   * channel_ attribute.
   */
  void set_channel(Channel *channel);

  /**
   * A public method to get the type of the action
   */
  std::string get_type();

  /* Others methods*/
};

#endif
