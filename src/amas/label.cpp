#include "label.hpp"

using namespace std;

Label::Label(string name) : name_(name) {}

Label::~Label() {}

/* Gets */

string Label::get_name() { return this->name_; }

string Label::get_type() { return "Label"; }

string Label::get_info() { return this->name_; }

void Label::accept(AMASVisitor &visitor) { visitor.visit(this); }

vector<string> Label::get_info_vector() {
  vector<string> info_vector;
  info_vector.push_back(this->name_);
  return info_vector;
}

/* Sets */

void Label::set_name(string name) { this->name_ = name; }
