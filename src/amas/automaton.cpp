#include "automaton.hpp"

#include <algorithm>
#include <iterator>
#include <string>

using namespace std;

Automaton::Automaton(State *initial_state, string name, string type)
    : initial_state_(initial_state), name_(name), type_(type) {
  bool correct_type = initial_state->get_state_type() == StateType::Initial;
  if (!correct_type)
    throw AutomatonException(
        "Incorrect type for the parameter initial_state, must be Initial "
        "type");

  ++current_id;
  id = current_id;
  this->state_ids_.insert(initial_state->get_id());
  this->states_.push_back(initial_state);
}

Automaton::~Automaton() { delete this->initial_state_; }

/* Gets */

int Automaton::get_id() { return this->id; }

string Automaton::get_name() { return this->name_; }

string Automaton::get_type() { return this->type_; }

State *Automaton::get_initial_state() { return this->initial_state_; }

set<int> Automaton::get_transition_ids() { return this->transition_ids_; }

set<int> Automaton::get_state_ids() { return this->state_ids_; }

vector<State *> Automaton::get_states() { return this->states_; }

vector<Transition *> Automaton::get_transitions() { return this->transitions_; }

std::vector<Action *> Automaton::get_actions() {
  std::vector<Transition *> transitions = this->get_transitions();

  std::vector<Action *> actions;
  std::transform(transitions.begin(), transitions.end(),
                 std::back_inserter(actions),
                 [](Transition *t) { return t->get_action(); });

  return actions;
}

string Automaton::get_info() {
  string info_amaton = "Automaton " + to_string(this->id) + ":\n";
  for (auto &i : this->states_) {
    info_amaton += "\t" + i->get_info(2);
    info_amaton += i != this->states_.back() ? "\n" : "";
  }
  return info_amaton;
}

/* Sets */

void Automaton::set_initial_state(State *initial_state) {
  State *tmp_state = new State;
  tmp_state = this->initial_state_;
  this->initial_state_ = initial_state;
  tmp_state->set_state_type(StateType::Normal);
  this->initial_state_->set_state_type(StateType::Initial);
  this->state_ids_.insert(initial_state->get_id());
  this->states_.push_back(initial_state);
}

int Automaton::current_id = 0;

/* Other methods */

// Adding...

void Automaton::add_transition(State *source_state, State *destination_state,
                               Action *action, vector<Assignment *> updates,
                               vector<Inequality *> guard) {
  // Check that both states belong to the automaton
  auto no_source_state_in_automaton =
      this->state_ids_.find(source_state->get_id()) == this->state_ids_.end();
  auto no_dest_state_in_automaton =
      this->state_ids_.find(destination_state->get_id()) ==
      this->state_ids_.end();

  if (no_source_state_in_automaton || no_dest_state_in_automaton)
    throw AutomatonException(
        "both states have to exist already in the automaton");

  Transition *new_transition =
      new Transition(source_state, destination_state, action, updates, guard);

  vector<Transition *> tmp_transitions = source_state->get_transitions();
  tmp_transitions.push_back(new_transition);
  source_state->set_transitions(tmp_transitions);

  this->transition_ids_.insert(new_transition->get_id());
  this->transitions_.push_back(new_transition);
}

void Automaton::add_state(State *new_state) {
  auto state_in_automaton =
      this->state_ids_.find(new_state->get_id()) != this->state_ids_.end();

  if (state_in_automaton)
    throw AutomatonException(
        "state can't be added to the automaton because it exists already");

  this->state_ids_.insert(new_state->get_id());
  this->states_.push_back(new_state);
}

// Removing...
void Automaton::remove_transition(int transition_id) {
  auto transition_in_automaton =
      this->transition_ids_.find(transition_id) != this->transition_ids_.end();

  if (!transition_in_automaton)
    throw AutomatonException(
        "transition can't be removed since it doesn't belong to this "
        "automaton");

  auto it = this->transitions_.begin();
  for (; it != this->transitions_.end() && (*it)->get_id() != transition_id;
       it++) {
  }
  remove_transition(*it);
}

void Automaton::remove_transition(Transition *cur_transition) {
  auto transition_in_automaton =
      this->transition_ids_.find(cur_transition->get_id()) !=
      this->transition_ids_.end();

  if (!transition_in_automaton)
    throw AutomatonException(
        "transition can't be removed since it doesn't belong to this "
        "automaton");

  more_transitions_initial_state(cur_transition);

  /*Deleting transition pointer in transitions vector of the source state  */
  vector<Transition *> tmp_transitions =
      cur_transition->get_source_state()->get_transitions();
  auto it = tmp_transitions.begin();
  for (; it != tmp_transitions.end() &&
         (*it)->get_id() != cur_transition->get_id();
       it++) {
  }
  it = tmp_transitions.erase(it);
  cur_transition->get_source_state()->set_transitions(tmp_transitions);

  it = this->transitions_.begin();
  for (; it != this->transitions_.end() &&
         (*it)->get_id() != cur_transition->get_id();
       it++) {
  }
  it = this->transitions_.erase(it);
  this->transition_ids_.erase(cur_transition->get_id());
  delete cur_transition;
}

void Automaton::remove_initial_state() {
  vector<Transition *> tmp_transitions =
      this->initial_state_->get_transitions();

  auto no_transitions = tmp_transitions.empty();
  if (!no_transitions)
    throw AutomatonException(
        "in order to delete initial state, you must ensure there is not more "
        "transitions in the whole automaton");

  auto it = this->states_.begin();
  for (; it != this->states_.end() &&
         (*it)->get_id() != this->initial_state_->get_id();
       it++) {
  }
  it = this->states_.erase(it);
  this->state_ids_.erase(this->initial_state_->get_id());
  this->initial_state_ = nullptr;
}

void Automaton::remove_state(State *cur_state) {
  // Check that the state belongs to the automaton
  bool state_in_automaton =
      this->state_ids_.find(cur_state->get_id()) != this->state_ids_.end();
  if (!state_in_automaton)
    throw AutomatonException(
        "state can't be removed since it doesn't belong to this automaton");

  // Check that the state is not the initial state
  bool is_initial_state = cur_state->get_id() == this->initial_state_->get_id();
  if (is_initial_state)
    throw AutomatonException(
        "you can't remove their automaton initial state before replace it");

  for (int i = cur_state->get_transitions().size() - 1; i >= 0; i--) {
    remove_transition(cur_state->get_transitions()[i]);
  }

  /*Deleting transition pointers in transitions vector of the automaton which
   * have as destination state this cur_state  */
  remove_transition_due_removing_state(cur_state);

  auto it = this->states_.begin();
  for (; it != this->states_.end() && (*it)->get_id() != cur_state->get_id();
       it++) {
  }
  it = this->states_.erase(it);
  this->state_ids_.erase(cur_state->get_id());
}

void Automaton::more_transitions_initial_state(Transition *cur_transition) {
  bool more_transitions_initial_state = 1;
  int several_states = this->states_.size();
  if ((cur_transition->get_source_state() == this->initial_state_ ||
       cur_transition->get_destination_state() == this->initial_state_) &&
      several_states > 2) {
    more_transitions_initial_state =
        this->initial_state_->get_transitions().size() > 1;
  }

  if (!more_transitions_initial_state)
    throw AutomatonException(
        "You can't remove this transition since it is the unique transition "
        "associated with the initial state");
}

void Automaton::remove_transition_due_removing_state(State *cur_state) {
  for (int i = this->transitions_.size() - 1; i >= 0; i--) {
    if (this->transitions_[i]->get_destination_state() == cur_state) {
      Transition *on_transition = this->transitions_[i];

      more_transitions_initial_state(on_transition);

      vector<Transition *> source_state_transitions =
          on_transition->get_source_state()->get_transitions();
      auto it = source_state_transitions.begin();
      for (; it != source_state_transitions.end() &&
             (*it)->get_id() != on_transition->get_id();
           it++) {
      }
      it = source_state_transitions.erase(it);
      on_transition->get_source_state()->set_transitions(
          source_state_transitions);

      it = this->transitions_.begin();
      it = this->transitions_.erase(it + i);
      this->transition_ids_.erase(on_transition->get_id());
      delete on_transition;
    }
  }
}

vector<Assignment *> Automaton::get_variables() { return variables_; }

void Automaton::add_variable(Assignment *new_variable) {
  // Add variable only if it has not been declared yet
  if (find_if(variables_.begin(), variables_.end(),
              [new_variable](Assignment *a) {
                return a->get_variable() == new_variable->get_variable();
              }) == variables_.end()) {
    variables_.push_back(new_variable);
  }
}

void Automaton::accept(AMASVisitor &visitor) { visitor.visit(this); }
