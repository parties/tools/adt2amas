#include <CLI11.hpp>
#include <chrono>
#include <climits>
#include <exception>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <unordered_map>

#include "ad_tree.hpp"
#include "adtree/factory.hpp"
#include "adtree/gates/and.hpp"
#include "adtree/gates/nand.hpp"
#include "adtree/gates/or.hpp"
#include "adtree/gates/sand.hpp"
#include "adtree/gates/snand.hpp"
#include "algorithms/layered_reductions.hpp"
#include "algorithms/min_agents.hpp"
#include "amas/parser/amas_imitator_visitor.hpp"
#include "amas/parser/amas_tikz_visitor.hpp"
#include "automata.hpp"
#include "exception.hpp"
#include "translator.hpp"

using namespace std;

/**
 * Error class to handle CLI errors
 */
class ADT2AMASException : public Exception {
 public:
  explicit ADT2AMASException(const std::string &message) : Exception(message) {}
};

/**
 * Class that allows to output to two sources
 *
 */
class Tee {
 private:
  std::ostream &os1, &os2;

 public:
  Tee(std::ostream &os1, std::ostream &os2) : os1(os1), os2(os2) {}

  template <typename T>
  Tee &operator<<(const T &msg) {
    os1 << msg;
    os2 << msg;
    return *this;
  }
};

/**
 * Generate a LaTeX representation of an EAMAS model
 *
 * @param model EAMAS model
 * @param filename output filename
 */
void generate_latex(Automata *model, std::string filename) {
  ofstream myfile(filename + ".tex");
  AMASTikzVisitor visitor;
  model->accept(visitor);

  if (myfile.is_open()) {
    myfile << "\\documentclass[preview]{standalone}" << endl;
    myfile << "\\usepackage[table,dvipsnames]{xcolor}" << endl;
    myfile << "\\colorlet{colorCondition}{RedOrange}" << endl;
    myfile << "\\colorlet{colorAttribute}{PineGreen!75!green!85!black}" << endl;
    myfile << "\\colorlet{colorSync}{Blue}" << endl;
    myfile << "\\colorlet{colorTransition}{blue}" << endl;
    myfile << "\\usepackage{tikz}" << endl;
    myfile << "\\usetikzlibrary{arrows, automata, positioning}" << endl;
    myfile << "\\tikzset{initial text =\\(\\)}" << endl;
    myfile << "\\begin{document}" << endl;

    myfile << visitor.get_tikz() << endl;

    myfile << "\\end{document}" << endl;
    myfile.close();
  }
}

/**
 * Generate the IMITATOR model of the EAMAS model
 *
 * @param model EAMAS model
 * @param filename output filename
 */
void generate_imitator(Automata *model, std::string filename) {
  ofstream myfile(filename + ".imi");
  AMASIMITATORVisitor visitor;
  model->accept(visitor);

  if (myfile.is_open()) {
    myfile << "(************************************************************"
           << endl;
    myfile << " *                      IMITATOR MODEL " << endl;
    myfile << " * " << endl;
    myfile << " * Author : ADT2AMAS tool " << endl;
    myfile << " * " << endl;
    myfile << "************************************************************)"
           << endl;

    myfile << visitor.get_imitator() << endl;

    myfile.close();
  }
}

/**
 * Check if string contains substring
 *
 * @param str - string to be compared
 * @param substring - substring to find
 * @return boolean
 */
bool includes(const std::string &str, const std::string &substring) {
  return str.find(substring) != std::string::npos;
}

int get_copies_transitions(
    const std::unordered_map<std::string, int> &transitions_map,
    const std::string &transition_name) {
  auto search = transitions_map.find(transition_name);
  return (search != transitions_map.end()) ? search->second : 0;
}

/**
 * Generate the Modgraph model of the EAMAS model
 *
 * @param model EAMAS model
 * @param model_name model name
 */
void generate_modgraph(Automata *model, std::string model_name, bool symprod) {
  filesystem::path folder = model_name;

  // create folder containing the modules
  filesystem::remove_all(model_name);
  filesystem::create_directories(model_name);

  // synchronization transitions
  set<string> sync_transitions;

  bool root = true;

  std::vector<Automaton *> automata = model->get_vector_automaton();
  int nb_states = accumulate(
      automata.begin(), automata.end(), 0,
      [&](int total, Automaton *a) { return total + a->get_states().size(); });

  int next_nok = nb_states + 1;

  std::unordered_map<std::string, int> transitions_map;
  for (auto automaton : model->get_vector_automaton()) {
    string automaton_name = automaton->get_name();
    string automaton_type = automaton->get_type();

    // indicate which module is the root of the ADTree model
    if (root) {
      root = false;
      automaton_name += "-top";
    }

    ofstream myfile(folder / (automaton_name + ".modgraph"));

    if (myfile.is_open()) {
      bool is_leaf = automaton_type == "Leaf";

      // states
      myfile << "states" << endl;
      for (auto s : automaton->get_state_ids()) {
        myfile << s + 1 << endl;
      }

      if (!is_leaf) {
        myfile << next_nok << endl;
      }

      // transitions
      myfile << "transitions" << endl;
      std::string type_t = "";
      for (auto t : automaton->get_transitions()) {
        string source = std::to_string(t->get_source_state()->get_id() + 1);
        string target =
            std::to_string(t->get_destination_state()->get_id() + 1);

        string transition_name;
        if (t->get_action()->get_type() == "Synchronization") {
          transition_name =
              ((Synchronization *)t->get_action())->get_channel()->get_name();

          // it's a loop
          if (source == target) {
            if (includes(transition_name, "_nok")) {  // NOK loop
              if (!is_leaf) {  // create new state for NOK transition
                target = to_string(next_nok);
                next_nok++;
              }
              type_t = "loop_nok";
            } else {  // OK loop
              type_t = "loop_ok";
            }
          } else {
            type_t = "";
          }
        } else {
          // replace labelled transition by ok loop transition
          transition_name = ((Label *)t->get_action())->get_name() + "_ok";
          type_t = "label";
        }

        int nb_copies =
            get_copies_transitions(transitions_map, transition_name);

        // handle normal transitions
        if (!is_leaf && type_t == "") {
          // symprod compatibility
          if (symprod) {
            if (nb_copies > 0) {
              (transitions_map.find(transition_name))->second++;
              transition_name = transition_name + "_" + to_string(nb_copies);
            } else {
              transitions_map.insert({transition_name, 1});
            }
          }

          myfile << "(" << source << ", " << transition_name << " ," << target
                 << ")" << endl;

        } else if (type_t == "label" ||
                   (is_leaf && type_t == "" &&
                    includes(transition_name, "_nok")) ||
                   (!is_leaf && type_t == "loop_nok")) {
          myfile << "(" << source << ", " << transition_name << " ," << target
                 << ")" << endl;

          if (symprod) {
            for (int i = 1; i < nb_copies; i++) {
              myfile << "(" << source << ", "
                     << transition_name + "_" + to_string(i) << " ," << target
                     << ")" << endl;
            }
          }
        }

        // add transition to sync set
        sync_transitions.insert(transition_name);
      }

      myfile.close();
    }
  }

  // synchronization
  ofstream myfile(folder / ("sync.modgraph"));
  if (myfile.is_open()) {
    for (auto transition : sync_transitions) {
      myfile << transition << endl;
    }
  }
  myfile.close();
}

/**
 * Generates the Tikz representation of a preprocessing graph
 *
 * @param graph preprocessing graph
 * @param labels whether to print the vertices' depth and level
 * @param filename output filename
 */
void generate_tikz(const ProcessingGraph &graph, bool labels,
                   std::string filename) {
  TikzPrinter tikz_printer;
  std::ofstream tikz_file(filename + ".tex");

  if (tikz_file.is_open()) {
    tikz_printer.parser(tikz_file, graph, labels);
    tikz_file.close();
  }
}

/**
 * Generate the latex/ascii representation of the scheduling table
 *
 * @param minimal_assignment assignment to be printed
 * @param filename output filename
 * @param print_latex print latex table
 */
std::string generate_table(const MinimalAgentAssignment &minimal_assignment,
                           std::string filename, bool print_latex = false) {
  TablePrinter table_printer;

  if (print_latex) {
    std::ofstream latex_file(filename + ".tex");
    if (latex_file.is_open()) {
      std::string latex_table = table_printer.latex(minimal_assignment);
      latex_file << latex_table;
      latex_file.close();
    }
  }

  std::string ascii_table = table_printer.ascii(minimal_assignment);
  std::ofstream ascii_file(filename + ".txt");
  if (ascii_file.is_open()) {
    ascii_file << ascii_table;
    ascii_file.close();
  }

  return ascii_table;
}

/**
 * Creates an ADTree node depending on the string representation of its type and
 * role
 *
 * @param goal node's goal
 * @param type_s node's type
 * @param role_s node's role
 *
 * @return ADTree node
 */
TreeNode *get_node(std::string goal, std::string type_s, std::string role_s) {
  NodeRole role =
      (std::tolower(role_s[0]) == 'a') ? NodeRole::Attack : NodeRole::Defence;

  std::string type(type_s);
  std::transform(type.begin(), type.end(), type.begin(), ::tolower);

  if (type == "and") {
    return new And(role, goal);
  } else if (type == "nand" || type == "cand") {
    return new Nand(role, goal);
  } else if (type == "snand" || type == "scand") {
    return new Snand(role, goal);
  } else if (type == "sand") {
    return new Sand(role, goal);
  } else if (type == "or") {
    return new Or(role, goal);
  } else if (type == "leaf") {
    return new Leaf(role, goal);
  } else {
    throw ADT2AMASException("Type " + type_s + " does not exist");
  }
}

/**
 * Set the time and cost attributes of a ADTree node
 *
 * @param node ADTree node to be modified
 * @param cost node's cost
 * @param time node's time
 * @param children node's children
 */
void set_node_attributes(TreeNode *node, int cost, int time,
                         std::vector<TreeNode *> children) {
  std::string type = node->get_node_type();
  NodeRole role = node->get_node_role();

  if (type == "And") {
    node->set_attributes(and_factory_attributes(cost, time, children));
  } else if (type == "Nand") {
    TreeNode *child_same_role = *std::find_if(
        children.begin(), children.end(),
        [role](TreeNode *c) { return c->get_node_role() == role; });
    node->set_attributes(nand_factory_attributes(cost, time, child_same_role));
  } else if (type == "Snand") {
    TreeNode *child_same_role = *std::find_if(
        children.begin(), children.end(),
        [role](TreeNode *c) { return c->get_node_role() == role; });
    node->set_attributes(snand_factory_attributes(cost, time, child_same_role));
  } else if (type == "Sand") {
    node->set_attributes(sand_factory_attributes(cost, time, children));
  } else if (type == "Or") {
    node->set_attributes(or_factory_attributes(cost, time, node->get_goal()));
  } else if (type == "Leaf") {
    node->set_attributes(leaf_factory_attributes(cost, time));
  } else {
    throw ADT2AMASException("Type " + type + " does not exist");
  }
}

/**
 * Load an ADTree model from a file
 *
 * @param filename path of the file containing the model
 * @return ADTree model
 */
ADtree *load_model(std::string filename) {
  std::string line;
  ifstream model_file(filename);

  // error when opening file
  if (!model_file) {
    std::cerr << "Error: " << std::strerror(errno) << std::endl;
    exit(0);
  }

  // get root
  std::string root;
  getline(model_file, root);
  getline(model_file, line);  // ignore empty line

  // read nodes
  std::map<std::string, TreeNode *> nodes;
  std::map<std::string, std::pair<int, int>> info;
  while (getline(model_file, line), line != "") {
    std::string goal, type, role;
    int cost, time;

    stringstream s(line);
    s >> goal >> type >> role >> cost >> time;
    nodes[goal] = get_node(goal, type, role);
    info[goal] = make_pair(cost, time);
  }

  // create ADTree
  ADtree *model = new ADtree(nodes[root]);

  // read children
  std::map<std::string, std::vector<TreeNode *>> children;
  while (getline(model_file, line), line != "") {
    std::string parent;
    stringstream s(line);
    s >> parent;

    std::string child;
    while (s >> child) {
      children[parent].push_back(nodes[child]);
      model->add_child(nodes[child], nodes[parent]);
    }
  }

  // set attributes
  for (const auto &node : nodes) {
    std::string label = node.first;
    std::pair<int, int> info_node = info[label];
    set_node_attributes(node.second, info_node.first, info_node.second,
                        children[label]);
  }

  model_file.close();
  return model;
}

/*
 * Get the model's name from a file path
 *
 * @param s path
 * @return  model's name
 */
std::string get_model_name(const std::string &s) {
  int length = s.length();
  int i = s.rfind('/', length);
  int j = s.rfind('.', length);

  string model_name = s.substr(i + 1, j - i - 1);

  return model_name;
}

template <class result_t = std::chrono::milliseconds,
          class clock_t = std::chrono::steady_clock,
          class duration_t = std::chrono::milliseconds>
auto since(std::chrono::time_point<clock_t, duration_t> const &start) {
  return std::chrono::duration_cast<result_t>(clock_t::now() - start);
}

int main(int argc, const char *argv[]) {
  CLI::App app{"Attack-Defence Tree tool"};
  app.require_subcommand(1);

  // file containing the model
  string model_file;

  // model's name
  string model_name;

  // full model
  bool full_model = false;

  // rename transitions for compatibility with symprod tool
  bool symprod = false;

  // layered reductions
  bool layered_reductions = false;

  // output format
  string output_format = "imitator";

  // print latex
  bool print_latex = false;

  // print table with the scheduling
  bool print_scheduling = false;

  // ADTree model
  ADtree *adtree;

  /** subcommand: Atrack-Defence transformation */
  auto transform =
      app.add_subcommand("transform", "ADTree transformation into EAMAS");
  transform->add_option("--model,-m", model_file, "file with ADTree model")
      ->required();
  transform->add_option("--output-format,-f", output_format,
                        "format represeting the EAMAS", true);
  auto full_model_flag =
      transform->add_flag("--full-model", full_model,
                          "Generate full model [default: use reduced model]");
  auto symprod_flag = transform->add_flag("--symprod-compatibility", symprod,
                                          "rename  transitions");

  transform
      ->add_flag("--layered-reductions", layered_reductions,
                 "Apply layered reductions")
      ->excludes(full_model_flag);

  transform->callback([&]() {
    adtree = load_model(model_file);
    model_name = get_model_name(model_file);

    if (full_model) {
      cout << "\nGenerating full EAMAS model (variables will be ignored) ...";
    } else {
      cout << "\nGenerating EAMAS model ...";
    }

    Automata *model = Translator::adt2amas_translate(adtree, full_model);
    cout << " finished" << endl;

    // apply layered reductions
    if (layered_reductions && !full_model) {
      cout << "Applying layered reductions ..." << endl;
      LayeredReductions reduction = LayeredReductions(adtree, model);
      reduction.apply_reduction();
    }

    cout << "Saving EAMAS model as " << model_name << ".tex" << endl;
    generate_latex(model, model_name);

    // translating EAMAS into an output format
    if (output_format == "modgraph") {
      cout << "Saving Modgraph model as " << model_name << ".modgraph" << endl;
      generate_modgraph(model, model_name, symprod);
    } else {
      cout << "Saving IMITATOR model as " << model_name << ".imi" << endl;
      generate_imitator(model, model_name);
    }
  });

  /** subcommand: Schedule for minimal attack and number of agents */
  auto minimal = app.add_subcommand(
      "minimal", "Schedule for minimal attack time and number of agents");
  minimal->add_option("--model,-m", model_file, "file with ADTree model")
      ->required();
  minimal->add_flag("--print-latex", print_latex, "Print output in LaTeX");
  minimal->add_flag("--print-scheduling", print_scheduling, "Print scheduling");

  minimal->callback([&]() {
    // get name of the model
    model_name = get_model_name(model_file);

    // create logs
    ofstream log_file(model_name + ".log");
    Tee t(std::cout, log_file);

    // load model
    auto start_load = std::chrono::steady_clock::now();
    adtree = load_model(model_file);
    t << "Time to load the model: " << since(start_load).count() << "ms \n";

    /** Preprocessing step */
    auto start_preprocessing = std::chrono::steady_clock::now();
    Preprocessing preprocessing(adtree);
    std::vector<Preprocessing::Output> output = preprocessing.get_graphs();
    const auto elapsed_preprocessing = since(start_preprocessing).count();
    t << "Time to preprocess the graph: " << elapsed_preprocessing << "ms \n";
    t << "Number of generated graphs: " << output.size() << "\n";

    // iterating over the pre-processing graphs
    auto start_all = std::chrono::steady_clock::now();
    for (int i = 1; i <= output.size(); i++) {
      t << "\nGraph #" << std::to_string(i) << "\n";

      /** Print active counter nodes */
      t << "Active counter nodes in graph: ";
      std::vector<label> active_counter_nodes = output[i - 1].counter_nodes;
      for (int i = 0; i < active_counter_nodes.size(); i++) {
        if (i > 0) t << ", ";
        t << active_counter_nodes[i];
      }
      if (active_counter_nodes.empty()) t << "None";

      /** Saving Preprocessing graph */
      if (print_latex) {
        std::string file_graph =
            model_name + "_preprocessing_" + std::to_string(i);
        t << "\nSaving preprocessing graph as " << file_graph << ".tex"
          << "\n";
        ProcessingGraph g = output[i - 1].preprocessed_graph;
        generate_tikz(g, false, file_graph);
      }

      /** Apply minimal assignment algorithm to each possible graph */
      auto start_local = std::chrono::steady_clock::now();
      std::vector<MinimalAgentAssignment> candidates;
      for (const ProcessingGraph &flat_graph : output[i - 1].flat_graphs) {
        MinimalAgentAssignment minimal_assignment_tmp(flat_graph);
        candidates.push_back(minimal_assignment_tmp);
      }
      auto elapsed_local = since(start_local).count();
      t << "\nTime to compute the minimal assignment for the graph (without "
           "preprocessing time): "
        << elapsed_local << "ms \n";

      /* Best minimal assignment */
      std::sort(candidates.begin(), candidates.end());
      MinimalAgentAssignment minimal_assignment = candidates.front();

      if (print_latex) {
        std::string assignment_file =
            model_name + "_minimal_assignment_" + std::to_string(i);
        t << "Saving minimal assignment graph as " << assignment_file << ".tex";
        generate_tikz(minimal_assignment.get_graph(), true, assignment_file);
      }

      /* Printing information */
      t << "# Agents: " << minimal_assignment.get_agents() << "\n";
      t << "# Slots: " << minimal_assignment.get_slots() << "\n";

      if (print_scheduling) {
        /* Printing scheduling table */
        std::string scheduling_file =
            model_name + "_scheduling_" + std::to_string(i);
        t << "Printing scheduling table as " << scheduling_file << ".{tex,txt}"
          << "\n";

        std::string ascii_table =
            generate_table(minimal_assignment, scheduling_file, print_latex);
        t << ascii_table;
      }
    }
    const auto elapsed_all = since(start_all).count();
    t << "\nTime to compute the minimal assignment for all the graphs (without "
         "preprocessing time): "
      << elapsed_all << "ms \n";
    t << "Time to compute the minimal assignment for all the graphs : "
      << elapsed_all + elapsed_preprocessing << "ms \n";
  });

  // parse arguments
  CLI11_PARSE(app, argc, argv);

  return 0;
}
