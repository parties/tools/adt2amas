#ifndef CONSTANT_HPP
#define CONSTANT_HPP

#include <string>
#include "linear_expression.hpp"
#include "parser/linear_expression_visitor.hpp"

class LinearExpressionVisitor;

/**
 * @ingroup linear_expr
 *
 * Represents a constant value
 */
class Constant : public LinearExpression {
  /** constant value */
  int value_;

 public:
  /**
   * Creates a new constant
   *
   * @param value constant's value
   */
  Constant(int value);

  /**
   * Gets the value of the constant
   *
   * @return an integer
   */
  int get_value();

  /**
   * Returns the string representation of the constant
   *
   * @return a string
   */
  std::string get_info();

  /**
   * Returns the string representation of the constant type
   *
   * @return an string
   */
  std::string get_type();

  /**
   * Implements the visitor pattern
   *
   * @param visitor is a visitor instance
   */
  void accept(LinearExpressionVisitor &visitor);
};

#endif
