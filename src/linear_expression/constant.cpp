#include "constant.hpp"
#include <string>

using namespace std;

Constant::Constant(int value) : value_(value) {}
int Constant::get_value() { return value_; }
string Constant::get_info() { return to_string(value_); }

void Constant::accept(LinearExpressionVisitor& visitor) { visitor.visit(this); }

string Constant::get_type() { return "Constant"; };
