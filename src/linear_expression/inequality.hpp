#ifndef INEQUALITY_HPP
#define INEQUALITY_HPP

#include <string>
#include "linear_expression.hpp"
#include "operators/inequality_operator.hpp"
#include "parser/linear_expression_visitor.hpp"

class LinearExpressionVisitor;

/**
 * @ingroup linear_expr
 *
 * Represents an inequality
 */
class Inequality : public LinearExpression {
 private:
  /** left part of the inequality */
  LinearExpression* left_;

  /** right part of the inequality */
  LinearExpression* right_;

  /** inequality */
  InequalityOperator operator_;

 public:
  Inequality(LinearExpression* left, LinearExpression* right,
             InequalityOperator op);

  /**
   * Gets the string representation of the inequality
   *
   * @return a string
   */
  std::string get_info();

  /**
   * Returns the string representation of the inequality type
   *
   * @return an string
   */
  std::string get_type();

  /**
   * Retuns the left side of the inequality
   *
   * @return a linear expression pointer
   */
  LinearExpression* get_left();

  /**
   * Retuns the right side of the inequality
   *
   * @return a linear expression pointer
   */
  LinearExpression* get_right();

  /**
   * Gets the operator of the inequality
   *
   * @return an inequality operator
   */
  InequalityOperator get_operator();

  /**
   * Implements the visitor pattern
   *
   * @param visitor is a visitor instance
   */
  void accept(LinearExpressionVisitor& visitor);
};

#endif  // INEQUALITY_HPP
