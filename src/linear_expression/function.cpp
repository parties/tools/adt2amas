#include "function.hpp"
#include <string>
#include <vector>

using namespace std;

Function::Function(string name, vector<string> arguments)
    : name_(name), arguments_(arguments){};

string Function::get_name() { return this->name_; }

vector<string> Function::get_arguments() { return this->arguments_; }

string Function::get_info() {
  string text = name_ + "(";

  for (auto& a : arguments_) {
    text += a;

    if (a != arguments_.back()) {
      text += ",";
    }
  }
  text += ")";

  return text;
}

void Function::accept(LinearExpressionVisitor& visitor) { visitor.visit(this); }

string Function::get_type() { return "Function"; };
