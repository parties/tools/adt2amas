#ifndef OPERATOR_HPP
#define OPERATOR_HPP

#include <string>
#include <vector>
#include "linear_expression.hpp"
#include "parser/linear_expression_visitor.hpp"

class LinearExpressionVisitor;

/**
 * @ingroup linear_expr_operator
 *
 * Represents a linear expression composed by an operator
 */
class Operator : public LinearExpression {
 protected:
  /** terms of the linear expression */
  std::vector<LinearExpression *> terms_;

  /** operator symbol*/
  std::string operator_symbol_;

 public:
  /**
   * Creates a new linear expression using an operation
   *
   * @param symbol operator symbol
   * @param terms terms of the operator
   */
  Operator(std::string symbol, std::vector<LinearExpression *> terms);

  /**
   * Adds a new term to the operator
   */
  void add_term(LinearExpression *linear_expr);

  /**
   * Gets the symbol of the operator
   *
   * @return the string representation of the operator symbol
   */
  std::string get_symbol();

  /**
   * Gets the terms of the operator
   *
   * @return a list of linear expressions (terms)
   */
  std::vector<LinearExpression *> get_terms();

  /**
   * Gets the string representation of the linear expression
   *
   * @return a string
   */
  std::string get_info();

  /**
   * Returns the string representation of the operator type
   *
   * @return an string
   */
  std::string get_type();

  /**
   * Implements the visitor pattern
   *
   * @param visitor is a visitor instance
   */
  void accept(LinearExpressionVisitor &visitor);
};

#endif
