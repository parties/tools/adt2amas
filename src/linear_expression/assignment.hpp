#ifndef ASSIGNMENT_HPP
#define ASSIGNMENT_HPP

#include <string>
#include "linear_expression.hpp"

/**
 * @ingroup linear_expr
 *
 * Represents the assignment of a value to a variable
 */
class Assignment {
 protected:
  /** variable being updated */
  std::string variable_;

  /** update linear expression */
  LinearExpression* linear_expr_;

 public:
  /**
   * Creates a new variable update
   *
   * @param variable_name variable being updated
   * @param expr update linear expression
   */
  Assignment(std::string variable_name, LinearExpression* expr);

  /**
   * Gets the name of the variable being updated.
   *
   * @return string of the variable's name
   */
  std::string get_variable();

  /**
   * Gets the update linear expression
   *
   * @return the linear expression
   */
  LinearExpression* get_linear_expression();

  /**
   * Gets the string representation of the variable's update
   *
   * @return the string representation
   */
  std::string get_info();
};

#endif
