#ifndef FUNCTION_HPP
#define FUNCTION_HPP

#include <string>
#include <vector>
#include "linear_expression.hpp"
#include "parser/linear_expression_visitor.hpp"

class LinearExpressionVisitor;

/**
 * @ingroup linear_expr
 *
 * Represents a function
 */
class Function : public LinearExpression {
 protected:
  /** the name of the function */
  std::string name_;

  /** function's arguments */
  std::vector<std::string> arguments_;

 public:
  /**
   * Creates a new function
   *
   * @param name function's name
   * @param arguments function's arguments
   */
  Function(std::string name, std::vector<std::string> arguments);

  /**
   * Gets the name of the function
   *
   * @return a string of the name
   */
  std::string get_name();

  /**
   * Gets the arguments of the function
   *
   * @return a list of arguments
   */
  std::vector<std::string> get_arguments();

  /**
   * Returns the string representation of the function
   *
   * @return a string
   */
  std::string get_info();

  /**
   * Returns the string representation of the function type
   *
   * @return an string
   */
  std::string get_type();

  /**
   * Implements the visitor pattern
   *
   * @param visitor is a visitor instance
   */
  void accept(LinearExpressionVisitor &visitor);
};

#endif
