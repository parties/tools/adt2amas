#include "variable.hpp"
#include <string>

using namespace std;

Variable::Variable(string variableName) : variable_(variableName) {}

string Variable::get_variable() { return variable_; }

string Variable::get_info() { return variable_; }

void Variable::accept(LinearExpressionVisitor &visitor) { visitor.visit(this); }

string Variable::get_type() { return "Variable"; };
