#ifndef VARIABLE_HPP
#define VARIABLE_HPP

#include <string>
#include "linear_expression.hpp"
#include "parser/linear_expression_visitor.hpp"

class LinearExpressionVisitor;

/**
 * @ingroup linear_expr
 *
 * Represents a variable
 */
class Variable : public LinearExpression {
  /** variable's name */
  std::string variable_;

 public:
  /**
   * Creates a new variable
   *
   * @param variable_name name of the variable
   */
  Variable(std::string variable_name);

  /**
   * Gets the variable's name
   *
   * @return a string
   */
  std::string get_variable();

  /**
   * Returns the string representation of the variable
   *
   * @return an string
   */
  std::string get_info();

  /**
   * Returns the string representation of the variable type
   *
   * @return an string
   */
  std::string get_type();

  /**
   * Implements the visitor pattern
   *
   * @param visitor is a visitor instance
   */
  void accept(LinearExpressionVisitor &visitor);
};

#endif
