#include "inequality.hpp"

using namespace std;

Inequality::Inequality(LinearExpression* left, LinearExpression* right,
                       InequalityOperator op)
    : left_(left), right_(right), operator_(op) {}

string Inequality::get_info() {
  string text =
      left_->get_info() + " " + to_string(operator_) + " " + right_->get_info();
  return text;
};

string Inequality::get_type() { return "Inequality"; };

InequalityOperator Inequality::get_operator() { return this->operator_; }

LinearExpression* Inequality::get_right() { return this->right_; }
LinearExpression* Inequality::get_left() { return this->left_; }

void Inequality::accept(LinearExpressionVisitor& visitor) {
  visitor.visit(this);
}
