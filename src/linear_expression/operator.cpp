#include "operator.hpp"
#include <string>
#include <vector>

using namespace std;

Operator::Operator(string symbol, vector<LinearExpression *> terms)
    : operator_symbol_(symbol), terms_(terms) {}

void Operator::add_term(LinearExpression *linear_expr) {
  this->terms_.push_back(linear_expr);
}

string Operator::get_info() {
  string text = "";

  for (auto &term : terms_) {
    text += term->get_info();
    if (term != terms_.back()) text += operator_symbol_;
  }

  return text;
};

string Operator::get_symbol() { return this->operator_symbol_; }

vector<LinearExpression *> Operator::get_terms() { return this->terms_; };

void Operator::accept(LinearExpressionVisitor &visitor) { visitor.visit(this); }

string Operator::get_type() { return "Operator"; };
