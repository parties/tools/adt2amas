#include "inequality_operator.hpp"

using namespace std;

string to_string(InequalityOperator op) {
  std::string op_str = "";
  switch (op) {
    case InequalityOperator::Less:
      op_str = "<";
      break;
    case InequalityOperator::LessEqual:
      op_str = "<=";
      break;
    case InequalityOperator::Equal:
      op_str = "=";
      break;
    case InequalityOperator::Greater:
      op_str = ">";
      break;
    case InequalityOperator::GreaterEqual:
      op_str = ">=";
      break;
    default:
      break;
  }

  return op_str;
};
