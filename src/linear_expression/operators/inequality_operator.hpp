#ifndef INEQUALITY_OPERATOR
#define INEQUALITY_OPERATOR

#include <string>

/**
 * @ingroup linear_expr
 *
 * Represents the inequality operators
 */
enum class InequalityOperator { Less, LessEqual, Equal, Greater, GreaterEqual };

/**
 * Returns the string representation of the inequality operator.
 *
 * @param op  inequality operator.
 *
 * @return a string with the inequality operator.
 */
std::string to_string(InequalityOperator op);

#endif  // INEQUALITY_OPERATOR
