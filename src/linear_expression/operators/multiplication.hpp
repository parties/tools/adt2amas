#ifndef MULTIPLICATION_HPP
#define MULTIPLICATION_HPP

#include <vector>
#include "linear_expression.hpp"
#include "operator.hpp"

/**
 * @ingroup linear_expr_operator
 *
 * Represents a multiplication operator
 */
class Multiplication : public Operator {
 public:
  /**
   * Creates a new linear expression composed by a multiplication operator
   *
   * @param terms operator terms
   */
  Multiplication(std::vector<LinearExpression *> terms)
      : Operator("*", terms){};
};

#endif
