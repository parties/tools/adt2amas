#ifndef ADDITION_HPP
#define ADDITION_HPP

#include <vector>
#include "linear_expression.hpp"
#include "operator.hpp"

/**
 * @ingroup linear_expr_operator
 *
 * Represents an addition operator
 */
class Addition : public Operator {
 public:
  /**
   * Creates a new linear expression composed by an addition operator
   *
   * @param terms operator terms
   */
  Addition(std::vector<LinearExpression *> terms) : Operator("+", terms){};
};

#endif
