#include "linear_expression_imitator_visitor.hpp"
#include <string>

using namespace std;

LinearExpressionImitatorVisitor::LinearExpressionImitatorVisitor(){};

void LinearExpressionImitatorVisitor::visit(Function *function) {
  vector<string> arguments = function->get_arguments();

  text_ += function->get_name() + "(";

  for (auto &a : arguments) {
    text_ += a;
    if (a != arguments.back()) text_ += ",";
  }

  text_ += ")";
};

void LinearExpressionImitatorVisitor::visit(Constant *constant) {
  text_ += to_string(constant->get_value());
};

void LinearExpressionImitatorVisitor::visit(Variable *variable) {
  text_ += variable->get_variable();
};

void LinearExpressionImitatorVisitor::visit(Operator *op) {
  vector<LinearExpression *> terms = op->get_terms();
  string symbol = op->get_symbol();

  for (auto &t : terms) {
    t->accept(*this);
    if (t != terms.back()) text_ += " " + symbol + " ";
  }
};

void LinearExpressionImitatorVisitor::visit(Inequality *inequality) {
  inequality->get_left()->accept(*this);
  text_ += " " + to_string(inequality->get_operator()) + " ";
  inequality->get_right()->accept(*this);
}

string LinearExpressionImitatorVisitor::get_imitator() { return text_; };
