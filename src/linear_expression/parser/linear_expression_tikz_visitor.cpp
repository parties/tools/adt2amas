#include "linear_expression_tikz_visitor.hpp"

#include <regex>
#include <string>
#include <vector>

using namespace std;

LinearExpressionTikzVisitor::LinearExpressionTikzVisitor() {}

void LinearExpressionTikzVisitor::visit(Function* function) {
  regex re("(_)");
  vector<string> arguments = function->get_arguments();

  text_ += function->get_name() + "(";

  for (auto& a : arguments) {
    text_ += regex_replace(a, re, "\\$1");

    if (a != arguments.back()) {
      text_ += ",";
    }
  }

  text_ += ")";
}

void LinearExpressionTikzVisitor::visit(Constant* constant) {
  text_ += constant->get_info();
};

void LinearExpressionTikzVisitor::visit(Variable* linear_coefficient) {
  regex re("(_)");
  string variable = linear_coefficient->get_variable();

  text_ += regex_replace(variable, re, "\\$1");
};

void LinearExpressionTikzVisitor::visit(Operator* op) {
  vector<LinearExpression*> children = op->get_terms();
  string symbol = op->get_symbol();

  for (auto& child : children) {
    child->accept(*this);
    if (child != children.back()) text_ += symbol;
  }
};

void LinearExpressionTikzVisitor::visit(Inequality* inequality) {
  inequality->get_left()->accept(*this);
  text_ += to_string(inequality->get_operator());
  inequality->get_right()->accept(*this);
}

string LinearExpressionTikzVisitor::get_tikz() { return text_; }
