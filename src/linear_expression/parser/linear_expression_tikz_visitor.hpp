#ifndef LINEAR_EXPRESSION_TIKZ_VISITOR_HPP
#define LINEAR_EXPRESSION_TIKZ_VISITOR_HPP

#include <string>
#include "linear_expression_visitor.hpp"

/**
 * @ingroup linear_expr_visitor
 *
 * Visitor Tikz parser for the Linear Expression module
 */
class LinearExpressionTikzVisitor : public LinearExpressionVisitor {
 public:
  LinearExpressionTikzVisitor();

  void visit(Function *function);
  void visit(Constant *constant);
  void visit(Variable *variable);
  void visit(Operator *op);
  void visit(Inequality *inequality);

  /**
   * Gets the Tikz representation of an object
   *
   * @return the string of tikz
   */
  std::string get_tikz();

 private:
  /** text buffer */
  std::string text_ = "";
};

#endif  // LINEAR_EXPRESSION_TIKZ_VISITOR_HPP
