#ifndef LINEAR_EXPRESSION_VISITOR_HPP
#define LINEAR_EXPRESSION_VISITOR_HPP

#include "constant.hpp"
#include "function.hpp"
#include "inequality.hpp"
#include "operator.hpp"
#include "variable.hpp"

class Operator;
class Inequality;
class Function;
class Variable;
class Constant;

/**
 * @ingroup linear_expr_visitor
 * Visitor interface for the Linear Expression module
 */
class LinearExpressionVisitor {
 public:
  /**
   * Visits an operator
   *
   * @param[in] op operator
   */
  virtual void visit(Operator* op) = 0;

  /**
   * Visits an inequality
   *
   * @param[in] inequality inequality
   */
  virtual void visit(Inequality* inequality) = 0;

  /**
   * Visits a function
   *
   * @param[in] function function
   */
  virtual void visit(Function* function) = 0;

  /**
   * Visits a variable
   *
   * @param[in] variable variable
   */
  virtual void visit(Variable* variable) = 0;

  /**
   * Visits a constant
   *
   * @param[in] constant constant
   */
  virtual void visit(Constant* constant) = 0;
};

#endif  // LINEAR_EXPRESSION_VISITOR_HPP
