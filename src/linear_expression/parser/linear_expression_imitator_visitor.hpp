#ifndef LINEAR_EXPRESSION_IMITATOR_VISITOR_HPP
#define LINEAR_EXPRESSION_IMITATOR_VISITOR_HPP

#include <string>
#include "linear_expression_visitor.hpp"

/**
 * @ingroup linear_expr_visitor
 *
 * Visitor Tikz parser for the Linear Expression module
 */
class LinearExpressionImitatorVisitor : public LinearExpressionVisitor {
 public:
  LinearExpressionImitatorVisitor();

  void visit(Function *function);
  void visit(Constant *constant);
  void visit(Variable *variable);
  void visit(Operator *op);
  void visit(Inequality *inequality);

  /**
   * Gets the imitator representation of a linear expression
   *
   * @return a string
   */
  std::string get_imitator();

 private:
  /** text buffer */
  std::string text_ = "";
};

#endif  // LINEAR_EXPRESSION_IMITATOR_VISITOR_HPP
