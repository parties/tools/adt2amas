#include "assignment.hpp"
#include <string>
#include "linear_expression.hpp"

using namespace std;

Assignment::Assignment(string variable_name, LinearExpression* expr)
    : variable_(variable_name), linear_expr_(expr) {}

string Assignment::get_variable() { return this->variable_; }

LinearExpression* Assignment::get_linear_expression() {
  return this->linear_expr_;
}

string Assignment::get_info() {
  string text = this->variable_ + " := " + this->linear_expr_->get_info();
  return text;
}
