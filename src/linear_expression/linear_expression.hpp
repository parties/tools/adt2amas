#ifndef LINEAR_EXPRESSION_HPP
#define LINEAR_EXPRESSION_HPP

#include <string>

class LinearExpressionVisitor;

/**
 * @ingroup linear_expr
 * Interface specifying a Linear Expression
 */
class LinearExpression {
 public:
  /**
   * Returns the string representation of the linear expression
   *
   * @return an string
   */
  virtual std::string get_info() = 0;

  /**
   * Returns the string representation of the linear expression type
   *
   * @return an string
   */
  virtual std::string get_type() = 0;

  /**
   * Implements the visitor pattern
   *
   * @param visitor is a visitor instance
   */
  virtual void accept(LinearExpressionVisitor &visitor) = 0;
};

#endif
